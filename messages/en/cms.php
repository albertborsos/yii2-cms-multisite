<?php

return [
	'login_successful' => '<h4>Successful login!</h4>',
    'logout_succesful' => '<h4>Successful logout!</h4>',

    'registration_succesful' => '<h4>Successfule registration!</h4><p>Check out your mailbox to activate your account!</p>',

    'activation_successful' => '<h4>Successful activation!</h4><p>Now you can log in to the site!</p>',
    'activation_error_wrong_link' => '<h4>Wrong activation link!</h4><p>That is not the correct one...</p>',
    'activation_error_wrong_key' => '<h4>Wrong activation key!</h4><p>Or you already activated your registration. Ty to log in!</p>',
    'activation_error_wrong_email' => '<h4>This email address is not exists in the system</h4><p>Or you already activated your registration. Ty to log in!</p>',
    'activation_error' => '<h4>Unable to activate your account</h4>',

    'reminder_email_sent' => '<h4>Password reminder sent!</h4><p>Follow the instructions in the mail!</p>',
    'reminder_error' => '<h4>Unable to send a password reminder to you!</h4>',

    'new_password_successfully_changed' => '<h4>Password successfully updated!</h4>',
    'new_password_error_wrong_link' => '<h4>Wrong link...</h4><p>... or your password reminder is expired. Require a new one!</p>',
    'new_password_error_email' => '<h4>This email is not the same as in our system!</h4>',
    'new_password_error_valid' => '<h4>The new passwords are not the same!</h4>',

    'user_remove_successful' => ' user successfully removed from the system!',
    'user_remove_error' => ' users rights is impossible to remove!',
    'user_remove_yourself' => 'You can not remove yourself from the system!',
    'user_right_not_exists' => 'This permission is not exists!',

    'user_details_update_successful' => '<h4>Detail informations successfully updated!</h4>',
    'user_details_update_error' => '<h4>Unable to store this data!</h4>',
    'en' => 'English',
    'de' => 'Deutsch',
    'hu' => 'Hungarian',

    'requested-page-not-exists' => 'The requested page does not exist.',
];