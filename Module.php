<?php

namespace albertborsos\yii2cmsmultisite;

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\Document;
use albertborsos\yii2cmsmultisite\models\Gallery;
use albertborsos\yii2cmsmultisite\models\Site;
use albertborsos\yii2cmsmultisite\models\SitePost;
use albertborsos\yii2cmsmultisite\models\Textbox;
use albertborsos\yii2lib\helpers\Glyph;
use albertborsos\yii2lib\helpers\S;
use rmrevin\yii\fontawesome\FA;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Module extends \yii\base\Module
{
    const MENU_CMS = 'cms';
    const MENU_USER = 'user';
    const MENU_SITES = 'sites';

    public $controllerNamespace = 'albertborsos\yii2cmsmultisite\controllers';
    public $name = 'Tartalomkezelő';

    public $site;
    public $staticUrl;

    public $urlPostfix = '.html';

    /**
     * "site_id => postfix map" for correct admin url previews
     * @var array
     */
    public $urlPostfixes = [];

    public $galleryPath = '@frontend/../uploads/images';

    public $disableAdmin = false;
    public $disablePublic = false;

    public $defaultShareImage = [];

    public $themes = [];
    public $layouts = [];
    public $widgets = [];

    public $consolePath = '@app/../yii';
    public $phpBinPath = '/usr/bin/php';

    public $showLanguageSelector = false;

    public $skipUrlCheck = false;

    public $viewMenu = '@vendor/albertborsos/yii2-cms-multisite/views/default/_menu';
    public $viewBlog = '@vendor/albertborsos/yii2-cms-multisite/views/default/_blog';
    public $viewBlogIndex = '@vendor/albertborsos/yii2-cms-multisite/views/default/index';
    public $viewBlogPostPreview = '@vendor/albertborsos/yii2-cms-multisite/views/default/_post_preview';

    /**
     * List of extra GET request paramters to add to the page cache variation calculation.
     *
     * ```
     *     'pageCacheVariationExtraParameters' => [
     *         '/relative-url' => ['parameter1', 'parameter2'],
     *     ],
     * ```
     *
     * @var array[]
     */
    public $pageCacheVariationExtraParameters = [];

    /**
     * Merge these contents with the default replaceableContents
     *
     *   'extraReplaceableContents' => [
     *      ['\albertborsos\yii2newsletter\models\Lists', 'insertForms'],
     *   ],
     */
    public $extraReplaceableContents = [];

    /**
     * Posts::insertform() and Galleries::insertGallery()
     */
    private $_replaceableContents    = [];

    /**
     * override default ContactForm with other Form
     *
     *  'forms' => [
     *      '[#contactUs#]' => '\frontend\models\ContactForm',
     *  ],
     */
    public $forms     = [];

    /**
     * override defauélt subscribe form with other Form
     *
     *  'listForms' => [
     *      //'1' => '\frontend\models\ContactForm',
     *   ]
     */
    public $listForms = [];

    /**
     * override defauélt subscribe form with other Form
     *
     *  'listForms' => [
     *      //'1' => '\frontend\models\ContactForm',
     *   ]
     */
    public $campaignForms = [];

    public $dynamicStrings = [];

    public $enableSyntaxHighlighter = false;

    /**
     * Module specific urlManager
     * @param $app
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            $this->id . '/?' => $this->id . '/default/index',
            $this->id . '/<id:\d+>' => $this->id . '/default/view',
            $this->id . '/<action:\w+>/?' => $this->id . '/default/<action>',
            $this->id . '/<controller:\w+>/<action:\w+>' => $this->id . '/<controller>/<action>',
        ], false);
    }

    public function init()
    {
        parent::init();
        $i18n = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@vendor/albertborsos/yii2-cms-multisite/messages',
                'forceTranslation' => true
            ];
        Yii::$app->i18n->translations['cms'] = $i18n;

        if(is_null($this->staticUrl)){
            $this->staticUrl = Yii::$app->getUrlManager()->getBaseUrl();
        }
        // custom initialization code goes here
        $view = \Yii::$app->view;

        $this->setReplaceableContents();
        $this->setForms();
    }

    private function setForms(){
        $defaultForms = [
            //'[#contactUs#]' => ContactForm::className(),
        ];

        $this->forms = ArrayHelper::merge($defaultForms, $this->forms);
    }

    /**
     * merges the CMS default replaceable items with $extrareplaceableContents
     */
    private function setReplaceableContents(){
        $this->_replaceableContents = [
            // [className, method]
            [Gallery::className(), 'insertGallery'],
            [SitePost::className(), 'insertForms'],
            [Textbox::className(), 'insertTextBoxes'],
        ];

        $this->_replaceableContents = ArrayHelper::merge($this->extraReplaceableContents, $this->_replaceableContents);
    }

    private function getReplaceableContents(){
        return $this->_replaceableContents;
    }

    public function replaceItems($content){
        $items = $this->getReplaceableContents();
        foreach($items as $item){
            $class = ArrayHelper::getValue($item, '0');
            $method = ArrayHelper::getValue($item, 1);

            $content = $class::$method($content);
        }
        return $content;
    }

    public function replaceDynamicStrings($content): string
    {
        foreach ($this->dynamicStrings as $key => $value) {
            if (is_callable($value)) {
                $value = $value();
            }
            $content = str_replace($key, $value, $content);
        }
        return $content;
    }

    public static function getMenu($type){
        $items = [];
        switch($type){
            case self::MENU_CMS:
                $items = [
                    'label' => FA::icon(FA::_GLOBE) . ' Oldalak',
                    'url' => ['/cms2/site/index'],
                    'visible' => Yii::$app->getUser()->can('editor'),
                ];
                break;
            case self::MENU_SITES:
                $items[] = [
                    'label' => FA::icon(FA::_PENCIL_SQUARE_O) . ' Szövegdobozok',
                    'url' => ['/cms2/textbox/index'],
                    'visible' => Yii::$app->getUser()->can('editor'),
                ];
                $items[] = [
                    'label' => FA::icon(FA::_FILE) . ' Dokumentumok',
                    'items' => [
                        ['label' => 'Képek', 'url' => ['/cms2/document/index', 'type' => Document::TYPE_IMAGE]],
                        ['label' => 'PDF', 'url' => ['/cms2/document/index', 'type' => Document::TYPE_DOCUMENT]],
                        ['label' => 'Videó előnézeti kép', 'url' => ['/cms2/document/index', 'type' => Document::TYPE_VIDEO_PREVIEW]],
                        ['label' => 'PDF előnézeti kép', 'url' => ['/cms2/document/index', 'type' => Document::TYPE_DOCUMENT_PREVIEW]],
                    ],
                    'visible' => Yii::$app->getUser()->can('editor'),
                ];
                $items[] = [
                    'label' => FA::icon(FA::_PICTURE_O) . ' Galéria',
                    'url' => ['/cms2/gallery/index'],
                    'visible' => Yii::$app->getUser()->can('editor'),
                ];
                $items[] = S::divider(true);
                $sites = Site::find()->where(['status' => Site::STATUS_ACTIVE])->orderBy(['id' => SORT_ASC])->all();
                foreach($sites as $site){
                    $items[] = [
                        'label' => FA::icon(FA::_GLOBE) . ' ' . $site->name,
                        'items' => [
                            ['label' => 'Nyelvek', 'url' => ['/cms2/language/index', 'site' => $site->id]],
                            ['label' => 'Menüpontok', 'url' => ['/cms2/post/index', 'site' => $site->id, 'type' => SitePost::TYPE_MENU], 'visible' => count($site->siteLanguages) > 0],
                            ['label' => 'Blog Bejegyzések', 'url' => ['/cms2/post/index', 'site' => $site->id, 'type' => SitePost::TYPE_BLOG], 'visible' => count($site->siteLanguages) > 0],
                        ],
                        'url' => '#',
                        'linkOptions' => [
                            'class' => 'dropdown-toggle',
                            'data-toggle' => 'dropdown',
                        ],
                        'visible' => Yii::$app->getUser()->can('editor'),
                    ];
                }
                $items = [
                    'label' => FA::icon(FA::_BOOK) . ' Tartalomkezelés',
                    'items' => $items,
                    'url' => '#',
                    'linkOptions' => [
                        'class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown',
                    ],
                    'visible' => Yii::$app->getUser()->can('editor') && count($sites) > 0,
                ];
                break;
            case self::MENU_USER:
                $items = [
                    'label' => FA::icon(FA::_USER) . ' ' . Yii::$app->user->identity->getFullname(),
                    'items' => [
                        ['label' => FA::icon(FA::_FILE) . ' Profilom', 'url' => ['/cms2/user/profile']],
                        ['label' => FA::icon(FA::_COG) . ' Beállítások', 'url' => ['/cms2/user/settings']],
                        ['label' => FA::icon(FA::_COG) . ' Jogosultságok', 'url' => ['/cms2/rights/admin']],
                        S::divider(true),
                        ['label' => FA::icon(FA::_SIGN_OUT) . ' Kijelentkezés', 'url' => ['/cms2/user/logout'], 'linkOptions' => ['data-method' => 'post',]],
                    ],
                    'url' => '#',
                    'linkOptions' => [
                        'class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown',
                    ],
                ];
        }
        return $items;
    }

    public function setTheme($type){
        $selectedTheme = S::get($this->themes, $type);
        if(!is_null($selectedTheme)){
            Yii::$app->view->theme->pathMap = ['@app/views' => [$selectedTheme]];
        }
    }

    public static function profileBoxParts(){
        if (Yii::$app->user->isGuest){
            $header = Html::tag('h3', Glyph::icon(Glyph::ICON_USER).' Belépés', ['class' => 'panel-title']);
            $content = Html::a('Belépés', ['/cms2/user/login'], ['class' => 'btn btn-success btn-block'])
                . Html::a('Regisztráció', ['/cms2/user/register'], ['class' => 'btn btn-info btn-block']);
        }else{
            $header = Html::tag('h3', Glyph::icon(Glyph::ICON_USER).' '.Yii::$app->getUser()->getIdentity()->getFullName(), ['class' => 'panel-title']);
            $content = Html::beginTag('div', ['class' => 'btn-group-vertical btn-block', 'role' => 'group']);
            if (Yii::$app->user->can('editor')){
                $content .= Html::a('Tartalomkezelés', ['/cms2/post/index', 'language' => 'hu'], ['class' => 'btn btn-default btn-block']);
            }
            $content .= Html::a('Fiókom', ['/cms2/user/profile'], ['class' => 'btn btn-default btn-block']);
            $content .= Html::a('Kijelentkezés', ['/cms2/user/logout'], ['class' => 'btn btn-default btn-block', 'data-method' => 'post']);
            $content .= Html::endTag('div');
        }

        return [
            'heading' => $header,
            'body' => $content,
        ];
    }

    public static function sidebarProfileBox(){
        $parts = self::profileBoxParts();
        return \kartik\helpers\Html::panel($parts);
    }

    public static function renderPublicNavbar(){
        $language = Yii::$app->language;
        $menuCacheID = Yii::$app->user->isGuest ? 'public-navbar' : 'navbar-user-' . Yii::$app->user->id;
        $menuCacheID .= '-' . $language;
        return Yii::$app->cache->getOrSet($menuCacheID, function () {
            return array_merge(DataProvider::renderItems(), DataProvider::addLanguageSelector());
        }, 10 * 60);
    }

    public function getSitePostfix(int $siteId): ?string
    {
        return $this->urlPostfixes[$siteId] ?? $this->urlPostfix;
    }
}
