<?php

namespace albertborsos\yii2cmsmultisite\commands;

use albertborsos\yii2cmsmultisite\models\Document;
use Spatie\Image\Image;
use yii\console\Controller;
use yii\helpers\Console;

class ImageController extends Controller
{
    public $defaultAction = 'optimize';

    /**
     * Optimizes original images and thumbnails
     * @return void
     */
    public function actionOptimize(int $maxFullWidth = 1920, int $maxThumbnailWidth = 640): void
    {
        /** @var Document[] $documents */
        $documents = Document::findAll([
            'type' => Document::TYPE_IMAGE,
        ]);

        $variations = [
            false,
            true,
        ];

        ini_set('memory_limit', '256M');
        foreach ($variations as $isThumbnail) {
            $maxWidth = $isThumbnail ? $maxThumbnailWidth : $maxFullWidth;
            foreach ($documents as $document) {
                $sourceFilePath = $document->getPathFull();
                $targetFilePath = $document->getPathFull($isThumbnail);
                if (!is_file($sourceFilePath)) {
                    Console::output('missing - ' . $sourceFilePath . ' ID:' . $document->id);
                    continue;
                }
                $image = Image::load($sourceFilePath);
                if ($image->getWidth() < $maxWidth) {
                    continue;
                }
                Console::output('resize to ' . $maxWidth . 'px - ' . $targetFilePath);
                $image->width($maxWidth);
                $image->save($targetFilePath);
            }
        }
    }
}
