<?php

namespace albertborsos\yii2cmsmultisite\controllers;

use albertborsos\yii2cmsmultisite\models\Document;
use albertborsos\yii2cmsmultisite\models\Gallery;
use albertborsos\yii2cmsmultisite\models\GalleryDocument;
use albertborsos\yii2cmsmultisite\models\GalleryDocumentSearch;
use albertborsos\yii2lib\web\Controller;
use himiklab\sortablegrid\SortableGridAction;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * GalleryDocumentController implements the CRUD actions for GalleryDocument model.
 */
class GalleryDocumentController extends Controller
{
    public function actions(): array
    {
        return [
            'sort' => [
                'class' => SortableGridAction::class,
                'modelName' => GalleryDocument::class,
            ],
        ];
    }

    public function init()
    {
        parent::init();
        $this->defaultAction = 'index';
        $this->name = 'Galériaképek';
        $this->layout = '//center';
    }

    private function setBreadcrumbs($gallery)
    {
        $module = $this->breadcrumbs[0];
        $controller = $this->breadcrumbs[1];
        $action = $this->breadcrumbs[2];

        $this->breadcrumbs[1] = [
            'label' => ucfirst($gallery->name . ' Galéria'),
            'url' => ['gallery/update', 'id' => $gallery->id],
        ];

        $this->breadcrumbs[2] = $action;
        $this->breadcrumbs[2]['label'] = 'Képfeltöltés';
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        'matchCallback' => function(){
                            return Yii::$app->user->can('reader');
                            }
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'updatebyeditable', 'sort'],
                        'allow' => true,
                        'matchCallback' => function () {
                            return Yii::$app->user->can('editor');
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'           => ['post'],
                    'updatebyeditable' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GalleryDocument models.
     * @return mixed
     */
    public function actionIndex($gallery_id = null)
    {
        $this->layout = '//fluid';
        if (!is_null($gallery_id)){
            /** @var Gallery $gallery */
            $gallery = Gallery::findOne(['id' => $gallery_id]);
            $this->setBreadcrumbs($gallery);
            if (Yii::$app->request->isPost){
                try{
                    Yii::$app->response->getHeaders()->set('Vary', 'Accept');
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $response = [];
                    $uploaded = UploadedFile::getInstanceByName('filename');
                    $document = new Document();
                    $document->type = Document::TYPE_IMAGE;
                    if (!is_null($uploaded)){
                        $existingDocumentID = $document->isShaExists($uploaded);
                        if($existingDocumentID === false){
                            // ha még nem volt feltöltve a dokumentumok közé
                            // akkor fel kell tölteni és utána hozzárendelni a galériához
                            $document->type = Document::TYPE_IMAGE;
                            $extension =  $document->setExtension($uploaded);
                            $document->filename = $uploaded->baseName;
                            $document->generateUniqueName($extension);
                            $document->status = Document::STATUS_ACTIVE;
                            if ($document->validate()){
                                if($document->saveUploadedFile($uploaded)){
                                    switch($document->type){
                                        case Document::TYPE_IMAGE:
                                        case Document::TYPE_VIDEO_PREVIEW:
                                        case Document::TYPE_DOCUMENT_PREVIEW:
                                            $document->savePhoto($document->getPathFull(), 1980);
                                            $document->savePhoto($document->getPathFull(), 640, true);
                                            $thumbnail = $document->getUrlFull(true);
                                            break;
                                        case Document::TYPE_DOCUMENT:
                                            $thumbnail = null;
                                            break;
                                    }

                                }
                            }else{
                                $document->throwNewException('Hibás kép attribútum!');
                            }
                        }else{
                            // ha ez a kép már szerepel a dokumentumok között
                            // akkor tudjuk a típusát is
                            $document = Document::findOne(['id' => $existingDocumentID]);
                        }
                        $galleryDocument = new GalleryDocument();
                        $galleryDocument->gallery_id = $gallery_id;
                        $galleryDocument->document_id = $document->id;
                        $galleryDocument->status = GalleryDocument::STATUS_ACTIVE;
                        if($galleryDocument->save()){
                            //Now we return our json
                            $response['files'][] = [
                                'name' => $document->filename,
                                'type' => $uploaded->type,
                                'size' => $uploaded->size,
                                'url' => $galleryDocument->document->getUrlFull(),
                                'thumbnailUrl' => $galleryDocument->document->getUrlFull(true),
                                'deleteUrl' => $galleryDocument->getUrlDelete(),
                                'deleteType' => 'POST'
                            ];
                        }else{
                            $galleryDocument->throwNewException('Hiba a feltöltés során!');
                        }
                    }else{
                        throw new Exception('Nem érkezett kép!');
                    }

                }catch (Exception $e){
                    $response['files'][] = [
                        'error' => strip_tags($e->getMessage()),
                    ];
                }
                return $response;
            }
        } else {
            return $this->redirect(['gallery/index']);
        }

        $searchModel = new GalleryDocumentSearch();
        $dataProvider = $searchModel->search([
            'GalleryDocumentSearch' => ['gallery_id' => (int)$gallery_id],
            'Sort' => [
                'enableMultiSort' => true,
                'defaultOrder' => $gallery->getOrderByCondition(),
            ],
        ]);

        return $this->render('upload', [
            'gallery' => $gallery,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GalleryDocument model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing GalleryDocument model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $gallery_id = null;
        try{
            $model = $this->findModel($id);
            $gallery_id = $model->gallery_id;
            $model->delete();
        }catch (Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index', 'gallery_id' => $gallery_id]);
    }

    /**
     * Finds the GalleryDocument model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GalleryDocument the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GalleryDocument::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdatebyeditable(){
		$key       = Yii::$app->request->post('pk');
		$id        = unserialize(base64_decode($key));
        $attribute = Yii::$app->request->post('name');
        $value     = Yii::$app->request->post('value');

        $model = $this->findModel($id);
        try{
            if (!is_null($model)){
                $model->$attribute = $value;
                if (!$model->save()){
                    throw new Exception('Galériakép módosítása nem sikerült');
                }
            }else{
                throw new Exception('Nem létezik ilyen rekord!');
            }
        }catch (Exception $e){
            throw new HttpException(400,$e->getMessage());
        }
    }
}
