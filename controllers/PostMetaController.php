<?php

namespace albertborsos\yii2cmsmultisite\controllers;

use albertborsos\yii2cmsmultisite\models\SiteLanguage;
use albertborsos\yii2cmsmultisite\models\SitePost;
use albertborsos\yii2cmsmultisite\Module;
use Yii;
use albertborsos\yii2cmsmultisite\models\SitePostMeta;
use albertborsos\yii2cmsmultisite\models\SitePostMetaSearch;
use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2lib\web\Controller;
use yii\base\Exception;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PostMetaController implements the CRUD actions for SitePostMeta model.
 */
class PostMetaController extends Controller
{
    public function init()
    {
        parent::init();
        $this->defaultAction = 'index';
        $this->name = 'SEO adatok';
        $this->layout = '//center';
        $this->addActionNames(['edit' => 'Szerkesztés']);
    }

    private function setBreadcrumbs($post = null, $language = null){
        if(!is_null($post) && !is_null($language)){
            $module = $this->breadcrumbs[0];
            $controller = $this->breadcrumbs[1];
            $action = $this->breadcrumbs[2];

            $post = SitePost::findOne(['id' => $post]);
            $language = SiteLanguage::findOne(['id' => $language]);

            $this->breadcrumbs[1] = [
                'label' => $post->site->name,
                'url' => ['post/index', 'site' => $post->site->id],
            ];

            $this->breadcrumbs[2] = [
                'label' => DataProvider::items('post_type', $post->type, false),
                'url' => ['post/index', 'site' => $post->site->id, 'type' => $post->type],
            ];

            $this->breadcrumbs[3] = $post->name . ' SEO adatok szerkesztése ('.$language->name.')';
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        'matchCallback' => function(){
                            return Yii::$app->user->can('reader');
                        }
                    ],
                    [
                        'actions' => ['edit', 'delete', 'updatebyeditable'],
                        'allow' => true,
                        'matchCallback' => function(){
                            return Yii::$app->user->can('editor');
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'           => ['post'],
                    'updatebyeditable' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SitePostMeta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SitePostMetaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SitePostMeta model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SitePostContent model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionEdit($post, $language_id, $rev = null)
    {
        if(!is_null($rev)){
            $model = SitePostMeta::findOne(['id' => $rev]);
        }else{
            $model = SitePostMeta::find()->where([
                'site_post_id' => $post,
                'site_language_id' => $language_id,
            ])->orderBy('id DESC')->one();
        }
        if(is_null($model)){
            $model = new SitePostMeta();
            $model->site_post_id = $post;
            $model->site_language_id = $language_id;
        }
        $model->id = null;
        $model->published = DataProvider::YES;
        $model->setIsNewRecord(true);

        $this->setBreadcrumbs($post, $language_id);

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try{
                if ($model->save()){
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', '<h4>Bejegyzés SEO beállításai sikeresen létrehozva!</h4>');
                    return $this->redirect(['edit', 'post' => $model->site_post_id, 'language_id' => $model->site_language_id]);
                }else{
                    $model->throwNewException('Bejegyzés SEO beállításainak mentése nem sikerült!');
                }
            }catch (Exception $e){
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $searchModel = new SitePostMetaSearch();
        $params = Yii::$app->request->queryParams;
        $params['SitePostMetaSearch']['site_post_id'] = $model->site_post_id;
        $params['SitePostMetaSearch']['site_language_id'] = $model->site_language_id;
        $dataProvider = $searchModel->search($params);

        $model->sitePost->showAlerts($language_id);

        /** @var Module $module */
        $module = Yii::$app->getModule('cms2');

        return $this->render('edit', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'urlPostfix' => $module->getSitePostfix($model->sitePost->site_id),
        ]);
    }
    /**
     * Deletes an existing SitePostMeta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try{
            $this->findModel($id)
                ->delete();
        }catch (Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the SitePostMeta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SitePostMeta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SitePostMeta::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdatebyeditable(){
        $key       = Yii::$app->request->post('pk');
        $id        = unserialize(base64_decode($key));
        $attribute = Yii::$app->request->post('name');
        $value     = Yii::$app->request->post('value');

        $model = $this->findModel($id);
        try{
            if (!is_null($model)){
                $model->$attribute = $value;
                if (!$model->save()){
                    throw new Exception('SEO beállítások módosítása nem sikerült');
                }
            }else{
                throw new Exception('Nem létezik ilyen rekord!');
            }
        }catch (Exception $e){
            throw new HttpException(400,$e->getMessage());
        }
    }
}