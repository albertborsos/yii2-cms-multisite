<?php

namespace albertborsos\yii2cmsmultisite\controllers;

use albertborsos\yii2cmsmultisite\assets\SyntaxHighlighterAsset;
use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\components\Widget;
use albertborsos\yii2cmsmultisite\models\SitePost;
use albertborsos\yii2cmsmultisite\models\SitePostContent;
use albertborsos\yii2cmsmultisite\models\SitePostMeta;
use albertborsos\yii2cmsmultisite\models\TextboxContent;
use albertborsos\yii2cmsmultisite\Module;
use albertborsos\yii2lib\helpers\Seo;
use albertborsos\yii2lib\web\Controller;
use albertborsos\yii2tagger\models\Tags;
use Yii;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class DefaultController
 * @package albertborsos\yii2cmsmultisite\controllers
 *
 * @property Module $module
 */
class DefaultController extends Controller
{
    const URL_BLOG = '/blog';

    /** @var SitePost|null */
    private $_page;

    public function init(): void
    {
        parent::init();
        $this->defaultAction = 'index';
        $this->name = 'Alap';
        $this->layout = '//center';
        $this->module->setTheme('page');
    }

    public function behaviors(): array
    {
        return [
            'httpCache' => [
                'class' => \yii\filters\HttpCache::class,
                'enabled' => $this->isCacheEnabled(),
                'cacheControlHeader' => 'public, max-age=3600',
                'only' => ['index', 'blog'],
                'lastModified' => function ($action, $params) {
                    $q = new \yii\db\Query();

                    $spc = $q->from(SitePostContent::tableName())->max(new Expression('COALESCE(`updated_at`, `created_at`)'));
                    $tbc = $q->from(TextboxContent::tableName())->max(new Expression('COALESCE(`updated_at`, `created_at`)'));

                    return max($spc, $tbc);
                },
            ],
            'pageCache' => [
                'class' => 'yii\filters\PageCache',
                'enabled' => $this->isCacheEnabled(),
                'only' => ['index', 'blog'],
                'duration' => 24 * 60 * 60,
                'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql' => 'SELECT COUNT(*) as `sort_order` FROM `tbl_cms2_site_post_content` `spc`
                        JOIN `tbl_cms2_site_post` `sp` ON sp.id=spc.site_post_id
                        JOIN `tbl_cms2_textbox_content` `tc`
                        WHERE `sp`.`site_id`=:site AND `spc`.`published`=:published
                        ORDER BY `sort_order` DESC',
                    'params' => [
                        ':site' => $this->module->site,
                        ':published' => 1,
                    ],
                ],
                'variations' => $this->getCacheVariation(),
            ]
        ];
    }

    private function isCacheEnabled(): bool
    {
        if (YII_DEBUG) {
            return false;
        }

        $numFlash = count(Yii::$app->session->getAllFlashes());
        if ($numFlash > 0) {
            return false;
        }

        if (Yii::$app->request->isPost) {
            return false;
        }

        /** @var SitePost $page */
        $slug = Yii::$app->request->get('slug');
        $id = Yii::$app->request->get('id');
        $action = Yii::$app->request->get('action');

        if (empty($slug) && empty($id) && Yii::$app->request->url === self::URL_BLOG) {
            return false;
        }

        $page = $this->findPage($slug, $id, false);

        if ($page === null) {
            return false;
        }

        if($page->hasForm()){
            return false;
        }

        // ha widget, akkor ne cache-eljen
        if($page instanceof SitePost && !empty($page->getContent()->widget)){

            $widget = $page->getContent()->widget;
            $widgetObject = new $widget;
            if($widgetObject instanceof Widget){
                return $widgetObject->cache;
            }

            return false;
        }

        return true;
    }

    public function actions(): array
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
            ],
        ];
    }

    public function actionIndex($title = null, $id = null, $slug = null)
    {
        $this->registerSyntaxHighlighter();

        $post = $this->findPage($slug, $id);

        if (!$this->hasRightForMenu($post)) {
            Yii::$app->session->setFlash('danger', 'Nincs jogosultságod az oldal megtekintéséhez! Jelentkezz be!');
            return $this->redirect(Yii::$app->user->loginUrl);
        }

        $this->breadcrumbs = [$post->getContent()->getTitle()];
        $post->setLayout();
        $post->setSEOValues();

        $content = $post->renderContent($this->module);
        $content = $this->module->replaceItems($content);
        $content = $this->module->replaceDynamicStrings($content);

        if($content === true){
            return $this->redirect(Yii::$app->request->url);
        }

        return $this->render('index', [
            'content' => $content,
        ]);
    }

    /**
     * Listázza az összes aktív blogbejegyzést
     */
    public function actionBlog(): string
    {
        DataProvider::checkUrlIsCorrect(Url::to(['/cms2/default/blog'], true));
        $this->registerSyntaxHighlighter();

        Yii::$app->getView()->title = 'Blog | ' . Yii::$app->name;
        $this->breadcrumbs = ['Blog'];

        $posts = SitePost::findAllBlogPosts(SORT_DESC);

        $content = '';
        if (empty($posts)) {
            $content .= '<legend>Egyelőre nincsenek bejegyzések!</legend>';
            Seo::noIndex();
        } else {
            foreach ($posts as $post) {
                $content .= $this->renderPartial($this->module->viewBlogPostPreview, [
                    'post' => $post,
                    'tags' => Tags::getAssignedTags($post, true, 'link'),
                ]);
            }
        }

        $post = $this->findPage('blog', null);
        $post->setSEOValues();

        return $this->render($this->module->viewBlogIndex, [
            'content' => $content,
        ]);
    }

    public function actionRedirecttohome(): Response
    {
        Yii::$app->session->setFlash('info', '<h4>Szia! A régi weboldalam megszűnt!</h4><p>Az a tartalom, amit kerestél már nem létezik. De azért nézz körbe hátha találsz néhány hasznos információt!</p>');
        return $this->redirect('/', 301);
    }

    private function registerSyntaxHighlighter(): void
    {
        if (Yii::$app->getModule('cms2')->enableSyntaxHighlighter) {
            SyntaxHighlighterAsset::register($this->view);
        }
    }

    private function hasRightForMenu(SitePost $post): bool
    {
        $rightToView = $post->right_to_view;

        if (empty($rightToView) && !empty($post->parent_post_id)) {
            $rightToView = $this->hasRightForMenu($post->parentPost);
        }

        if (empty($rightToView) || $rightToView === true) {
            return true;
        }

        return Yii::$app->user->can($rightToView);
    }

    private function findPage(?string $slug, ?int $id, bool $throwException = true): ?SitePost
    {
        if(empty($this->_page)){
            if (!is_null($slug)) {
                $id = SitePostMeta::getSitePostIdBySlug($slug, $this->module->site);
            }

            if(!is_null($slug) && empty($id) && $throwException) {
                throw new NotFoundHttpException(Yii::t('cms', 'requested-page-not-exists'));
            }

            if (empty($slug) && empty($id)) {
                // if there is no slug and no id, then it is the main page
                $this->_page = SitePost::getMainPage($this->module->site);
            } else {
                // menu or blog posts
                $this->_page = SitePost::getPageById($id, $this->module->site);
            }

            if (empty($this->_page) && $throwException) {
                throw new NotFoundHttpException(Yii::t('cms', 'requested-page-not-exists'));
            }
        }

        return $this->_page;
    }

    protected function getCacheVariation(): array
    {
        $defaultVariation = [
            Yii::$app->language,
            Yii::$app->request->get('title'),
            Yii::$app->request->get('id'),
            Yii::$app->request->get('slug'),
            Yii::$app->get('cookieConsent', false) ? Yii::$app->cookieConsent->getSettingsHash() : null,
        ];

        if (isset($this->module->pageCacheVariationExtraParameters[Yii::$app->request->url])) {
            foreach ($this->module->pageCacheVariationExtraParameters[Yii::$app->request->url] as $parameter) {
                array_push($defaultVariation, Yii::$app->request->get($parameter));
            }
        }

        return array_filter($defaultVariation);
    }
}
