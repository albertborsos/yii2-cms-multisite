<?php

namespace albertborsos\yii2cmsmultisite\controllers;

use albertborsos\yii2lib\helpers\Seo;
use albertborsos\yii2lib\helpers\Values;
use albertborsos\yii2lib\web\Controller;
use albertborsos\yii2cmsmultisite\forms\LoginForm;
use albertborsos\yii2cmsmultisite\forms\RegisterForm;
use albertborsos\yii2cmsmultisite\forms\ReminderForm;
use albertborsos\yii2cmsmultisite\forms\SetNewPasswordForm;
use albertborsos\yii2cmsmultisite\models\Users;
use yii\base\Action;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class UserController extends Controller
{
    public $name = 'Felhasználó';
    public $defaultAction = 'login';

    public function init()
    {
        parent::init();
        $names = [
            'profile' => 'Profil',
            'settings' => 'Beállítások',
            'login' => 'Bejelentkezés',
            'logout' => 'Kijelentkezés',
            'register' => 'Regisztráció',
            'activate' => 'Fiókaktiválás',
            'reminder' => 'Jelszóemlékeztető',
            'setnewpassword' => 'Új jelszó beállítása',
        ];
        $this->addActionNames($names);
        $this->module->setTheme('page');
        $this->layout = '//center';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['settings', 'profile'],
                        'allow' => true,
                        'matchCallback' => function () {
                            return !Yii::$app->user->isGuest;
                        }
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'matchCallback' => function () {
                            return !Yii::$app->user->isGuest;
                        }
                    ],
                    [
                        'actions' => ['register', 'activate', 'login', 'setnewpassword', 'reminder'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->setBreadcrumbs($action);
            return true;
        }
        return false;
    }


    private function setBreadcrumbs(Action $action)
    {
        $view = Yii::$app->getView();

        $this->breadcrumbs = [
            ['label' => $this->getActionName($action)],
        ];
        $view->title = $this->getActionName($action).' - '.$this->name.' | '.Yii::$app->name;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->session->setFlash('success', Yii::t('cms', 'login_successful'));

            if ($this->module->disablePublic) {
                return $this->redirect(['/cms2/site/index']);
            } else {
                return $this->goBack();
            }
        } else {
            Seo::noIndex();
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout(Yii::$app->user->destroySession);
        Yii::$app->session->setFlash('error', Yii::t('cms', 'logout_succesful'));

        return $this->goHome();
    }

    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RegisterForm();
        if ($model->load(Yii::$app->request->post()) && $model->register()) {
            Yii::$app->session->setFlash('success', Yii::t('cms', 'registration_succesful'));

            return $this->goHome();
        }
        Seo::noIndex();
        return $this->render('register', [
            'model' => $model,
        ]);
    }

    public function actionActivate($email, $key)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = Users::findByEmail($email, Users::STATUS_INACTIVE);

            if (is_null($user)) {
                throw new Exception(Yii::t('cms', 'activation_error_wrong_email'));
            }

            if (!$user->validateAuthKey($key)) {
                throw new Exception(Yii::t('cms', 'activation_error_wrong_key'));
            }

            //ok, lehet aktiválni
            if (!$user->activateRegistration()) {
                throw new Exception(Yii::t('cms', 'activation_error'));
            }

            $transaction->commit();
            Yii::$app->session->setFlash('success', Yii::t('cms', 'activation_successful'));

            // ha be van lépve és üres a jelszó
            if (!Yii::$app->user->isGuest && empty($user->password_hash)) {
                return $this->redirect(['/cms2/user/profile']);
            }
            // ha nincs belépve és üres a jelszó
            if (Yii::$app->user->isGuest && empty($user->password_hash)) {
                if(Yii::$app->user->login($user)){
                    return $this->redirect(['/cms2/user/profile']);
                }
            }

        } catch (Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['/cms2/user/login']);
    }

    public function actionReminder()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ReminderForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $user = Users::findByEmail($model->email, [Users::STATUS_ACTIVE, Users::STATUS_INACTIVE]);
                $user->generatePasswordResetToken();
                if ($user->save()) {
                    $user->sendReminderMail();
                    Yii::$app->session->setFlash('success', Yii::t('cms', 'reminder_email_sent'));

                    return $this->redirect(['/cms2/user/login']);
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('cms', 'reminder_error'));
                }
            }
        }
        Seo::noIndex();
        return $this->render('reminder', [
            'model' => $model,
        ]);
    }

    public function actionSetnewpassword($email, $key)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = Users::findByPasswordResetToken($key);
        if (!is_null($user) && $user->email === $email) {
            // talált usert
            // megváltoztathatja a jelszavát
            $model = new SetNewPasswordForm();
            $model->email = $email;

            if ($model->load(Yii::$app->request->post())) {
                if ($model->validate()) {
                    // ha minden ok, akkor
                    $user->setPassword($model->password);
                    $user->activateRegistration();
                    $user->removePasswordResetToken();
                    if ($user->save()) {
                        Yii::$app->session->setFlash('success', Yii::t('cms', 'new_password_successfully_changed'));
                        Yii::$app->user->login($user);
                        $this->redirect(['/cms2/user/profile']);
                    }
                }
            }
            Seo::noIndex();
            return $this->render('setnewpassword', [
                'model' => $model,
            ]);
        } else {
            Yii::$app->session->setFlash('error', Yii::t('cms', 'new_password_error_wrong_link'));

            return $this->redirect(['/cms2/user/reminder']);
        }
    }

    public function actionSettings()
    {
        Seo::noIndex();
        return $this->render('settings');
    }

    public function actionProfile()
    {
        $userEmail = Yii::$app->user->identity->email;
        $user = Users::findByEmail($userEmail, [Users::STATUS_ACTIVE, Users::STATUS_INACTIVE]);
        $ud = $user->getDetails();
        $ud->email = $user->email;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post('SetNewPasswordForm');
            if (!is_null($post)) {
                $user->changePassword(ArrayHelper::getValue($post, 'email'));
                return $this->redirect(['/cms2/user/profile']);
            }

            if (isset($_POST['remove-profile'])) {
                $user->delete();
                Yii::$app->user->logout(true);
                Yii::$app->session->setFlash('success', Yii::t('cms', 'user_profile_removed_successfully'));
                return $this->redirect(['/']);
            }

            if ($ud->load(Yii::$app->request->post()) && $ud->save()) {
                Yii::$app->session->setFlash('success', Yii::t('cms', 'user_details_update_successful'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('cms', 'user_details_update_error'));
            }
            return $this->redirect(['/cms2/user/profile']);
        }

        $form_pwd = new SetNewPasswordForm();
        $form_pwd->email = Yii::$app->user->identity->email;

        Seo::noIndex();

        return $this->render('profile', [
            'model' => $ud,
            'new_pwd_model' => $form_pwd,
        ]);
    }
}
