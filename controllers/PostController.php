<?php

namespace albertborsos\yii2cmsmultisite\controllers;

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\Site;
use albertborsos\yii2cmsmultisite\models\SitePostContent;
use albertborsos\yii2cmsmultisite\models\SitePostMeta;
use himiklab\sortablegrid\SortableGridAction;
use Yii;
use albertborsos\yii2cmsmultisite\models\SitePost;
use albertborsos\yii2cmsmultisite\models\SitePostSearch;
use albertborsos\yii2lib\web\Controller;
use yii\base\Exception;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PostController implements the CRUD actions for SitePost model.
 */
class PostController extends Controller
{
    public function init()
    {
        parent::init();
        $this->defaultAction = 'index';
        $this->name = 'Bejegyzések';
        $this->layout = '//center';
    }

    private function setBreadcrumbs($site = null, $type = null){
        $module = $this->breadcrumbs[0];
        $controller = $this->breadcrumbs[1];
        $action = $this->breadcrumbs[2];
        if(!is_null($site)){
            $site = Site::findOne(['id' => $site]);
            $this->breadcrumbs[1] = [
                'label' => $site->name,
                'url' => ['post/index', 'site' => $site->id],
            ];
        }
        if(!is_null($type)){
            $this->breadcrumbs[2] = [
                'label' => DataProvider::items('post_type', $type, false),
                'url' => ['post/index', 'site' => $site->id, 'type' => $type],
            ];

            $this->breadcrumbs[3] = $action;
        }
    }

    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => SitePost::className(),
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        'matchCallback' => function(){
                            return Yii::$app->user->can('reader');
                            }
                    ],
                    [
                        'actions' => ['create', 'update', 'delete', 'updatebyeditable', 'sort'],
                        'allow' => true,
                        'matchCallback' => function(){
                            return Yii::$app->user->can('editor');
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'           => ['post'],
                    'updatebyeditable' => ['post'],
                    'sort' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SitePost models.
     * @return mixed
     */
    public function actionIndex($site = null, $type = null)
    {
        $this->setBreadcrumbs($site, $type);
        $searchModel = new SitePostSearch();
        $params = Yii::$app->request->queryParams;
        if(!is_null($site)){
            $params['SitePostSearch']['site_id'] = $site;
        }
        $types = $type == SitePost::TYPE_BLOG ? [$type] : [
            SitePost::TYPE_MENU,
            SitePost::TYPE_MENU_LINK,
            SitePost::TYPE_DROPDOWN,
        ];

        $dataProvider = $searchModel->search($params, $types);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new SitePost model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate($site, $type)
    {
        $model = new SitePost();
        $model->site_id = $site;
        $model->type = $type;

        $this->setBreadcrumbs($site, $type);

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try{
                if ($model->save()){
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', '<h4>Bejegyzés sikeresen létrehozva!</h4>');
                    return $this->redirect(['index', 'site' => $model->site_id, 'type' => $model->type]);
                }else{
                    $model->throwNewException('Bejegyzés mentése nem sikerült!');
                }
            }catch (Exception $e){
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('create', [
        'model' => $model,
        ]);
    }

    /**
     * Updates an existing SitePost model.
     * If update is successful, the browser will be redirected to the 'update' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $this->setBreadcrumbs($model->site_id, $model->type);

        $transaction = Yii::$app->db->beginTransaction();
        if ($model->load(Yii::$app->request->post())) {
            try{
                if ($model->save()){
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', '<h4>Bejegyzés sikeresen módosítva!</h4>');
                    return $this->redirect(['update', 'id' => $model->id]);
                }else{
                    $model->throwNewException('Bejegyzés módosítása nem sikerült!');
                }
            }catch (Exception $e){
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SitePost model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try{
            $post = $this->findModel($id);
            $site_id = $post->site_id;

            if($post->sort_order == 1){
                throw new Exception('<h4>A kezdőlap nem törölhető!</h4>');
            }

            $children = SitePost::findAll(['parent_post_id' => $post->id]);

            if(count($children) > 0){
                throw new Exception('<h4>Előbb törölni kell a menühöz tartozó menüpontokat!</h4>');
            }

            $metas = SitePostMeta::findAll([
                'site_post_id' => $post->id
            ]);

            foreach ($metas as $meta) {
                $meta->delete();
            }

            $contents = SitePostContent::findAll([
                'site_post_id' => $post->id
            ]);

            foreach ($contents as $content) {
                $content->delete();
            }

            if(!$post->delete()){
                $post->throwNewException('Bejegyzés törlése nem sikerült!');
            }

            Yii::$app->session->setFlash('success', '<h4>Bejegyzés sikeresen törölve!</h4>');
            $transaction->commit();

        }catch (Exception $e){
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index', 'site' => $site_id]);
    }

    /**
     * Finds the SitePost model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SitePost the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SitePost::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdatebyeditable(){
		$key       = Yii::$app->request->post('pk');
		$id        = unserialize(base64_decode($key));
        $attribute = Yii::$app->request->post('name');
        $value     = Yii::$app->request->post('value');

        $model = $this->findModel($id);
        try{
            if (!is_null($model)){
                $model->$attribute = $value;
                if (!$model->save()){
                    throw new Exception('Bejegyzés módosítása nem sikerült');
                }
            }else{
                throw new Exception('Nem létezik ilyen rekord!');
            }
        }catch (Exception $e){
            throw new HttpException(400,$e->getMessage());
        }
    }
}
