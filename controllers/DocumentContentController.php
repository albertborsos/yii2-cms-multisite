<?php

namespace albertborsos\yii2cmsmultisite\controllers;

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\Document;
use Yii;
use albertborsos\yii2cmsmultisite\models\DocumentContent;
use albertborsos\yii2cmsmultisite\models\DocumentContentSearch;
use albertborsos\yii2lib\web\Controller;
use yii\base\Exception;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * DocumentContentController implements the CRUD actions for DocumentContent model.
 */
class DocumentContentController extends Controller
{
    public function init()
    {
        parent::init();
        $this->defaultAction = 'index';
        $this->name = 'Dokumentum tartalmak';
        $this->layout = '//center';

        $this->addActionNames(['edit' => 'Szerkesztés']);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        'matchCallback' => function(){
                            return Yii::$app->user->can('reader');
                            }
                    ],
                    [
                        'actions' => ['edit', 'delete', 'updatebyeditable'],
                        'allow' => true,
                        'matchCallback' => function(){
                            return Yii::$app->user->can('editor');
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'           => ['post'],
                    'updatebyeditable' => ['post'],
                ],
            ],
        ];
    }

    private function setBreadcrumbs($document = null, $language_code = null){
        if(!is_null($document) && !is_null($language_code)){
            $module = $this->breadcrumbs[0];
            $controller = $this->breadcrumbs[1];
            $action = $this->breadcrumbs[2];

            $document = Document::findOne(['id' => $document]);
            $language = DataProvider::items('languages', $language_code);

            $this->breadcrumbs[1] = [
                'label' => 'Dokumentumok',
                'url' => ['document/index', 'type' => $document->type],
            ];

            $this->breadcrumbs[2] = [
                'label' => '"' . $document->name . '" szerkesztése ('.$language.')',
                'url' => null,
            ];
        }
    }

    /**
     * Lists all DocumentContent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DocumentContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DocumentContent model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionEdit($document, $language_code, $rev = null)
    {
        if(!is_null($rev)){
            $model = DocumentContent::findOne(['id' => $rev]);
        }else{
            $model = DocumentContent::find()->where([
                'document_id' => $document,
                'language_code' => $language_code,
            ])->orderBy('id DESC')->one();
        }
        if(is_null($model)){
            $model = new DocumentContent();
            $model->document_id = $document;
            $model->language_code = $language_code;
        }
        $model->id = null;
        $model->setIsNewRecord(true);

        $this->setBreadcrumbs($document, $language_code);

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try{
                if ($model->save()){
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', '<h4>Dokumentum tartalom sikeresen létrehozva!</h4>');
                    return $this->redirect(['edit', 'document' => $model->document_id, 'language_code' => $model->language_code]);
                }else{
                    $model->throwNewException('Dokumentum tartalom mentése nem sikerült!');
                }
            }catch (Exception $e){
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $searchModel = new DocumentContentSearch();
        $params = Yii::$app->request->queryParams;
        $params['DocumentContentSearch']['document_id'] = $model->document_id;
        $params['DocumentContentSearch']['language_code'] = $model->language_code;
        $dataProvider = $searchModel->search($params);

        $model->document->showAlerts($language_code);

        return $this->render('edit', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Deletes an existing DocumentContent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try{
            $this->findModel($id)
            ->delete();
        }catch (Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the DocumentContent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DocumentContent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DocumentContent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdatebyeditable(){
		$key       = Yii::$app->request->post('pk');
		$id        = unserialize(base64_decode($key));
        $attribute = Yii::$app->request->post('name');
        $value     = Yii::$app->request->post('value');

        $model = $this->findModel($id);
        try{
            if (!is_null($model)){
                $model->$attribute = $value;
                if (!$model->save()){
                    throw new Exception('Dokumentum tartalom módosítása nem sikerült');
                }
            }else{
                throw new Exception('Nem létezik ilyen rekord!');
            }
        }catch (Exception $e){
            throw new HttpException(400,$e->getMessage());
        }
    }
}
