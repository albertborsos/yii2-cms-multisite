<?php

namespace albertborsos\yii2cmsmultisite\controllers;

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\SiteLanguage;
use albertborsos\yii2cmsmultisite\models\SitePost;
use albertborsos\yii2lib\helpers\Date;
use Yii;
use albertborsos\yii2cmsmultisite\models\SitePostContent;
use albertborsos\yii2cmsmultisite\models\SitePostContentSearch;
use albertborsos\yii2lib\web\Controller;
use yii\base\Exception;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PostContentController implements the CRUD actions for SitePostContent model.
 */
class PostContentController extends Controller
{
    public function init()
    {
        parent::init();
        $this->defaultAction = 'index';
        $this->name = 'Bejegyzéstartalmak';
        $this->layout = '//center';
        $this->addActionNames(['edit' => 'Szerkesztés']);
    }

    private function setBreadcrumbs($post = null, $language = null){
        if(!is_null($post) && !is_null($language)){
            $module = $this->breadcrumbs[0];
            $controller = $this->breadcrumbs[1];
            $action = $this->breadcrumbs[2];

            $post = SitePost::findOne(['id' => $post]);
            $language = SiteLanguage::findOne(['id' => $language]);

            $this->breadcrumbs[1] = [
                'label' => $post->site->name,
                'url' => ['post/index', 'site' => $post->site->id],
            ];

            $this->breadcrumbs[2] = [
                'label' => DataProvider::items('post_type', $post->type, false),
                'url' => ['post/index', 'site' => $post->site->id, 'type' => $post->type],
            ];

            $this->breadcrumbs[3] = $post->name . ' szerkesztése ('.$language->name.')';
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        'matchCallback' => function(){
                            return Yii::$app->user->can('reader');
                            }
                    ],
                    [
                        'actions' => ['edit', 'delete', 'updatebyeditable'],
                        'allow' => true,
                        'matchCallback' => function(){
                            return Yii::$app->user->can('editor');
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'           => ['post'],
                    'updatebyeditable' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SitePostContent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SitePostContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new SitePostContent model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionEdit($post, $language_id, $rev = null)
    {
        if(!is_null($rev)){
            $model = SitePostContent::findOne(['id' => $rev]);
        }else{
            $model = SitePostContent::find()->where([
                'site_post_id' => $post,
                'site_language_id' => $language_id,
            ])->orderBy('id DESC')->one();
        }
        if(is_null($model)){
            $model = new SitePostContent();
            $model->site_post_id = $post;
            $model->site_language_id = $language_id;
            $model->date_show = Date::timestampToDate(time());
        }
        $model->id = null;
        $model->published = DataProvider::YES;
        $model->setIsNewRecord(true);

        $this->setBreadcrumbs($post, $language_id);

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try{
                if ($model->save()){
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', '<h4>Bejegyzéstartalom sikeresen létrehozva!</h4>');
                    return $this->redirect(['edit', 'post' => $model->site_post_id, 'language_id' => $model->site_language_id]);
                }else{
                    $model->throwNewException('Bejegyzéstartalom mentése nem sikerült!');
                }
            }catch (Exception $e){
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $searchModel = new SitePostContentSearch();
        $params = Yii::$app->request->queryParams;
        $params['SitePostContentSearch']['site_post_id'] = $model->site_post_id;
        $params['SitePostContentSearch']['site_language_id'] = $model->site_language_id;
        $dataProvider = $searchModel->search($params);

        $model->sitePost->showAlerts($language_id);

        return $this->render('edit', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Deletes an existing SitePostContent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try{
            $this->findModel($id)
            ->delete();
        }catch (Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the SitePostContent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SitePostContent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SitePostContent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdatebyeditable(){
		$key       = Yii::$app->request->post('pk');
		$id        = unserialize(base64_decode($key));
        $attribute = Yii::$app->request->post('name');
        $value     = Yii::$app->request->post('value');

        $model = $this->findModel($id);
        try{
            if (!is_null($model)){
                $model->$attribute = $value;
                if (!$model->save()){
                    throw new Exception('Bejegyzéstartalom módosítása nem sikerült');
                }
            }else{
                throw new Exception('Nem létezik ilyen rekord!');
            }
        }catch (Exception $e){
            throw new HttpException(400,$e->getMessage());
        }
    }
}
