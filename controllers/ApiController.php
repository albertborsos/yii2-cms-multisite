<?php

namespace albertborsos\yii2cmsmultisite\controllers;

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\Gallery;
use albertborsos\yii2cmsmultisite\models\GalleryDocument;
use albertborsos\yii2lib\helpers\Seo;
use vova07\imperavi\actions\GetImagesAction;
use vova07\imperavi\actions\UploadFileAction;
use Yii;
use yii\caching\CacheInterface;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\Session;

class ApiController extends \yii\rest\Controller
{
    public function actions()
    {
        return [
            'images-get' => [
                'class' => GetImagesAction::class,
                'url' => Url::to(['/uploads/images/'], true), // Directory URL address, where files are stored.
                'path' => $this->module->galleryPath, // Or absolute path to directory where files are stored.
                'options' => [
                    'recursive' => false,
                ]
            ],
            'image-upload' => [
                'class' => UploadFileAction::class,
                'url' => Url::to(['/uploads/images/'], true), // Directory URL address, where files are stored.
                'path' => $this->module->galleryPath, // Or absolute path to directory where files are stored.
            ],
        ];
    }


    public function actionMigrateUp(CacheInterface $cache, Response $response, Session $session){
        $content = DataProvider::migrateUp();
        Seo::noIndex();
        $cache->flush();
        $session->setFlash('success', $content);
        $this->redirect(['/']);
    }

    public function actionRenderAjaxGallery($id, $page = null){
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var Gallery $model */
        $model = Gallery::findOne(['id' => $id]);

        return $model->generate($page);
    }

    public function actionLoadOtherImages($id, $activePage){
        //Yii::$app->response->getHeaders()->set('Vary', 'Accept');
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Gallery $gallery */
        $gallery = Gallery::findOne(['id' => $id]);

        $query = $gallery->getGalleryDocuments();
        $totalNum = $query->count();

        $dataProvider = new ActiveDataProvider([
            'query' => $query->limit($activePage*$gallery->page_size)->offset(0),
            'pagination' => false,
        ]);

        $links['before'] = \albertborsos\yii2cmsmultisite\components\Gallery::renderLinksOnly($dataProvider);

        // lekérdezem azokat a képeket, amik az aktuális oldal után vannak
        $dataProvider = new ActiveDataProvider([
            'query' => $query->limit(null)->offset(($activePage+1)*$gallery->page_size),
            'pagination' => false,
        ]);

        $links['after'] = \albertborsos\yii2cmsmultisite\components\Gallery::renderLinksOnly($dataProvider);

        return $links;
    }
}
