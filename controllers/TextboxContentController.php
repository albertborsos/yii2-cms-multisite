<?php

namespace albertborsos\yii2cmsmultisite\controllers;

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\Textbox;
use Yii;
use albertborsos\yii2cmsmultisite\models\TextboxContent;
use albertborsos\yii2cmsmultisite\models\TextboxContentSearch;
use albertborsos\yii2lib\web\Controller;
use yii\base\Exception;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * TextboxContentController implements the CRUD actions for TextboxContent model.
 */
class TextboxContentController extends Controller
{
    public function init()
    {
        parent::init();
        $this->defaultAction = 'index';
        $this->name = 'Szövegdoboz tartalmak';
        $this->layout = '//center';
        $this->addActionNames([
            'edit' => 'Szerkesztés',
        ]);
    }

    private function setBreadcrumbs($textbox = null, $language_code = null){
        if(!is_null($textbox) && !is_null($language_code)){
            $textbox = Textbox::findOne(['id' => $textbox]);
            $language = DataProvider::items('languages', $language_code);

            $this->breadcrumbs[1] = [
                'label' => 'Szövegdobozok',
                'url' => ['textbox/index']
            ];

            $this->breadcrumbs[2] = [
                'label' => '"' . $textbox->name . '" szerkesztése ('.$language.')',
                'url' => null,
            ];
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow'   => true,
                        'matchCallback' => function(){
                            return Yii::$app->user->can('reader');
                            }
                    ],
                    [
                        'actions' => ['edit', 'delete', 'updatebyeditable'],
                        'allow' => true,
                        'matchCallback' => function(){
                            return Yii::$app->user->can('editor');
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'           => ['post'],
                    'updatebyeditable' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TextboxContent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TextboxContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionEdit($textbox, $language_code, $rev = null)
    {
        if(!is_null($rev)){
            $model = TextboxContent::findOne(['id' => $rev]);
        }else{
            $model = TextboxContent::find()->where([
                'textbox_id' => $textbox,
                'site_language_code' => $language_code,
            ])->orderBy('id DESC')->one();
        }
        if(is_null($model)){
            $model = new TextboxContent();
            $model->textbox_id = $textbox;
            $model->site_language_code = $language_code;
        }
        $model->id = null;
        $model->published = DataProvider::YES;
        $model->setIsNewRecord(true);

        $this->setBreadcrumbs($textbox, $language_code);

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try{
                if ($model->save()){
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', '<h4>Szövegdoboz tartalom sikeresen létrehozva!</h4>');
                    return $this->redirect(['edit', 'textbox' => $model->textbox_id, 'language_code' => $model->site_language_code]);
                }else{
                    $model->throwNewException('Szövegdoboz tartalom mentése nem sikerült!');
                }
            }catch (Exception $e){
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $searchModel = new TextboxContentSearch();
        $params = Yii::$app->request->queryParams;
        $params['TextboxContentSearch']['textbox_id'] = $model->textbox_id;
        $params['TextboxContentSearch']['site_language_code'] = $model->site_language_code;
        $dataProvider = $searchModel->search($params);

        $model->textbox->showAlerts($language_code);

        return $this->render('edit', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Deletes an existing TextboxContent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try{
            $this->findModel($id)
            ->delete();
        }catch (Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the TextboxContent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TextboxContent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TextboxContent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdatebyeditable(){
		$key       = Yii::$app->request->post('pk');
		$id        = unserialize(base64_decode($key));
        $attribute = Yii::$app->request->post('name');
        $value     = Yii::$app->request->post('value');

        $model = $this->findModel($id);
        try{
            if (!is_null($model)){
                $model->$attribute = $value;
                if (!$model->save()){
                    throw new Exception('Szövegdoboz tartalom módosítása nem sikerült');
                }
            }else{
                throw new Exception('Nem létezik ilyen rekord!');
            }
        }catch (Exception $e){
            throw new HttpException(400,$e->getMessage());
        }
    }
}
