<?php

namespace albertborsos\yii2cmsmultisite\controllers;

use albertborsos\yii2cmsmultisite\models\Document;
use albertborsos\yii2cmsmultisite\models\DocumentSearch;
use albertborsos\yii2cmsmultisite\models\GalleryDocument;
use albertborsos\yii2lib\web\Controller;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class DocumentController extends Controller
{
    const FILE_SIZE_5MB = 5000000;

    public function init()
    {
        parent::init();
        $this->defaultAction = 'index';
        $this->name = 'Dokumentumok';
        $this->layout = '//center';

        $this->addActionNames(['upload' => 'Feltöltés']);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'matchCallback' => function () {
                            return Yii::$app->user->can('reader');
                        }
                    ],
                    [
                        'actions' => ['upload', 'upload-ajax', 'delete', 'updatebyeditable'],
                        'allow' => true,
                        'matchCallback' => function () {
                            return Yii::$app->user->can('editor');
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'updatebyeditable' => ['post'],
                    'upload-ajax' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Document models.
     * @return mixed
     */
    public function actionIndex($type = Document::TYPE_IMAGE)
    {
        $this->breadcrumbs[2] = [
            'label' => Document::TYPE_LABELS[$type],
        ];
        $searchModel = new DocumentSearch();
        $dataProvider = $searchModel->search(ArrayHelper::merge(
            Yii::$app->request->queryParams,
            ['DocumentSearch' => ['type' => $type]])
        );

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'hideButtons' => false,
            'label' => Document::TYPE_LABELS[$type],
        ]);
    }

    public function actionUpload($type = Document::TYPE_IMAGE)
    {
        $this->layout = '//fluid';

        switch ($type) {
            case Document::TYPE_DOCUMENT:
                $accept = 'application/pdf';
                $maxFileSize = 10 * self::FILE_SIZE_5MB;
                break;
            default:
                $accept = 'image/*';
                $maxFileSize = 2 * self::FILE_SIZE_5MB;
                break;
        }

        $searchModel = new DocumentSearch();
        $dataProvider = $searchModel->search(ArrayHelper::merge(
            Yii::$app->request->queryParams,
            ['DocumentSearch' => ['type' => $type]])
        );

        return $this->render('upload', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type,
            'accept' => $accept,
            'maxFileSize' => $maxFileSize,
            'hideButtons' => true,
        ]);
    }

    public function actionUploadAjax($type)
    {
        try {
            Yii::$app->response->getHeaders()->set('Vary', 'Accept');
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = [];
            $uploaded = UploadedFile::getInstanceByName('filename');
            if (!is_null($uploaded)) {
                $document = new Document();
                $document->type = $type;
                $extension = $document->setExtension($uploaded);
                $document->filename = $uploaded->baseName;
                $document->generateUniqueName($extension);
                $document->status = Document::STATUS_ACTIVE;
                if ($document->validate()) {
                    if ($document->saveUploadedFile($uploaded)) {
                        switch ($type) {
                            case Document::TYPE_IMAGE:
                            case Document::TYPE_VIDEO_PREVIEW:
                            case Document::TYPE_DOCUMENT_PREVIEW:
                                $document->savePhoto($document->getPathFull(), 1980);
                                $document->savePhoto($document->getPathFull(), 640, true);
                                $thumbnail = $document->getUrlFull(true);
                                break;
                            case Document::TYPE_DOCUMENT:
                                $thumbnail = null;
                                break;
                        }
                        //Now we return our json
                        $response['files'][] = [
                            'name' => $document->filename,
                            'type' => $uploaded->type,
                            'size' => $uploaded->size,
                            'url' => $document->getUrlFull(),
                            'thumbnailUrl' => $thumbnail,
                            'deleteUrl' => $document->getUrlDelete(),
                            'deleteType' => 'POST'
                        ];
                    }
                } else {
                    $document->throwNewException('Hibás kép attribútum!');
                }
            } else {
                throw new Exception('Nem érkezett kép!');
            }

        } catch (Exception $e) {
            $response['files'][] = [
                'error' => strip_tags($e->getMessage()),
            ];
        }
        return $response;
    }

    /**
     * Displays a single Document model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing Document model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $model = $this->findModel($id);

            foreach ($model->documentContents as $documentContent) {
                $documentContent->delete();
            }

            // törölni kell a galériákból is
            $assigns = GalleryDocument::findAll(['document_id' => $model->id]);
            foreach ($assigns as $assign) {
                $assign->delete();
            }

            if ($model->deleteFiles()) {
                $model->delete();
            }
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index', 'type' => $model->type]);
    }

    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Document the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Document::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdatebyeditable()
    {
        $key = Yii::$app->request->post('pk');
        $id = unserialize(base64_decode($key));
        $attribute = Yii::$app->request->post('name');
        $value = Yii::$app->request->post('value');

        $model = $this->findModel($id);
        try {
            if (!is_null($model)) {
                $model->$attribute = $value;
                if (!$model->save()) {
                    throw new Exception('Dokumentum módosítása nem sikerült');
                }
            } else {
                throw new Exception('Nem létezik ilyen rekord!');
            }
        } catch (Exception $e) {
            throw new HttpException(400, $e->getMessage());
        }
    }
}
