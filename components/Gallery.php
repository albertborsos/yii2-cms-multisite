<?php

namespace albertborsos\yii2cmsmultisite\components;

use albertborsos\yii2cmsmultisite\models\GalleryDocument;
use albertborsos\yii2lib\bootstrap\Carousel;
use albertborsos\yii2lib\helpers\S;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\LinkPager;

class Gallery extends \yii\base\Widget
{

    const TYPE_GALLERY = 'gallery';
    const TYPE_CAROUSEL = 'carousel';

    public $htmlOptions = [];
    public $pluginOptions = [];

    public $header;
    public $galleryId;

    public $pager;
    public $page = 0;
    public $pageSize = 20;
    public $itemNumInRow = 3;
    public $order = 'ASC';

    public $photoWrapperClass = 'col-sm-3';

    public $displayControl = false;

    private $galleryType = self::TYPE_GALLERY;

    public function init()
    {
        parent::init();
        $this->htmlOptions['id'] = S::get($this->htmlOptions, 'id', $this->getId());
        $this->pluginOptions['container'] = '#' . $this->htmlOptions['id'] . '-gallery';
        $this->setPhotoWrapperClass();
        if ($this->pageSize == 1) {
            $this->galleryType = self::TYPE_CAROUSEL;
        }
    }

    private function setPhotoWrapperClass()
    {
        $this->photoWrapperClass = 'col-sm-' . round(12 / $this->itemNumInRow);
    }

    public function run()
    {
        switch ($this->galleryType) {
            case self::TYPE_GALLERY:
                $this->renderGallery();
                break;
            case self::TYPE_CAROUSEL:
                $this->renderCarousel();
                break;
        }
    }

    public function renderGallery()
    {
        echo Html::beginTag('div', ['id' => 'gallery-container', 'data-id' => $this->galleryId, 'style' => 'text-align:center;', 'data-pagesize' => $this->pageSize]);
        echo Html::beginTag('div', ['id' => 'gallery-' . $this->galleryId . '-before', 'style' => 'display:none;']);
        echo Html::endTag('div');
        if (!is_null($this->header)) {
            echo Html::tag('h3', $this->header, ['style' => 'text-align:center;']);
        }
        $this->renderLinks();
        echo Html::beginTag('div', ['id' => 'gallery-' . $this->galleryId . '-after', 'style' => 'display:none;']);
        echo Html::endTag('div');
        echo Html::endTag('div');
    }

    public function renderCarousel()
    {
        $items = $this->renderCarouselItems();
        if (!is_null($this->header) && $this->header !== '') {
            echo Html::tag('h3', $this->header, ['style' => 'text-align:center;']);
        }
        echo Carousel::widget(['items' => $items, 'showIndicators' => false]);
    }

    private function getGallery(): \albertborsos\yii2cmsmultisite\models\Gallery
    {
        return \albertborsos\yii2cmsmultisite\models\Gallery::findOne($this->galleryId);
    }

    private function renderLinks()
    {
        $query = $this->getGallery()->getGalleryDocuments();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize,
                'totalCount' => $query->count(),
                'page' => $this->page,
                'route' => 'cms2/api/render-ajax-gallery',
                'params' => false,
            ],
        ]);

        $models = $dataProvider->getModels();

        echo $this->getPager($dataProvider->getPagination());
        echo Html::beginTag('div', $this->htmlOptions);
        $n = 0;
        /** @var GalleryDocument $model */
        foreach ($models as $model) {
            if ($n == 0) {
                echo Html::beginTag('div', ['class' => 'row']);
            }

            $document = \Yii::$app->db->cache(function ($db) use ($model) {
                return $model->document;
            });

            $url = $document->getUrlFull();
            $url = $model->url ? $model->url : $url;

            $src = $document->getUrlFull(true);
            if ($model->url) {
                $options = [
                    'title' => $document->getContent()->title,
                    'target' => strpos($model->url, \Yii::$app->urlManager->getBaseUrl()) == false ? false : '_blank',
                ];
            } else {
                $options = [
                    'data-rel' => 'lightbox[gallery-' . $model->gallery_id . ']',
                    'title' => $document->getContent()->title,
                ];
            }

            echo Html::beginTag('div', ['class' => $this->photoWrapperClass . ' photo-item']);
            echo Html::a(Html::img($src, ['class' => 'img-responsive', 'alt' => $document->getContent()->alt_description]), $url, $options);
            echo Html::endTag('div');

            $n++;
            if ($n == $this->itemNumInRow) {
                echo Html::endTag('div');
                $n = 0;
            }
        }
        if ($n !== 0) {
            echo Html::endTag('div');
        }
        echo Html::endTag('div');
        echo $this->getPager($dataProvider->getPagination());
    }

    private function getPager($pagination)
    {
        if (is_null($this->pager)) {
            $this->pager = LinkPager::widget([
                'pagination' => $pagination,
                'options' => ['class' => 'pagination', 'style' => 'margin:20px auto;'],
            ]);
        }
        return $this->pager;
    }

    /**
     * @param \yii\data\ActiveDataProvider $dataProvider
     * @return string
     */
    public static function renderLinksOnly($dataProvider)
    {
        $links = '';
        $models = $dataProvider->getModels();

        /** @var GalleryDocument $model */
        foreach ($models as $model) {
            $url = $model->document->getUrlFull();
            $options = [
                'data-rel' => 'lightbox[gallery-' . $model->gallery_id . ']',
                'title' => $model->document->getContent()->title,
            ];

            $links .= Html::a('', $url, $options);
        }

        return $links;
    }

    private function renderCarouselItems()
    {
        $query = GalleryDocument::find()
            ->where(['status' => GalleryDocument::STATUS_ACTIVE, 'gallery_id' => $this->galleryId])
            ->orderBy($this->getOrderBy());

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $models = $dataProvider->getModels();

        $items = [];
        /** @var GalleryDocument $model */
        foreach ($models as $model) {
            $document = \Yii::$app->db->cache(function ($db) use ($model) {
                return $model->document;
            });
            $img = Html::img($document->getUrlFull(), [
                'alt' => $document->getContent()->title,
                'style' => 'width:100%;'
            ]);

            if (!is_null($model->url)) {
                $content = Html::a($img, $model->url, [
                    'title' => $document->getContent()->title,
                    'target' => '_blank',
                ]);
            } else {
                $content = $img;
            }

            $items[] = [
                'content' => $content,
                'caption' => $document->getContent()->title,
            ];
        }
        return $items;
    }

    private function getOrderBy(): array
    {
        $sortOrder = $this->order == 'ASC'
            ? ['sort_order' => SORT_ASC]
            : ['sort_order' => SORT_DESC];

        $idOrder = $this->order = 'ASC'
            ? ['id' => SORT_DESC]
            : ['id' => SORT_ASC];

        return array_merge($sortOrder, $idOrder);
    }
}