<?php

namespace albertborsos\yii2cmsmultisite\components;

use albertborsos\yii2cmsmultisite\models\Site;
use albertborsos\yii2cmsmultisite\models\SiteLanguage;
use albertborsos\yii2cmsmultisite\models\SitePost;
use albertborsos\yii2cmsmultisite\models\SitePostContent;
use albertborsos\yii2cmsmultisite\models\Users;
use albertborsos\yii2cmsmultisite\Module;
use rmrevin\yii\fontawesome\FA;
use vova07\console\ConsoleRunner;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;

class DataProvider {

    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_DELETED  = 2;

    const YES = 1;
    const NO = 0;

    public static function items($category, $id = null, $returnArray = true)
    {
        $array = array();
        switch ($category) {
            case 'site_type':
                $array = [
                    Site::TYPE_WEBSITE => 'Weboldal',
                    Site::TYPE_SUBDOMAIN => 'Aloldal',
                ];
                break;
            case 'languages':
                $array = [
                    'hu' => 'magyar',
                    'en' => 'angol',
                    'de' => 'német',
                ];
                break;
            case 'status':
                $array = [
                    self::STATUS_ACTIVE   => 'Aktív',
                    self::STATUS_INACTIVE => 'Inaktív',
                    self::STATUS_DELETED  => 'Törölt',
                ];
                break;
            case 'post_status':
                $array = [
                    SitePost::STATUS_ACTIVE   => 'Aktív',
                    SitePost::STATUS_INACTIVE => 'Nem listázott',
                    SitePost::STATUS_DELETED  => 'Törölt',
                ];
                break;
            case 'post_type':
                $array = [
                    SitePost::TYPE_BLOG     => 'Blog bejegyzés',
                    SitePost::TYPE_MENU     => 'Menüpont',
                    SitePost::TYPE_DROPDOWN => 'Legördülő Menü',
                    SitePost::TYPE_MENU_LINK => 'Menülink',
                ];
                break;
            case 'yesno':
                $array = [
                    '1' => 'Igen',
                    '0' => 'Nem',
                ];
                break;
            case 'noyes':
                $array = [
                    '0' => 'Nem',
                    '1' => 'Igen',
                ];
                break;
            case 'order':
                $array = [
                    'ASC'  => 'Régebbiek elöl',
                    'DESC' => 'Frissek elöl',
                ];
                break;
            case 'sort_order':
                for($i = 1; $i <= 99; $i++){
                    $array[$i] = $i;
                }
                break;
            case 'pagesize':
                $sizes = self::items('itemsinarow');
                for($i = 1; $i <5; $i++){
                    foreach($sizes as $size){
                        $array[$size*$i] = $size*$i;
                    }
                }
                asort($array);
                break;
            case 'itemsinarow':
                $array = [
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    6 => 6,
                    12 => 12,
                ];
                break;
            case 'meta-robots':
                $array = [
                    'INDEX'             => 'INDEX',
                    'NOINDEX'           => 'NOINDEX',
                    'NOINDEX, NOFOLLOW' => 'NOINDEX, NOFOLLOW',
                ];
                break;
            case 'roles':
                $array = array(
                    'guest'  => 'Vendég',
                    'reader' => 'Olvasó',
                    'editor' => 'Szerkesztő',
                    'admin'  => 'Adminisztrátor',
                );
                break;
            case 'status_user':
                $array = array(
                    Users::STATUS_ACTIVE   => 'Aktív',
                    Users::STATUS_INACTIVE => 'Inaktív',
                    Users::STATUS_DELETED  => 'Törölt',
                );
                break;
        }
        if (is_null($id) && $returnArray) {
            return $array;
        } else {
            return isset($array[$id]) ? $array[$id] : $id;
        }
    }

    public static function replaceCharsToUrl($string): string
    {
        $string = strip_tags($string);
        $slug = Inflector::slug($string);
        $slug = str_replace(['.html', '.php', '.htm'], '', $slug);
        return $slug;
    }

    public static function renderItems($menus = [], $parents = [], &$printedItems = [], $level = 0)
    {
        $level++;
        $menuItems = [];
        if (empty($menus)) $menus = self::getMenusByType('main');
        if (empty($parents)) $parents = self::getMenusByType('parents');
        /** @var SitePost $menu */
        foreach ($menus as $menu) {
            if (empty($menu->right_to_view) || Yii::$app->user->can($menu->right_to_view)) {
                if (array_key_exists($menu['id'], $parents) && !array_key_exists($menu['id'], $printedItems)) {
                    $printedItems[$menu->id] = 'printed';
                    // ha szülő és még nem volt kiírva, akkor le kell generálni a gyermekeit
                    $children = self::getMenusByType('belongsTo', $menu->id);
                    if (count($children) > 0) {
                        if ($level == 1) {
                            $menuItems[] = [
                                'label' => $menu->getContent()->name,
                                'url' => '#',
                                'linkOptions' => [
                                    'class' => 'dropdown-toggle',
                                ],
                                'items' => self::renderItems($children, $parents, $printedItems, $level),
                                'options' => [
                                    'class' => $menu->menu_item_class,
                                ],
                            ];
                        } else {
                            $menuItems[] = [
                                'label' => $menu->getContent()->name,
                                'url' => '#',
                                'items' => self::renderItems($children, $parents, $printedItems, $level),
                                'options' => [
                                    'class' => $menu->menu_item_class,
                                ],
                            ];
                        }
                    }
                } else {
                    // ha nem szülő, akkor nem lesz hozzá dropdown
                    $url = $menu->generateUrl();
                    $linkOptions = [
                        'title' => $menu->getMeta()->title,
                    ];
                    $baseUrl = Url::to(['/'], 'https');
                    $compareUrl = Url::to($url, 'https');
                    if ($baseUrl && strpos($compareUrl, $baseUrl) === false) {
                        $linkOptions['target'] = '_blank';
                    }

                    $menuItems[] = [
                        'label' => $menu->getContent()->name,
                        'url' => $url,
                        'linkOptions' => $linkOptions,
                        'options' => [
                            'class' => $menu->menu_item_class,
                        ],
                    ];
                }
            }
        }

        return $menuItems;
    }

    public static function addLanguageSelector(){
        $menuItem = [];
        if(Yii::$app->getModule('cms2')->showLanguageSelector){
            $query = SitePostContent::find()
                ->select('site_language_id')
                ->from(SitePostContent::tableName() . ' spc')
                ->join('JOIN', SitePost::tableName() . ' sp', 'sp.id=spc.site_post_id')
                ->distinct()
                ->where([
                    'spc.published' => 1,
                    'sp.status' => SitePost::STATUS_ACTIVE,
                ]);
            $isMultiLanguage = (int)$query->count() >= 2;

            if($isMultiLanguage){
                $languageIDs = array_values(ArrayHelper::map($query->all(), 'site_language_id', 'site_language_id'));
                $languages = SiteLanguage::findAll(['id' => $languageIDs]);
                $items = [];
                foreach ($languages as $language) {
                    if($language->code != Yii::$app->language){
                        $items[] = [
                            'label' => Yii::t('cms', $language->code),
                            'url' => Yii::$app->urlManager->getBaseUrl() . '/' . $language->code,
                        ];
                    }
                }

                if(count($items) < 1){
                    return [];
                }else{
                    $menuItem[] = [
                        'label' => FA::icon(FA::_GLOBE) . '',
                        'url' => '#',
                        'linkOptions' => [
                            'class' => 'dropdown-toggle',
                            'data-toggle' => 'dropdown',
                        ],
                        'items' => $items,
                    ];
                }
            }
        }
        return $menuItem;
    }

    public static function getMenusByType($type = 'main', $belongsTo = null){
        $posts = SitePost::find()->distinct()->from([
            'sp' => SitePost::tableName()
        ])->where([
            'sp.site_id' => Yii::$app->getModule('cms2')->site,
            'sp.type' => [SitePost::TYPE_MENU, SitePost::TYPE_MENU_LINK, SitePost::TYPE_DROPDOWN],
            'sp.status' => SitePost::STATUS_ACTIVE,
            'sl.code' => Yii::$app->language,
        ]);

        switch($type){
            case 'main':
                $posts->andWhere(['sp.parent_post_id' => NULL]);
                break;
            case 'belongsTo':
                $posts->andWhere(['sp.parent_post_id' => $belongsTo]);
                $posts->andWhere('(SELECT count(*)
                    FROM '.SitePostContent::tableName()." spc2 where (spc2.`site_post_id`=sp.id OR spc2.`site_post_id`=COALESCE(sp.link_post_id, '')) AND spc.published=:published) > 0", [
                    ':published' => DataProvider::YES,
                ]);
                break;
            case 'parents':
                $posts->andWhere('sp.parent_post_id IS NOT NULL');
                break;
        }
        $posts->join('JOIN', SitePostContent::tableName().' spc', "(sp.id=spc.site_post_id OR COALESCE(sp.link_post_id, '')=spc.site_post_id)")
            ->join('JOIN', SiteLanguage::tableName().' sl', 'sl.id=spc.site_language_id')
            ->orderBy('sp.sort_order ASC');

        if ($type !== 'parents'){
            return $posts->all();
        }else{
            return ArrayHelper::map($posts->asArray()->all(), 'parent_post_id', 'parent_post_id');
        }
    }

    public static function migrateUp()
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('cms2');
        $yiiPath = Yii::getAlias($module->consolePath);

        $cr = new ConsoleRunner([
            'file' => $module->consolePath,
            'phpBinaryPath' => $module->phpBinPath,
        ]);
        $cr->run('migrate/up --interactive=0 --color=0');
        return 'Done';
    }

    public static function checkUrlIsCorrect($urlCorrect){
        $urlActual  = Yii::$app->request->getAbsoluteUrl();
        $queryString = Yii::$app->request->queryString;

        if (Yii::$app->request->get('redirect', true) == 0) {
            $urlActual = str_replace('?' . $queryString, '', $urlActual);
        }

        if ($urlActual !== $urlCorrect){
            return Yii::$app->controller->redirect($urlCorrect, 301);
        }
    }
} 