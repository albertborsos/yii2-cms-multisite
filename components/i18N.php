<?php
/**
 * Created by PhpStorm.
 * User: albertborsos
 * Date: 15. 08. 30.
 * Time: 18:11
 */

namespace albertborsos\yii2cmsmultisite\components;


class i18N extends \yii\i18n\I18N{

    public function translate($category, $message, $params, $language)
    {
        if($language=='en' && $category=='yii'){
            $language = 'en-US';
        }
        return parent::translate($category, $message, $params, $language);
    }
}