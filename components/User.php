<?php

namespace albertborsos\yii2cmsmultisite\components;


class User extends \yii\web\User{

    public $defaultRole = 'guest';

    public $destroySession = true;

} 