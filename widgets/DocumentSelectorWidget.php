<?php

namespace albertborsos\yii2cmsmultisite\widgets;

use albertborsos\yii2cmsmultisite\models\Document;
use kartik\select2\Select2;
use yii\web\JsExpression;

class DocumentSelectorWidget extends Select2
{
    public function init()
    {
        $this->options = array_merge([
            'placeholder' => 'Válassz..',
        ], $this->options);
        $this->data = Document::asDropDown('id', function (Document $item): string {
            return $item->filename;
        }, ['type' => Document::TYPE_DOCUMENT], ['id' => SORT_DESC]);
        parent::init();
    }
}