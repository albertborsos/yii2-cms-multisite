<?php

namespace albertborsos\yii2cmsmultisite\widgets;

use albertborsos\yii2cmsmultisite\models\Document;

class DocumentPreviewImageSelectorWidget extends ImageSelectorWidget
{
    protected $documentType = Document::TYPE_DOCUMENT_PREVIEW;
}
