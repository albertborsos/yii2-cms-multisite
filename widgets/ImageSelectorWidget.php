<?php

namespace albertborsos\yii2cmsmultisite\widgets;

use albertborsos\yii2cmsmultisite\models\Document;
use kartik\select2\Select2;
use yii\web\JsExpression;

class ImageSelectorWidget extends Select2
{
    protected $documentType = Document::TYPE_IMAGE;

    private const FORMAT_JS_EXPRESSION = 'function (item) {
        return $(\'<span><img src="/uploads/images/s/\' + item.text + \'" height="20px"/>\' + item.text + \'</span>\');
    }';

    public function init()
    {
        $this->options = array_merge([
            'placeholder' => 'Válassz..',
        ], $this->options);
        $this->data = Document::asDropDown('id', function (Document $item): string {
            return $item->filename;
        }, ['type' => $this->documentType], ['id' => SORT_DESC]);
        $this->pluginOptions = array_merge([
            'formatSelection' => new JsExpression(self::FORMAT_JS_EXPRESSION),
            'formatResult' => new JsExpression(self::FORMAT_JS_EXPRESSION),
        ], $this->pluginOptions);
        parent::init();

    }
}