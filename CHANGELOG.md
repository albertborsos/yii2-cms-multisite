# v0.15.3
 - turn off pagecache from config params
 - fix table th content overflow
# v0.15.2
 - fix hasform
# v0.15.1
 - do not cache page if it has form
# v0.15.0
 - Widget component added with `cache` option default is `false`
# v0.14.10
 - pagecache and httpcache added to index and blog actions
# v0.14.7
 - rmrevin/yii2-fontawesome update
# v0.14.6
 - fix cache menu by languages
# v0.14.5
 - redactor get image recursive false
# v0.14.4
 - redactor image upload in post content textarea
 - refactor gallery image upload
 - CmsCommonAsset - fix magnific popup on admin
# v0.14.3
 - fix if `right_to_view` is empty
# v0.14.2
 - Url::to() fb share image
# v0.14.1
 - default share image
 - able to set required right for a menuitem in navbar
 - fix activation link urlencode
 - disable url check redirects when query params exists by `redirect=0` GET param then `Seo::noIndex()`
# v0.14.0
 - set widgets for pages from admin