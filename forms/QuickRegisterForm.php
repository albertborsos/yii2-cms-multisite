<?php
/**
 * Created by PhpStorm.
 * User: borsosalbert
 * Date: 2014.04.28.
 * Time: 13:46
 */

namespace albertborsos\yii2cmsmultisite\forms;

use albertborsos\yii2cmsmultisite\models\Users;
use albertborsos\yii2shop\models\Addresses;
use Yii;
use yii\base\Exception;
use yii\base\Model;

class QuickRegisterForm extends Model {

    public $firstName;
    public $lastName;
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['firstName', 'required'],
            ['firstName', 'string'],

            ['lastName', 'required'],
            ['lastName', 'string'],


            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'validateIsPermanent']
        ];
    }

    public function validateIsPermanent($attribute, $params){
        $existingUser = Users::findByEmail($this->email);
        if(is_null($existingUser)){
            return true;
        }else{
            if($existingUser->is_permanent){
                return true;
            }else{
                $this->addError('email', 'Ezzel az emailcímmel már regisztráltak!');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lastName' => 'Vezetéknév',
            'firstName' => 'Keresztnév',
            'email' => 'E-mail cím',
        ];
    }

    public function register(){
        if ($this->validate()){
            $transaction = Yii::$app->db->beginTransaction();
            try {
                // megnézzük, hogy létezik-e ilyen user és
                // ha létezik, akkor is_permanent = 1-nek kell lennie
                $exists = Users::findByEmail($this->email);
                if(!is_null($exists) && $exists->is_permanent) {
                    // ha van ilyen user, akkor töröljük a felvitt szállítási címeit
                    $res = Addresses::updateAll(['status' => Addresses::STATUS_DELETED], ['created_user' => $exists->id]);
                    $transaction->commit();
                    return true;
                }else{
                    // ha ok, akkor mentjük
                    $user = new Users();
                    $user->email = $this->email;
                    $user->setPassword(uniqid('pwd_', true));
                    $user->status = $user::STATUS_ACTIVE;

                    if ($user->save()) {
                        $userdetails = $user->getDetails();
                        $userdetails->user_id = $user->id;
                        $userdetails->name_first = $this->firstName;
                        $userdetails->name_last = $this->lastName;
                        $userdetails->status = $user::STATUS_ACTIVE;

                        if ($userdetails->save()) {
                            $transaction->commit();
                            $user->sendInfoMail();
                            return true;
                        } else {
                            $this->addError('email', 'Nem sikerült menteni a felhasználóadatokat!');
                            return false;
                        }
                    } else {
                        $this->addError('email', 'Nem sikerült menteni a felhasználót');
                        return false;
                    }
                }
            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $e->getMessage());
                return false;
            }
        }else{
            return false;
        }
    }
} 