<?php

namespace albertborsos\yii2cmsmultisite\forms;

interface CmsPublicFormInterface
{
    public function process($params): void;
}