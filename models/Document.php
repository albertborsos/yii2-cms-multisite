<?php

namespace albertborsos\yii2cmsmultisite\models;

use albertborsos\yii2cmsmultisite\models\traits\CacheTrait;
use albertborsos\yii2historizer\Historizer;
use albertborsos\yii2lib\db\ActiveRecord;
use Spatie\Image\Image;
use Yii;
use yii\base\Exception;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tbl_cms2_document".
 *
 * @property integer $id
 * @property integer $type
 * @property string $name
 * @property string $filename
 * @property string $sha1_original
 * @property string $sha1_this
 * @property string $right_to_view
 * @property string $url
 * @property integer $preview_image_id
 * @property string $preview_max_width
 * @property string $preview_max_height
 * @property integer $created_at
 * @property integer $created_user
 * @property integer $updated_at
 * @property integer $updated_user
 * @property integer $status
 *
 * @property Document $previewImage
 * @property DocumentContent[] $documentContents
 * @property GalleryDocument[] $galleryDocuments
 */
class Document extends ActiveRecord
{
    use CacheTrait;

    const STATUS_ACTIVE = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_DELETED = 2;

    const TYPE_IMAGE = 0;
    const TYPE_DOCUMENT = 1;
    const TYPE_VIDEO_PREVIEW = 2;
    const TYPE_DOCUMENT_PREVIEW = 3;

    const TYPE_LABELS = [
        Document::TYPE_IMAGE => 'Kép feltöltés',
        Document::TYPE_DOCUMENT => 'PDF feltöltés',
        Document::TYPE_VIDEO_PREVIEW => 'Videó előnézeti kép feltöltés',
        Document::TYPE_DOCUMENT_PREVIEW => 'PDF előnézeti kép feltöltés',
    ];

    const UPLOADS_IMAGES = '/uploads/images';

    public $pathBase;
    public $pathFile;
    public $pathWatermark;
    public $urlBase;
    public $pathSavedFile;

    private $content;

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'clearCache']);

        $galleryPath = Yii::getAlias(Yii::$app->getModule('cms2')->galleryPath);
        $this->pathWatermark = $galleryPath . '/watermark-TL.png';
        $this->pathBase = $galleryPath;
        $this->urlBase = Yii::$app->getModule('cms2')->staticUrl;
    }

    public function getPathFull($thumbnail = false)
    {
        if (!$thumbnail) {
            return $this->pathBase . '/' . $this->filename;
        } else {
            return $this->pathBase . '/s/' . $this->filename;
        }
    }

    public function getUrlFull(bool $thumbnail = false):string
    {
        $subfolder = !$thumbnail ? '/' : '/s/';
        return $this->urlBase . self::UPLOADS_IMAGES . $subfolder . $this->filename;
    }

    public function getPreviewImageWithLink()
    {
        return Html::a(Html::img($this->getUrlFull(true), [
            'class' => 'img-responsive',
            'alt' => $this->getContent()->alt_description,
        ]), $this->getUrlFull(), [
            'data-rel' => 'lightbox[]',
            'title' => $this->getContent()->title,
        ]);
    }

    public function getUrlDelete()
    {
        return Yii::$app->urlManager->createUrl(['/cms2/document/delete', 'id' => $this->id]);
    }

    public function deleteFiles()
    {
        $files[] = $this->getPathFull(true);
        $files[] = $this->getPathFull();
        foreach ($files as $file) {
            if (file_exists($file)) {
                unlink($file);
            }
        }
        return true;
    }

    public function saveUploadedFile(UploadedFile $uploaded)
    {
        if ($this->isShaExists($uploaded) === false) {
            // not uploaded yet
            $pathFull = $this->getPathFull();
            if (!is_dir($this->pathBase)) {
                mkdir($this->pathBase, 0777, true);
                chmod($this->pathBase, 0777);
            }
            if ($uploaded->saveAs($pathFull)) {
                chmod($pathFull, 0777);
                if (!$this->save()) {
                    $this->throwNewException('Nem sikerült menteni az adatbázisba!');
                }
            }
            return true;
        } else {
            // already uploaded
            $this->throwNewException('Ez a fájl már fel van töltve!!');
            return false;
        }
    }

    public function isShaExists(UploadedFile $uploaded)
    {
        $this->sha1_original = sha1_file($uploaded->tempName);
        $exists = Document::find()
            ->where([
                'sha1_original' => $this->sha1_original,
                'type' => $this->type,
            ])
            ->orWhere(['sha1_this' => $this->sha1_original])->one();

        return $exists ? $exists->id : false;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cms2_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /* [['firstName', 'lastName'], 'default'], // resets to null */
        /* [['firstName', 'lastName'], 'trim'], // trim */
        return [
            [['type', 'preview_image_id', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['name', 'filename', 'sha1_original', 'sha1_this', 'right_to_view', 'url', 'preview_max_width', 'preview_max_height'], 'trim'],
            [['name', 'filename', 'sha1_original', 'sha1_this', 'right_to_view', 'url', 'preview_max_width', 'preview_max_height'], 'default'],
            [['name', 'filename', 'sha1_original', 'sha1_this', 'right_to_view', 'url', 'preview_max_width', 'preview_max_height'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'filename' => 'Filename',
            'sha1_original' => 'Sha1 Original',
            'sha1_this' => 'Sha1 This',
            'right_to_view' => 'Right To View',
            'url' => 'Url',
            'preview_image_id' => 'Preview Image ID',
            'preview_max_width' => 'Preview Max Width',
            'preview_max_height' => 'Preview Max Height',
            'created_at' => 'Created At',
            'created_user' => 'Created User',
            'updated_at' => 'Updated At',
            'updated_user' => 'Updated User',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreviewImage()
    {
        return $this->hasOne(Document::className(), ['id' => 'preview_image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentContents()
    {
        return $this->hasMany(DocumentContent::className(), ['document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleryDocuments()
    {
        return $this->hasMany(GalleryDocument::className(), ['document_id' => 'id']);
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            return true;
        } else {
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            Historizer::createArchive($this);
            $this->setOwnerAndTime();
            return true;
        } else {
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Historizer::createArchive($this);
            return true;
        } else {
            return false;
        }
    }

    public function setExtension(UploadedFile $uploaded)
    {
        switch ($uploaded->type) {
            case 'image/gif':
                $extension = '.gif';
                break;
            case 'image/jpeg':
            case 'image/x-citrix-jpeg':
            case 'image/pjpeg':
                $extension = '.jpg';
                break;
            case 'image/png':
            case 'image/x-png':
            case 'image/x-citrix-png':
                $extension = '.png';
                break;
            case 'application/pdf':
                $extension = '.pdf';
                break;
            default:
                throw new Exception('Nem megfelelő fájltípus!');
                break;
        }
        return $extension;
    }

    public function generateUniqueName($extension = null)
    {
        $mit = array("á", "é", "í", "ó", "ö", "ő", "ú", "ü", "ű", "Á", "É", "Í", "Ó", "Ö", "Ő", "Ú", "Ü", "Ű");
        $mire = array("a", "e", "i", "o", "o", "o", "u", "u", "u", "a", "e", "i", "o", "o", "o", "u", "u", "u");

        $this->filename = time() . '-' . $this->filename;

        $ext_in_filename = explode('.', $this->filename);
        $ext_in_filename = '.' . $ext_in_filename[count($ext_in_filename) - 1];
        $this->filename = str_replace($ext_in_filename, '', $this->filename);

        $this->filename = str_replace($mit, $mire, $this->filename);

        $this->filename = mb_strtolower($this->filename);

        $this->filename = preg_replace('@[\s!:;_\?=\\\+\*/%&#\.]+@', '-', $this->filename);
        $this->filename = preg_replace('/\-+/', '-', $this->filename);

        if (is_null($extension)) {
            $this->filename .= $ext_in_filename;
        } else {
            $this->filename .= $extension;
        }
        $this->filename = trim($this->filename, '-');
    }

    public function savePhoto($file, $width, $thumbnail = false)
    {
        $oldMemoryLimit = ini_set('memory_limit', '256M');
        try {
            $image = Image::load($file);

            if ($image->getWidth() > $width) {
                $image->width($width);
            }

            if ($thumbnail) {
                $pathToSave = $this->getPathFull(true);
            } else {
                $pathToSave = $this->getPathFull();
            }

            $image->save($pathToSave);

            ini_set('memory_limit', $oldMemoryLimit);

            if (!$thumbnail) {
                $this->sha1_this = sha1_file($pathToSave);
                return $this->update(false);
            }

            return true;
        } catch (\Exception $e) {
            ini_set('memory_limit', $oldMemoryLimit);
        }
    }

    private function setContent($language_code)
    {
        $this->content = new DocumentContent();

        if (is_null($language_code)) {
            $language_code = Yii::$app->language;
        }
        if ($language_code == Yii::$app->language) {
            $this->content->title = $this->name;
        }
        if (!is_null($language_code)) {
            $content = DocumentContent::getFreshest($this->id, $language_code);

            if (!is_null($content)) {
                $this->content = $content;
            }
        }
    }

    /**
     * @return \albertborsos\yii2cmsmultisite\models\DocumentContent
     */
    public function getContent($language_code = null)
    {
        if (is_null($this->content)) {
            $this->setContent($language_code);
        }
        return $this->content;
    }

    public function showAlerts($language_code, $show = true)
    {
        $latestContent = DocumentContent::getFreshest($this->id, $language_code);

        $alerts = [];

        if (is_null($latestContent)) {
            $alerts['danger'] = '<h4>Nem létezik tartalom ehhez a dokumentumhoz!</h4>';
        }

        if ($show) {
            foreach ($alerts as $key => $message) {
                Yii::$app->session->setFlash($key, $message);
            }
        } else {
            return $alerts;
        }
    }
}
