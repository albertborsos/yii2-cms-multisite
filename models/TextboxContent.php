<?php

namespace albertborsos\yii2cmsmultisite\models;

use albertborsos\yii2cmsmultisite\models\traits\CacheTrait;
use albertborsos\yii2lib\db\ActiveRecord;
use albertborsos\yii2historizer\Historizer;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tbl_cms2_textbox_content".
 *
 * @property integer $id
 * @property integer $textbox_id
 * @property string $site_language_code
 * @property string $widget
 * @property string $content
 * @property integer $published
 * @property integer $created_at
 * @property integer $created_user
 * @property integer $updated_at
 * @property integer $updated_user
 * @property integer $status
 *
 * @property Textbox $textbox
 */
class TextboxContent extends ActiveRecord
{
    use CacheTrait;

    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_DELETED  = 2;

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'clearCache']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cms2_textbox_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /* [['firstName', 'lastName'], 'default'], // resets to null */
        /* [['firstName', 'lastName'], 'trim'], // trim */
        return [
            [['textbox_id', 'published', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['site_language_code', 'content'], 'trim'],
            [['site_language_code', 'content'], 'default'],
            [['content', 'widget'], 'string'],
            [['site_language_code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'textbox_id' => 'Textbox ID',
            'site_language_code' => 'Site Language Code',
            'content' => 'Content',
            'widget' => 'Widget',
            'published' => 'Published',
            'created_at' => 'Created At',
            'created_user' => 'Created User',
            'updated_at' => 'Updated At',
            'updated_user' => 'Updated User',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTextbox()
    {
        return $this->hasOne(Textbox::className(), ['id' => 'textbox_id']);
    }
}
