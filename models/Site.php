<?php

namespace albertborsos\yii2cmsmultisite\models;

use albertborsos\yii2lib\db\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "tbl_cms2_site".
 *
 * @property integer $id
 * @property string $name
 * @property string $baseUrl
 * @property integer $type
 * @property integer $og_image_id
 * @property integer $created_at
 * @property integer $created_user
 * @property integer $updated_at
 * @property integer $updated_user
 * @property integer $status
 *
 * @property SiteLanguage[] $siteLanguages
 * @property SitePost[] $sitePosts
 * @property Document $ogImage
 */
class Site extends ActiveRecord
{
    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_DELETED  = 2;

    const TYPE_WEBSITE = 0;
    const TYPE_SUBDOMAIN = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cms2_site';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'baseUrl'], 'trim'],
            [['name', 'baseUrl'], 'default'],
            [['type', 'og_image_id', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['name', 'baseUrl'], 'string', 'max' => 255],
            [['og_image_id'], 'exist', 'targetClass' => Document::class, 'targetAttribute' => 'id', 'filter' => ['type' => Document::TYPE_IMAGE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'baseUrl' => 'Base Url',
            'type' => 'Type',
            'og_image_id' => 'Og:Image',
            'created_at' => 'Created At',
            'created_user' => 'Created User',
            'updated_at' => 'Updated At',
            'updated_user' => 'Updated User',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteLanguages()
    {
        return $this->hasMany(SiteLanguage::class, ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSitePosts()
    {
        return $this->hasMany(SitePost::class, ['site_id' => 'id']);
    }

    public function getOgImage(): ActiveQuery
    {
        return $this->hasOne(Document::class, ['id' => 'og_image_id']);
    }
}
