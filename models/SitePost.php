<?php

namespace albertborsos\yii2cmsmultisite\models;

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\forms\CmsPublicFormInterface;
use albertborsos\yii2cmsmultisite\models\traits\CacheTrait;
use albertborsos\yii2cmsmultisite\models\traits\HierarchyTrait;
use albertborsos\yii2cmsmultisite\Module;
use albertborsos\yii2historizer\Historizer;
use albertborsos\yii2lib\db\ActiveRecord;
use albertborsos\yii2lib\helpers\S;
use albertborsos\yii2lib\helpers\Seo;
use albertborsos\yii2tagger\models\Tags;
use himiklab\sortablegrid\SortableGridBehavior;
use Yii;
use yii\base\Widget;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Request;

/**
 * This is the model class for table "tbl_cms2_site_post".
 *
 * @property integer $id
 * @property integer $site_id
 * @property integer $parent_post_id
 * @property integer $link_post_id
 * @property integer $type
 * @property string $name
 * @property integer $sort_order
 * @property integer $commentable
 * @property integer $shareable
 * @property string $menu_item_class
 * @property string $right_to_view
 * @property integer $created_at
 * @property integer $created_user
 * @property integer $updated_at
 * @property integer $updated_user
 * @property integer $status
 *
 * @property SitePostContent $contents
 *
 * @property Site $site
 * @property SitePost $parentPost
 * @property SitePost $linkPost
 * @property SitePost[] $sitePosts
 * @property SitePostContent[] $sitePostContents
 * @property SitePostMeta[] $sitePostMetas
 */
class SitePost extends ActiveRecord
{
    use CacheTrait;
    use HierarchyTrait;

    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_DELETED = 2;

    const TYPE_MENU = 0;
    const TYPE_BLOG = 1;
    const TYPE_DROPDOWN = 2;
    const TYPE_MENU_LINK = 3;

    private $content;
    private $meta;

    private static function handleForm(string $formClass, Request $request): string
    {
        /* @var $model \yii\web\Model */
        $model = new $formClass();

        if ($model instanceof CmsPublicFormInterface && $model->load($request->post())) {
            if ($model->validate()) {
                $model->process(S::get(Yii::$app->params, 'cms.email.owner'));
            } else {
                Yii::$app->session->setFlash('danger', Html::errorSummary($model));
            }
            Yii::$app->controller->redirect($request->url);
            Yii::$app->end();
        }

        return Yii::$app->controller->renderPartial($model->view, [
            'model' => $model
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cms2_site_post';
    }

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_INSERT, [get_class($this), 'buildNodeTreeEvent']);
        $this->on(self::EVENT_AFTER_UPDATE, [get_class($this), 'buildNodeTreeEvent']);

        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'clearCache']);
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
           'sort' => [
               'class' => SortableGridBehavior::className(),
               'sortableAttribute' => 'sort_order'
           ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /* [['firstName', 'lastName'], 'default'], // resets to null */
        /* [['firstName', 'lastName'], 'trim'], // trim */
        return [
            [['site_id', 'type', 'status'], 'required'],
            ['link_post_id', 'required',
                'when' => function ($model) {
                    return $model->type == self::TYPE_MENU_LINK;
                },
                'whenClient' => "function (attribute, value) {
                    return $('.field-sitepost-link_post_id').length > 0;
                }
            "],
            [['site_id', 'parent_post_id', 'link_post_id', 'type', 'sort_order', 'commentable', 'shareable', 'menu_item_class', 'right_to_view', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'default'],
            [['site_id', 'parent_post_id', 'link_post_id', 'type', 'sort_order', 'commentable', 'shareable', 'menu_item_class', 'right_to_view', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'trim'],
            [['site_id', 'parent_post_id', 'link_post_id', 'type', 'sort_order', 'commentable', 'shareable', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['name'], 'trim'],
            [['name'], 'default'],
            [['name', 'menu_item_class', 'right_to_view'], 'string', 'max' => 255],
            [['sort_order'], 'unique', 'targetAttribute' => ['site_id', 'sort_order']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_id' => 'Site ID',
            'parent_post_id' => 'Parent Post ID',
            'link_post_id' => 'Link To',
            'type' => 'Type',
            'name' => 'Name',
            'sort_order' => 'Sort Order',
            'commentable' => 'Commentable',
            'shareable' => 'Shareable',
            'menu_item_class' => 'Menu Item Class',
            'right_to_view' => 'Right to view',
            'created_at' => 'Created At',
            'created_user' => 'Created User',
            'updated_at' => 'Updated At',
            'updated_user' => 'Updated User',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentPost()
    {
        return $this->hasOne(SitePost::className(), ['id' => 'parent_post_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinkPost()
    {
        return $this->hasOne(SitePost::className(), ['id' => 'link_post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSitePosts()
    {
        return $this->hasMany(SitePost::className(), ['parent_post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSitePostContents()
    {
        return $this->hasMany(SitePostContent::className(), ['site_post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSitePostMetas()
    {
        return $this->hasMany(SitePostMeta::className(), ['site_post_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            Historizer::createArchive($this);
            $this->setNextSortNumber();
            $this->setOwnerAndTime();
            return true;
        }else{
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()){
            Historizer::createArchive($this);
            return true;
        }else{
            return false;
        }
    }

    private function setNextSortNumber(){
        if(in_array($this->type, [self::TYPE_MENU, self::TYPE_MENU_LINK, self::TYPE_DROPDOWN])){
            if(is_null($this->sort_order) || $this->sort_order == ''){
                $lastNum = self::find()
                    ->select('sort_order')
                    ->where([
                        'site_id' => $this->site_id,
                    ])
                    ->orderBy('`sort_order` DESC')
                    ->limit(1)->scalar();
                if($lastNum === false || is_null($lastNum)){
                    $lastNum = 0;
                }else{
                    $lastNum = (int)$lastNum;
                }
                $this->sort_order = $lastNum + 1;
            }
        }else{
            $this->sort_order = null;
        }
    }

    public function getSelectParentMenu(){
        $items = SitePost::find()->where([
                'status' => SitePost::STATUS_ACTIVE,
                'type' => SitePost::TYPE_DROPDOWN
            ])
            ->andWhere(['site_id' => $this->site_id])
            ->andFilterWhere(['<>', 'id', $this->id])
            ->asArray()
            ->all();

        return ArrayHelper::map($items, 'id', 'name');
    }

    public function generateUrl($languageId = null, $createAbsoluteUrl = true){
        $link = '/';

        /** @var Module $module */
        $module = Yii::$app->getModule('cms2');
        $urlPostfix = $module->getSitePostfix($this->site_id);

        $seo = $this->getMeta($languageId);
        if (!$seo->isNewRecord && !is_null($seo->url)) {
            $link = $seo->url;
        } elseif(!$seo->isNewRecord && !is_null($seo->slug)) {
            switch ($this->type) {
                case self::TYPE_MENU:
                    if ($this->sort_order === 1) {
                        $link = '/';
                    } else {
                        $link = '/' . $seo->slug . ($urlPostfix ? $urlPostfix : '');
                    }
                    break;
                case self::TYPE_BLOG:
                    $link = '/blog/' . $seo->slug . ($urlPostfix ? $urlPostfix : '');
                    break;
            }
        } else {
            switch ($this->type) {
                case self::TYPE_MENU:
                    if ($this->sort_order === 1) {
                        $link = '/';
                    } else {
                        $link = '/' . DataProvider::replaceCharsToUrl($this->getContent($languageId)->name) . '-' . $this->id . ($urlPostfix ? $urlPostfix : '');
                    }
                    break;
                case self::TYPE_BLOG:
                    $link = '/blog/' . DataProvider::replaceCharsToUrl($this->getContent($languageId)->name) . '-' . $this->id . ($urlPostfix ? $urlPostfix : '');
                    break;
                case self::TYPE_MENU_LINK:
                    return $this->linkPost->generateUrl($languageId);
                    break;
            }
        }

        if ($createAbsoluteUrl) {
            if (strpos($link, '//')) {
                return $link;
            } else {
                $linkArray = $this->convertParamsToArray($link);
                return Yii::$app->urlManager->createAbsoluteUrl($linkArray);
            }
        } else {
            return $link;
        }
    }

    public function generateSlug($languageId = null){
        $slug = '';
        $seo = $this->getMeta($languageId);
        if (!$seo->isNewRecord && !is_null($seo->slug)) {
            $slug = $seo->slug;
        } else {
            switch ($this->type) {
                case self::TYPE_MENU:
                    if ($this->sort_order != 1) {
                        $slug = DataProvider::replaceCharsToUrl($this->getContent($languageId)->name);
                    }
                    break;
                case self::TYPE_BLOG:
                    $slug = DataProvider::replaceCharsToUrl($this->getContent($languageId)->name);
                    break;
                case self::TYPE_MENU_LINK:
                    return false;
                    break;
            }
        }
        return $slug;
    }

	protected function convertParamsToArray($link){
		$array = [];
		$exploded = explode('?', $link);
		$array[0] = $exploded[0];
		if(isset($exploded[1])){
			$params = explode('&',$exploded[1]);
			foreach($params as $param){
				$parts = explode('=', $param);
				$array[$parts[0]] = $parts[1];
			}
		}
		return $array;
	}

    private function setContent($language_id){
        $this->content = new SitePostContent();
        if(is_null($language_id)){
            $language_id = SiteLanguage::getIDByCode(Yii::$app->language);
        }
        if(!is_null($language_id)){
            $postID = ($this->type == SitePost::TYPE_MENU_LINK) ? $this->link_post_id : $this->id;

            $content = SitePostContent::getFreshest($postID, $language_id);

            if(!is_null($content)){
                $this->content = $content;
            }
        }
    }

    /**
     * @return \albertborsos\yii2cmsmultisite\models\SitePostContent
     */
    public function getContent($language_id = null){
        if(is_null($this->content)){
            $this->setContent($language_id);
        }

        return $this->content;
    }

    private function setMeta($language_id){
        $this->meta = new SitePostMeta();
        if(is_null($language_id)){
            $language_id = SiteLanguage::getIDByCode(Yii::$app->language);
        }
        if(!is_null($language_id)){
            $meta = SitePostMeta::getFreshest($this->id, $language_id);

            if(!is_null($meta)){
                $this->meta = $meta;
            }
        }
    }

    /**
     * @param null $language_id
     * @return SitePostMeta
     */
    public function getMeta($language_id = null)
    {
        if (is_null($this->meta)) {
            $this->setMeta($language_id);
        }
        return $this->meta;
    }

    public function setSEOValues()
    {
        /** @var \albertborsos\yii2cmsmultisite\models\SitePostMeta $meta */
        $meta = $this->getMeta();

        if (is_null($meta) || $meta->isNewRecord) {
            Seo::noIndex();
            return;
        }

        $title = !is_null($meta->title) ? $meta->title : $this->getContent()->getTitle(true);
        $description = !is_null($meta->meta_description) ? $meta->meta_description : null;
        $robots = !is_null($meta->meta_robots) ? $meta->meta_robots : SitePostMeta::META_ROBOTS_NOINDEX;

        /** @var Module $module */
        $module = Yii::$app->getModule('cms2');
        $title = $module->replaceDynamicStrings($title);
        $description = $module->replaceDynamicStrings($description);

        Seo::registerTag(Seo::TYPE_TITLE, $title);
        Seo::registerTag(Seo::TYPE_DESCRIPTION, $description);
        Seo::registerTag(Seo::TYPE_ROBOTS, SitePostMeta::items('meta-robots', $robots, false));
        Seo::registerTag(Seo::TYPE_CANONICAL, $meta->getCanonicalUrl());

        $fb = [
            Seo::TYPE_FB_TITLE => !is_null($meta->og_title) ? $meta->og_title : $title,
            Seo::TYPE_FB_DESCRIPTION => !is_null($meta->og_description) ? $meta->og_description : $description,
            Seo::TYPE_FB_LOCALE => Yii::$app->language,
            Seo::TYPE_FB_SHARE_URL => $this->generateUrl(),
            Seo::TYPE_FB_SITE_NAME => !is_null($meta->og_site_name) ? $meta->og_site_name : Yii::$app->name,
            Seo::TYPE_FB_TYPE => Seo::FB_TYPE_PAGE,
            Seo::TYPE_FB_IMAGE => $meta->getOgImageUrl(),
        ];

        foreach ($fb as $tagID => $tagValue) {
            $tagValue = $module->replaceDynamicStrings($tagValue);
            Seo::registerTag($tagID, $tagValue);
        }

        // ha kikapcsoltam az url ellenőrzést, akkor noindex
        if (Yii::$app->request->get('redirect', true) == 0) {
            Seo::noIndex();
        }
    }

    public static function insertForms($content)
    {
        $forms = Yii::$app->getModule('cms2')->forms;
        foreach ($forms as $formID => $formClass) {
            if (strpos($content, $formID) !== false) {
                $form = self::handleForm($formClass, Yii::$app->request);

                $content = str_replace($formID, $form, $content);
            }
        }
        return $content;
    }

    public static function renderForm($formId): ?string
    {
        $forms = Yii::$app->getModule('cms2')->forms;
        foreach ($forms as $id => $formClass) {
            if ($id !== $formId) {
                continue;
            }
            return self::handleForm($formClass, Yii::$app->request);
        }
        return null;
    }

    public function hasForm()
    {
        $forms = Yii::$app->getModule('cms2')->forms;
        foreach ($forms as $formID => $formClass) {
            if (strpos($this->getContent()->content_main, $formID) !== false) {
                return true;
            }
        }
        return false;
    }

    public function showAlerts($language_id, $show = true){
        $latestContent = SitePostContent::find()->where([
            'site_post_id' => $this->id,
            'site_language_id' => $language_id,
        ])->orderBy(['id' => SORT_DESC])->one();

        $latestMeta = SitePostMeta::find()->where([
            'site_post_id' => $this->id,
            'site_language_id' => $language_id,
        ])->orderBy(['id' => SORT_DESC])->one();

        $alerts = [];

        if(is_null($latestContent)){
            $alerts['danger-1'] = '<h4>Nem létezik tartalom ehhez a menüponthoz!</h4>';
        }else{
            if($latestContent->published == DataProvider::NO){
                $alerts['warning-1'] = '<h4>Nincs kipublikálva a legfrissebb tartalom!</h4>';
            }
        }

        if($this->type == SitePost::TYPE_MENU || $this->type == SitePost::TYPE_BLOG){
            if(is_null($latestMeta)){
                $alerts['danger-2'] = '<h4>Nem létezik SEO beállítás ehhez a menüponthoz!</h4>';
            }else{
                if($latestMeta->published == DataProvider::NO){
                    $alerts['warning-2'] = '<h4>Nincs kipublikálva a  legfrissebb SEO beállítás!</h4>';
                }
            }
        }

        if($show){
            foreach ($alerts as $key => $message) {
                Yii::$app->session->setFlash($key, $message);
            }
        }else{
            return $alerts;
        }
    }

    public function renderContent(Module $module){
        if (!empty($this->content->widget)) {
            /** @var Widget $className */
            $className = $this->content->widget;
            return $className::widget();
        }

        $sitePost = clone($this);
        $tags = Yii::$app->db->cache(function ($db) use ($sitePost) {
            return Tags::getAssignedTags($this, true, 'link');
        });

        switch($this->type){
            case self::TYPE_BLOG:
                return Yii::$app->controller->renderPartial($module->viewBlog, [
                    'post' => $this,
                    'tags' => $tags,
                ]);
                break;
            case self::TYPE_MENU:
                return Yii::$app->controller->renderPartial($module->viewMenu, [
                    'post' => $this,
                    'tags' => $tags,
                ]);
                break;
        }
        return false;
    }

    public function getLinkPosts($active = true, $forDropDownList = true){
        $models = SitePost::find()->where([
            'site_id' => $this->site_id,
        ]);

        switch($this->type){
            case SitePost::TYPE_MENU:
            case SitePost::TYPE_BLOG:
                $models->andWhere(['type' => $this->type]);
                break;
            case SitePost::TYPE_MENU_LINK:
                $models->andWhere(['type' => SitePost::TYPE_MENU]);
                break;
        }

        if ($active){
            $models->andWhere(['status' => SitePost::STATUS_ACTIVE]);
        }
        if ($forDropDownList){
            return ArrayHelper::map($models->asArray()->all(), 'id', 'name');
        }else{
            return $models->asArray()->all();
        }
    }

    public function setLayout(){
        $layout = $this->getContent()->layout;
        if(!is_null($layout)){
            Yii::$app->controller->layout = $layout;
        }
    }

    public static function getPageById($id, $siteId): ?self
    {
        return Yii::$app->db->cache(function ($db) use ($id, $siteId) {
            return SitePost::findOne([
                'id' => $id,
                'status' => [
                    SitePost::STATUS_ACTIVE,
                    SitePost::STATUS_INACTIVE,
                ],
                'site_id' => $siteId,
            ]);
        });
    }

    public static function getMainPage($siteId)
    {
        return Yii::$app->db->cache(function ($db) use ($siteId) {
            return SitePost::find()->where([
                'status' => SitePost::STATUS_ACTIVE,
                'site_id' => $siteId,
                'type' => SitePost::TYPE_MENU,
            ])->orderBy(['sort_order' => SORT_ASC])
                ->andWhere('sort_order IS NOT NULL')
                ->one();
        });
    }

    public function getNextBlogArticle(): ?self
    {
        if (!$this->isBlog()) {
            return null;
        }

        $nextId = $this->getNextArticleId();

        return $nextId ? self::findOne($nextId) : null;
    }

    public function getPreviousBlogArticle(): ?self
    {
        if (!$this->isBlog()) {
            return null;
        }

        $previousId = $this->getPreviousArticleId();

        return $previousId ? self::findOne($previousId) : null;
    }

    private function isBlog(): bool
    {
        return $this->type === self::TYPE_BLOG;
    }

    public static function findAllBlogPosts(int $sortOrder = SORT_ASC): array
    {
        $sortCondition = $sortOrder === SORT_ASC ? 'DESC' : 'ASC';

        return self::find()->from([
            'sp' => self::tableName()
        ])->where([
            'sp.type' => self::TYPE_BLOG,
            'sp.status' => self::STATUS_ACTIVE,
            'sl.code' => Yii::$app->language,
            'spc.published' => 1,
        ])->join('JOIN', SitePostContent::tableName() . ' spc', 'sp.id=spc.site_post_id')
            ->join('JOIN', SiteLanguage::tableName() . ' sl', 'sl.id=spc.site_language_id')
            ->orderBy(new Expression('-sp.sort_order ' . $sortCondition . ', id ' . $sortCondition))
            ->all();
    }

    private function getPreviousArticleId(): ?int
    {
        list($postOrders, $index) = $this->getCurrentArticleIndex();
        return $postOrders[$index - 1] ?? null;
    }

    private function getNextArticleId(): ?int
    {
        list($postOrders, $index) = $this->getCurrentArticleIndex();
        return $postOrders[$index + 1] ?? null;
    }

    private function getCurrentArticleIndex(): array
    {
        $postOrders = ArrayHelper::getColumn(self::findAllBlogPosts(SORT_DESC), 'id');
        $index = array_search($this->id, $postOrders);
        return [$postOrders, $index];
    }
}
