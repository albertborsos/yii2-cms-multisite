<?php

namespace albertborsos\yii2cmsmultisite\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * GalleryDocumentSearch represents the model behind the search form about `albertborsos\yii2cmsmultisite\models\GalleryDocument`.
 */
class GalleryDocumentSearch extends GalleryDocument
{
    public function rules(): array
    {
        return [
            [['id', 'gallery_id', 'document_id', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['url'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GalleryDocument::find()->orderBy([
            'sort_order' => SORT_ASC,
            'id' => SORT_DESC,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'gallery_id' => $this->gallery_id,
            'document_id' => $this->document_id,
            'created_at' => $this->created_at,
            'created_user' => $this->created_user,
            'updated_at' => $this->updated_at,
            'updated_user' => $this->updated_user,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}
