<?php

namespace albertborsos\yii2cmsmultisite\models;

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\traits\CacheTrait;
use albertborsos\yii2historizer\Historizer;
use albertborsos\yii2lib\db\ActiveRecord;
use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "tbl_cms2_textbox".
 *
 * @property integer $id
 * @property string $name
 * @property string $replace_key
 * @property integer $created_at
 * @property integer $created_user
 * @property integer $updated_at
 * @property integer $updated_user
 * @property integer $status
 *
 * @property TextboxContent[] $textboxContents
 */
class Textbox extends ActiveRecord
{

    use CacheTrait;

    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_DELETED  = 2;

    const PREFIX = '[#';
    const POSTFIX = '#]';

    private $content;


    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'clearCache']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cms2_textbox';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /* [['firstName', 'lastName'], 'default'], // resets to null */
        /* [['firstName', 'lastName'], 'trim'], // trim */
        return [
            [['name', 'replace_key'], 'trim'],
            [['name', 'replace_key'], 'default'],
            [['created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['name', 'replace_key'], 'string', 'max' => 255],
            [['replace_key'], 'unique'],
            [['replace_key'], 'validateUniqueReplaceKey'],
        ];
    }

    public function validateUniqueReplaceKey($attribute, $params){
        // check this key does not exists in textbox
        $gallery = Gallery::find()->where([
            'replace_id' => $this->replace_key,
        ])->one();

        if(!is_null($gallery)){
            $this->addError('replace_key', 'Ez az azonosító már foglalt!');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'replace_key' => 'Replace Key',
            'created_at' => 'Created At',
            'created_user' => 'Created User',
            'updated_at' => 'Updated At',
            'updated_user' => 'Updated User',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTextboxContents()
    {
        return $this->hasMany(TextboxContent::className(), ['textbox_id' => 'id']);
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()){
            $this->setReplaceKey();
            return true;
        }else{
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            Historizer::createArchive($this);
            $this->setOwnerAndTime();
            return true;
        }else{
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()){
            Historizer::createArchive($this);
            return true;
        }else{
            return false;
        }
    }

    public function setReplaceKey(){
        if (strpos($this->replace_key, '[#') === false && strpos($this->replace_key, '#]') === false) {
            $convertedKey = Inflector::slug($this->replace_key);
            $this->replace_key = self::PREFIX . $convertedKey . self::POSTFIX;
        }
    }

    public function showAlerts($language_code, $show = true){
        $latestContent = TextboxContent::find()->where([
            'textbox_id' => $this->id,
            'site_language_code' => $language_code,
        ])->orderBy(['id' => SORT_DESC])->one();

        $alerts = [];

        if(is_null($latestContent)){
            $alerts['danger'] = '<h4>Nem létezik tartalom ehhez a szövegdobozhoz!</h4>';
        }else{
            if($latestContent->published == DataProvider::NO){
                $alerts['warning'] = '<h4>Nincs kipublikálva a legfrissebb tartalom!</h4>';
            }
        }

        if($show){
            foreach ($alerts as $key => $message) {
                Yii::$app->session->setFlash($key, $message);
            }
        }else{
            return $alerts;
        }
    }

    private function setContent($language_code){
        $this->content = new TextboxContent();
        if(is_null($language_code)){
            $language_code = Yii::$app->language;
        }
        if(!is_null($language_code)){
            $textboxId = $this->id;
            $content = Yii::$app->db->cache(function ($db) use ($language_code, $textboxId){
                return TextboxContent::find()->where([
                    'textbox_id' => $textboxId,
                    'site_language_code' => $language_code,
                    'published' => DataProvider::YES,
                ])->orderBy(['id' => SORT_DESC])->one();
            });

            if(!is_null($content)){
                $this->content = $content;
            }
        }
    }

    /**
     * @return \albertborsos\yii2cmsmultisite\models\TextboxContent
     */
    public function getContent($language_code = null){
        if(is_null($this->content)){
            $this->setContent($language_code);
        }
        return $this->content;
    }

    public static function insertTextBoxes($content){
        $pattern = '/\[#.[^(\[#)]*#\]/';
        if(preg_match_all($pattern, $content, $results)){
            foreach($results as $codes){
                foreach ($codes as $code) {
                    $to = self::show($code);
                    $content = str_replace($code, $to ?: '', $content);
                }
            }
        }
        return $content;
    }

    public static function show($replace_key){
        $textBox = Yii::$app->db->cache(function ($db) use ($replace_key) {
            return Textbox::find()->where([
                'replace_key' => $replace_key,
                'status' => SitePost::STATUS_ACTIVE,
            ])->one();
        });

        if (empty($textBox)) {
            return null;
        }

        /** @var TextboxContent $textboxContent */
        $textboxContent = $textBox->getContent();
        $content = $textboxContent->content;
        if ($textboxContent->widget) {
            $widgetClass = $textboxContent->widget;
            $content = $widgetClass::widget();
        }
        $pattern = '/\[#.[^(\[#)]*#\]/';
        if(preg_match_all($pattern, $content, $results)){
            foreach($results as $codes){
                foreach ($codes as $code) {
                    $content = str_replace($code, self::show($code), $content);
                }
            }
        }
        return $content;
    }
}
