<?php

namespace albertborsos\yii2cmsmultisite\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use albertborsos\yii2cmsmultisite\models\SitePostMeta;

/**
 * SitePostMetaSearch represents the model behind the search form about `albertborsos\yii2cmsmultisite\models\SitePostMeta`.
 */
class SitePostMetaSearch extends SitePostMeta
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'site_post_id', 'site_language_id', 'meta_robots', 'canonical_post_id', 'og_image_id', 'published', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['title', 'meta_description', 'url', 'og_title', 'og_description', 'og_site_name', 'fb_app_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SitePostMeta::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            'pagination' => false,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'site_post_id' => $this->site_post_id,
            'site_language_id' => $this->site_language_id,
            'meta_robots' => $this->meta_robots,
            'canonical_post_id' => $this->canonical_post_id,
            'og_image_id' => $this->og_image_id,
            'published' => $this->published,
            'created_at' => $this->created_at,
            'created_user' => $this->created_user,
            'updated_at' => $this->updated_at,
            'updated_user' => $this->updated_user,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'og_title', $this->og_title])
            ->andFilterWhere(['like', 'og_description', $this->og_description])
            ->andFilterWhere(['like', 'og_site_name', $this->og_site_name])
            ->andFilterWhere(['like', 'fb_app_id', $this->fb_app_id]);

        return $dataProvider;
    }
}
