<?php

namespace albertborsos\yii2cmsmultisite\models;

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\traits\CacheTrait;
use albertborsos\yii2historizer\Historizer;
use albertborsos\yii2lib\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "tbl_cms2_site_post_content".
 *
 * @property integer $id
 * @property integer $site_post_id
 * @property integer $site_language_id
 * @property string $layout
 * @property string $widget
 * @property string $name
 * @property string $content_short
 * @property string $content_main
 * @property string $date_show
 * @property integer $published
 * @property integer $created_at
 * @property integer $created_user
 * @property integer $updated_at
 * @property integer $updated_user
 * @property integer $status
 *
 * @property SitePost $sitePost
 * @property SiteLanguage $siteLanguage
 */
class SitePostContent extends ActiveRecord
{
    use CacheTrait;

    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_DELETED  = 2;


    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'clearCache']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cms2_site_post_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /* [['firstName', 'lastName'], 'default'], // resets to null */
        /* [['firstName', 'lastName'], 'trim'], // trim */
        return [
            [['name'], 'required'],
            [['site_post_id', 'site_language_id', 'published', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['name', 'layout', 'widget', 'content_short', 'content_main'], 'trim'],
            [['name', 'layout', 'widget', 'content_short', 'content_main'], 'default'],
            [['content_short', 'content_main'], 'string'],
            [['date_show'], 'safe'],
            [['name', 'layout', 'widget'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_post_id' => 'Site Post ID',
            'site_language_id' => 'Site Language ID',
            'layout' => 'Layout',
            'widget' => 'Widget',
            'name' => 'Name',
            'content_short' => 'Content Short',
            'content_main' => 'Content Main',
            'date_show' => 'Date Show',
            'published' => 'Published',
            'created_at' => 'Created At',
            'created_user' => 'Created User',
            'updated_at' => 'Updated At',
            'updated_user' => 'Updated User',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSitePost()
    {
        return $this->hasOne(SitePost::className(), ['id' => 'site_post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteLanguage()
    {
        return $this->hasOne(SiteLanguage::className(), ['id' => 'site_language_id']);
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()){
            return true;
        }else{
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            Historizer::createArchive($this);
            $this->setOwnerAndTime();
            return true;
        }else{
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()){
            Historizer::createArchive($this);
            return true;
        }else{
            return false;
        }
    }

    public static function getFreshest($postID, $languageId)
    {
        return Yii::$app->db->cache(function ($db) use ($postID, $languageId) {
            return SitePostContent::find()->where([
                'site_post_id' => $postID,
                'site_language_id' => $languageId,
                'published' => DataProvider::YES,
            ])->orderBy(['id' => SORT_DESC])->one();
        });
    }

    public function getTitle(bool $withPostfix = false): string
    {
        $defaultTitle = strip_tags($this->name);
        if ($withPostfix) {
            return implode(' | ', [$defaultTitle, $this->sitePost->site->name]);
        }
        return $defaultTitle;
    }
}
