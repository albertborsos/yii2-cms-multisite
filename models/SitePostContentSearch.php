<?php

namespace albertborsos\yii2cmsmultisite\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use albertborsos\yii2cmsmultisite\models\SitePostContent;

/**
 * SitePostContentSearch represents the model behind the search form about `albertborsos\yii2cmsmultisite\models\SitePostContent`.
 */
class SitePostContentSearch extends SitePostContent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'site_post_id', 'site_language_id', 'published', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['name', 'content_short', 'content_main', 'date_show'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SitePostContent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            'pagination' => false,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'site_post_id' => $this->site_post_id,
            'site_language_id' => $this->site_language_id,
            'date_show' => $this->date_show,
            'published' => $this->published,
            'created_at' => $this->created_at,
            'created_user' => $this->created_user,
            'updated_at' => $this->updated_at,
            'updated_user' => $this->updated_user,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'content_short', $this->content_short])
            ->andFilterWhere(['like', 'content_main', $this->content_main]);

        return $dataProvider;
    }
}
