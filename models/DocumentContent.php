<?php

namespace albertborsos\yii2cmsmultisite\models;

use albertborsos\yii2cmsmultisite\models\traits\CacheTrait;
use albertborsos\yii2lib\db\ActiveRecord;
use albertborsos\yii2historizer\Historizer;
use Yii;

/**
 * This is the model class for table "tbl_cms2_document_content".
 *
 * @property integer $id
 * @property integer $document_id
 * @property string $language_code
 * @property string $title
 * @property string $alt_description
 * @property integer $created_at
 * @property integer $created_user
 * @property integer $updated_at
 * @property integer $updated_user
 * @property integer $status
 *
 * @property Document $document
 */
class DocumentContent extends ActiveRecord
{
    use CacheTrait;

    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_DELETED  = 2;

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'clearCache']);
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cms2_document_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /* [['firstName', 'lastName'], 'default'], // resets to null */
        /* [['firstName', 'lastName'], 'trim'], // trim */
        return [
            [['document_id', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['language_code', 'title', 'alt_description'], 'trim'],
            [['language_code', 'title', 'alt_description'], 'default'],
            [['language_code', 'title', 'alt_description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'document_id' => 'Document ID',
            'language_code' => 'Language Code',
            'title' => 'Title',
            'alt_description' => 'Alt Description',
            'created_at' => 'Created At',
            'created_user' => 'Created User',
            'updated_at' => 'Updated At',
            'updated_user' => 'Updated User',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()){
            return true;
        }else{
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            Historizer::createArchive($this);
            $this->setOwnerAndTime();
            return true;
        }else{
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()){
            Historizer::createArchive($this);
            return true;
        }else{
            return false;
        }
    }

    public static function getFreshest($documentID, $languageCode)
    {
        return Yii::$app->db->cache(function ($db) use ($documentID, $languageCode){
            return DocumentContent::find()->where([
                'document_id' => $documentID,
                'language_code' => $languageCode,
            ])->orderBy(['id' => SORT_DESC])->one();
        });
    }
}
