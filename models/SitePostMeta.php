<?php

namespace albertborsos\yii2cmsmultisite\models;

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\traits\CacheTrait;
use albertborsos\yii2historizer\Historizer;
use albertborsos\yii2lib\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_cms2_site_post_meta".
 *
 * @property integer $id
 * @property integer $site_post_id
 * @property integer $site_language_id
 * @property string $title
 * @property string $meta_description
 * @property integer $meta_robots
 * @property string $url
 * @property string $slug
 * @property integer $canonical_post_id
 * @property string $canonical_url
 * @property string $og_title
 * @property string $og_description
 * @property string $og_site_name
 * @property integer $og_image_id
 * @property string $fb_app_id
 * @property integer $published
 * @property integer $created_at
 * @property integer $created_user
 * @property integer $updated_at
 * @property integer $updated_user
 * @property integer $status
 *
 * @property SitePost $sitePost
 * @property SiteLanguage $siteLanguage
 * @property SitePost $canonicalPost
 */
class SitePostMeta extends ActiveRecord
{
    use CacheTrait;

    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_DELETED  = 2;

    const META_ROBOTS_NOINDEX = 0;
    const META_ROBOTS_INDEX = 1;
    const META_ROBOTS_NOINDEX_NOFOLLOW = 2;

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'clearCache']);
    }

    public static function items($category, $id = null, $returnArray = true)
    {
        $array = array();
        switch ($category) {
            case 'meta-robots':
                $array = [
                    self::META_ROBOTS_INDEX => 'INDEX',
                    self::META_ROBOTS_NOINDEX => 'NOINDEX',
                    self::META_ROBOTS_NOINDEX_NOFOLLOW => 'NOINDEX, NOFOLLOW',
                ];
                break;
        }
        if (is_null($id) && $returnArray) {
            return $array;
        } else {
            return isset($array[$id]) ? $array[$id] : $id;
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cms2_site_post_meta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_post_id', 'site_language_id', 'meta_robots', 'canonical_post_id', 'og_image_id','published', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['title', 'meta_description', 'url', 'slug', 'og_title', 'og_description', 'og_site_name', 'fb_app_id'], 'trim'],
            [['title', 'meta_description', 'url', 'slug', 'og_title', 'og_description', 'og_site_name', 'fb_app_id'], 'default'],
            [['title', 'og_title'], 'string', 'max' => 70],
            [['meta_description', 'og_description'], 'string', 'max' => 160],
            [['url', 'slug', 'fb_app_id', 'canonical_url'], 'string', 'max' => 255],
            [['og_site_name'], 'string', 'max' => 50],
            [['canonical_url'], 'url'],
            [['og_image_id'], 'exist', 'targetClass' => Document::class, 'targetAttribute' => 'id', 'filter' => ['type' => Document::TYPE_IMAGE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_post_id' => 'Site Post ID',
            'site_language_id' => 'Site Language ID',
            'title' => 'Title',
            'meta_description' => 'Meta Description',
            'meta_robots' => 'Meta Robots',
            'url' => 'Url',
            'slug' => 'Slug',
            'canonical_post_id' => 'Canonical Post',
            'canonical_url' => 'Canonical Url',
            'og_title' => 'Og Title',
            'og_description' => 'Og Description',
            'og_site_name' => 'Og Site Name',
            'og_image_id' => 'Og Image ID',
            'fb_app_id' => 'Fb App ID',
            'published' => 'Published',
            'created_at' => 'Created At',
            'created_user' => 'Created User',
            'updated_at' => 'Updated At',
            'updated_user' => 'Updated User',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSitePost()
    {
        return $this->hasOne(SitePost::className(), ['id' => 'site_post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCanonicalPost()
    {
        return $this->hasOne(SitePost::className(), ['id' => 'canonical_post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteLanguage()
    {
        return $this->hasOne(SiteLanguage::className(), ['id' => 'site_language_id']);
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()){
            $this->slug = DataProvider::replaceCharsToUrl($this->slug);
            return true;
        }else{
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            Historizer::createArchive($this);
            $this->setOwnerAndTime();
            return true;
        }else{
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()){
            Historizer::createArchive($this);
            return true;
        }else{
            return false;
        }
    }

    public function getCanonicalPosts($active = true, $forDropDownList = true){
        $models = SitePost::find()->where([
            'site_id' => $this->sitePost->site_id,
            'type' => $this->sitePost->type,
        ])->andWhere('id <> :this_post_id', [':this_post_id' => $this->site_post_id]);
        if ($active){
            $models->andWhere(['status' => SitePost::STATUS_ACTIVE]);
        }
        if ($forDropDownList){
            return ArrayHelper::map($models->asArray()->all(), 'id', 'name');
        }else{
            return $models->asArray()->all();
        }
    }

    public static function findBySlug(string $slug, int $siteId): ?static
    {
        return SitePostMeta::find()
            ->joinWith(['sitePost'])
            ->where([
                'slug' => $slug,
                'published' => DataProvider::YES,
                SitePost::tableName() . '.status' => [SitePost::STATUS_ACTIVE, SitePost::STATUS_INACTIVE],
                SitePost::tableName() . '.site_id' => $siteId,
            ])
            ->orderBy([
                SitePostMeta::tableName() . '.id' => SORT_DESC
            ])->cache()->one();
    }

    public static function getSitePostIdBySlug($slug, $siteId): ?int
    {
        return static::findBySlug($slug, $siteId)->site_post_id ?? null;
    }

    public static function getFreshest($postID, $languageId)
    {
        return Yii::$app->db->cache(function ($db) use ($postID, $languageId) {
            return SitePostMeta::find()->where([
                'site_post_id' => $postID,
                'site_language_id' => $languageId,
                'published' => DataProvider::YES,
            ])->orderBy(['id' => SORT_DESC])->one();
        });
    }

    public function getOgImageUrl(bool $isThumbnail = false): ?string
    {
        if (empty($this->og_image_id) && $ogImage = $this->sitePost->site->ogImage) {
            return $ogImage->getUrlFull($isThumbnail);
        }
        
        /** @var Document $image */
        $image = Document::findOne(['id' => $this->og_image_id]);
        if(!is_null($image)){
            return $image->getUrlFull($isThumbnail);
        }

        return null;
    }

    public function getCanonicalUrl(): string
    {
        if ($this->canonical_post_id !== null) {
            return $this->canonicalPost->generateUrl($this->sitePost->getContent()->site_language_id);
        }

        if (!empty($this->canonical_url)) {
            return $this->canonical_url;
        }

        return $this->sitePost->generateUrl();
    }
}
