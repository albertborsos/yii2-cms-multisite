<?php

namespace albertborsos\yii2cmsmultisite\models;

use albertborsos\yii2lib\db\ActiveRecord;
use albertborsos\yii2historizer\Historizer;
use albertborsos\yii2lib\helpers\S;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_cms2_site_language".
 *
 * @property integer $id
 * @property integer $site_id
 * @property string $code
 * @property string $name
 * @property integer $created_at
 * @property integer $created_user
 * @property integer $updated_at
 * @property integer $updated_user
 * @property integer $status
 *
 * @property Site $site
 * @property SitePostContent[] $sitePostContents
 * @property SitePostMeta[] $itePostMetas
 */
class SiteLanguage extends ActiveRecord
{
    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_DELETED  = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cms2_site_language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /* [['firstName', 'lastName'], 'default'], // resets to null */
        /* [['firstName', 'lastName'], 'trim'], // trim */
        return [
            [['site_id', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['code', 'name'], 'trim'],
            [['code', 'name'], 'default'],
            [['code', 'name'], 'string', 'max' => 255],
            [['code'], 'unique', 'targetAttribute' => ['site_id', 'code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_id' => 'Site ID',
            'code' => 'Code',
            'name' => 'Name',
            'created_at' => 'Created At',
            'created_user' => 'Created User',
            'updated_at' => 'Updated At',
            'updated_user' => 'Updated User',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSitePostContents()
    {
        return $this->hasMany(SitePostContent::className(), ['site_language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSitePostMetas()
    {
        return $this->hasMany(SitePostMeta::className(), ['site_language_id' => 'id']);
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()){
            return true;
        }else{
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            Historizer::createArchive($this);
            $this->setOwnerAndTime();
            return true;
        }else{
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()){
            Historizer::createArchive($this);
            return true;
        }else{
            return false;
        }
    }

    public static function getIDByCode($code){
        return SiteLanguage::find()->select('id')->where([
            'code' => $code,
            'site_id' => Yii::$app->getModule('cms2')->site,
            'status' => SiteLanguage::STATUS_ACTIVE,
        ])->createCommand()->cache()->queryScalar();
    }

    public static function getIDsByCode($code){
        $languages = SiteLanguage::find()->where([
            'code' => $code,
            'status' => SiteLanguage::STATUS_ACTIVE,
        ])->createCommand()->cache()->queryScalar();

        return array_keys(ArrayHelper::map($languages, 'id', 'id'));
    }
}
