<?php

namespace albertborsos\yii2cmsmultisite\models;

use albertborsos\yii2lib\db\ActiveRecord;
use himiklab\sortablegrid\SortableGridBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "tbl_cms2_gallery_document".
 *
 * @property integer $id
 * @property integer $gallery_id
 * @property integer $document_id
 * @property string $url
 * @property integer $sort_order
 * @property integer $created_at
 * @property integer $created_user
 * @property integer $updated_at
 * @property integer $updated_user
 * @property integer $status
 *
 * @property Document $document
 * @property Gallery $gallery
 */
class GalleryDocument extends ActiveRecord
{
    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_DELETED  = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cms2_gallery_document';
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'blameable' => [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
            'sort' => [
                'class' => SortableGridBehavior::class,
                'sortableAttribute' => 'sort_order',
                'scope' => function (ActiveQuery $query): void {
                    $query->andWhere(['gallery_id' => $this->gallery_id]);
                },
            ],
        ];
    }

    public function rules(): array
    {
        return [
            [['gallery_id', 'document_id', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['url'], 'trim'],
            [['url'], 'default'],
            [['url'], 'string', 'max' => 255],
            [['document_id'], 'unique', 'targetAttribute' => ['gallery_id', 'document_id']],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'gallery_id' => 'Gallery ID',
            'document_id' => 'Document ID',
            'url' => 'URL',
            'created_at' => 'Created At',
            'created_user' => 'Created User',
            'updated_at' => 'Updated At',
            'updated_user' => 'Updated User',
            'status' => 'Status',
        ];
    }

    public function getDocument(): ActiveQuery
    {
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }

    public function getGallery(): ActiveQuery
    {
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id']);
    }

    public function getUrlDelete(): string
    {
        return Yii::$app->urlManager->createUrl(['/cms2/gallery-document/delete', 'id' => $this->id]);
    }
}
