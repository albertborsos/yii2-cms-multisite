<?php

namespace albertborsos\yii2cmsmultisite\models\traits;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

trait HierarchyTrait
{
    private $_parentIDs;
    private $_childIDs;

    public static function buildNodeTreeEvent()
    {
        return self::buildNodeTree();
    }

    public static function buildNodeTree($parent = null, $left = null)
    {
        if (is_null($parent) && is_null($left)) {
            $mainItems = self::find()->where('parent_post_id IS NULL')->orderBy('sort_order ASC')->all();
            $left = 1;
            foreach ($mainItems as $mainItem) {
                $left = self::buildNodeTree($mainItem->id, $left);
            }
        } else {
            // the right value of this node is the left value + 1
            $right = $left + 1;
            // get all children of this node
            $children = static::find()->select(['id'])->where([
                'parent_post_id' => $parent
            ])->orderBy(['sort_order' => SORT_ASC, 'name' => SORT_ASC])->asArray()->all();

            foreach ($children as $child) {
                // recursive execution of this function for each
                // child of this node
                // $right is the current right value, which is
                // incremented by the rebuild_tree function
                $right = self::buildNodeTree($child['id'], $right);
            }

            // we've got the left value, and now that we've processed
            // the children of this node we also know the right value
            $sql = 'UPDATE ' . static::tableName() . ' SET lft=:left, rgt=:right WHERE id=:parent_post_id';

            Yii::$app->db->createCommand($sql, [
                ':left' => $left,
                ':right' => $right,
                ':parent_post_id' => $parent,
            ])->execute();

            // return the right value of this node + 1
            return $right + 1;
        }

        return true;
    }

    public function getTreeNodeAscii()
    {
        return Yii::$app->db->cache(function ($db) {
            return (new Query())
                ->select([
                    new Expression("COUNT(parent.name)-1 as ascii")
                ])
                ->from([
                    'node' => static::tableName(),
                    'parent' => static::tableName(),
                ])
                ->where(['BETWEEN', 'node.lft', new Expression('parent.lft'), new Expression('parent.rgt')])
                ->andWhere(['node.id' => $this->id])
                ->groupBy(['node.id'])
                ->orderBy(['node.lft' => SORT_ASC])
                ->scalar();
        });
    }

    public static function buildDropDownList($app_id)
    {
        return (new Query())
            ->select([
                'node.id',
                new Expression("CONCAT('<span class=\"tree-level level-', COUNT(parent.id)-1, '\"></span>', node.name) as name"),
            ])
            ->from([
                'node' => static::tableName(),
                'parent' => static::tableName(),
            ])
            ->where(['BETWEEN', 'node.lft', new Expression('parent.lft'), new Expression('parent.rgt')])
            ->andWhere(['node.app_id' => $app_id])
            ->groupBy(['node.id'])
            ->orderBy(['node.lft' => SORT_ASC])
            ->all();
    }

    /*
	 * Visszaadja a kategória feletti kategóriákat a gyermekkel együttt
	 */
    public function getParentIDs()
    {
        if (is_null($this->_parentIDs)) {
            $hash = self::hashArray([static::className() . '->getParentIDs()', $this->id]);
            if (!Yii::$app->cache->get($hash)) {
                $this->_parentIDs = (new Query())->select(['parent.id'])
                    ->from([
                        'node' => static::tableName(),
                        'parent' => static::tableName(),
                    ])
                    ->where(['BETWEEN', 'node.lft', new Expression('parent.lft'), new Expression('parent.rgt')])
                    ->andWhere(['node.id' => $this->id])
                    ->orderBy('parent.lft')
                    ->column();

                Yii::$app->cache->set($hash, $this->_parentIDs, 60 * 10);
            }
            $this->_parentIDs = Yii::$app->cache->get($hash);
        }

        return $this->_parentIDs;
    }

    /*
	 * Visszaadja a kategória alatti categóriákat a szülővel együttt
	 */
    public function getChildIDs()
    {
        if (is_null($this->_childIDs)) {
            $hash = self::hashArray([self::className() . '->getChildIDs()', $this->id]);
            if (!Yii::$app->cache->get($hash)) {

                $this->_childIDs = (new Query())->select(['node.id'])
                    ->from([
                        'node' => static::tableName(),
                        'parent' => static::tableName(),
                    ])
                    ->where(['BETWEEN', 'node.lft', new Expression('parent.lft'), new Expression('parent.rgt')])
                    ->andWhere(['parent.id' => $this->id])
                    ->orderBy('node.lft')
                    ->column();

                Yii::$app->cache->set($hash, $this->_childIDs, 60 * 10);
            }
            $this->_childIDs = Yii::$app->cache->get($hash);
        }

        return $this->_childIDs;
    }


    private static function hashArray($array)
    {
        return sha1(Json::encode($array));
    }

    public static function getList()
    {
        $list = self::buildDropDownList();

        return ArrayHelper::map($list, 'id', 'name');
    }
}
