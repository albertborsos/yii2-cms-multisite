<?php
/**
 * Created by PhpStorm.
 * User: albertborsos
 * Date: 16. 04. 21.
 * Time: 22:23
 */

namespace albertborsos\yii2cmsmultisite\models\traits;

use Yii;


trait CacheTrait {

    public function clearCache()
    {
        Yii::$app->cache->flush();
    }
}
