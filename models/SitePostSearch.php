<?php

namespace albertborsos\yii2cmsmultisite\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use albertborsos\yii2cmsmultisite\models\SitePost;
use yii\db\Expression;

/**
 * SitePostSearch represents the model behind the search form about `albertborsos\yii2cmsmultisite\models\SitePost`.
 */
class SitePostSearch extends SitePost
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'site_id', 'parent_post_id', 'type', 'sort_order', 'commentable', 'shareable', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $types = [])
    {
        $query = SitePost::find()
            ->select([
                'p.*',
                new Expression('CASE
                                    WHEN p.parent_post_id IS NULL
                                    THEN p.sort_order
                                    ELSE p.sort_order/100 + p2.sort_order
                                    END as indx'),
            ])
            ->join('LEFT JOIN', self::tableName() . ' p2', 'p.parent_post_id=p2.id')
            ->from(self::tableName() . ' p');

        $query->andFilterWhere(['p.type' => $types]);
        $query->orderBy('-`indx` DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => false,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'p.id' => $this->id,
            'p.site_id' => $this->site_id,
            'p.parent_post_id' => $this->parent_post_id,
            'p.type' => $this->type,
            'p.sort_order' => $this->sort_order,
            'p.commentable' => $this->commentable,
            'p.shareable' => $this->shareable,
            'p.created_at' => $this->created_at,
            'p.created_user' => $this->created_user,
            'p.updated_at' => $this->updated_at,
            'p.updated_user' => $this->updated_user,
            'p.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'p.name', $this->name]);

        return $dataProvider;
    }
}
