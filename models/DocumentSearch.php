<?php

namespace albertborsos\yii2cmsmultisite\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use albertborsos\yii2cmsmultisite\models\Document;

/**
 * DocumentSearch represents the model behind the search form about `albertborsos\yii2cmsmultisite\models\Document`.
 */
class DocumentSearch extends Document
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'preview_image_id', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['name', 'filename', 'sha1_original', 'sha1_this', 'right_to_view', 'url', 'preview_max_width', 'preview_max_height'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Document::find()->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'preview_image_id' => $this->preview_image_id,
            'created_at' => $this->created_at,
            'created_user' => $this->created_user,
            'updated_at' => $this->updated_at,
            'updated_user' => $this->updated_user,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'filename', $this->filename])
            ->andFilterWhere(['like', 'sha1_original', $this->sha1_original])
            ->andFilterWhere(['like', 'sha1_this', $this->sha1_this])
            ->andFilterWhere(['like', 'right_to_view', $this->right_to_view])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'preview_max_width', $this->preview_max_width])
            ->andFilterWhere(['like', 'preview_max_height', $this->preview_max_height]);

        return $dataProvider;
    }
}
