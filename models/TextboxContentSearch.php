<?php

namespace albertborsos\yii2cmsmultisite\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use albertborsos\yii2cmsmultisite\models\TextboxContent;

/**
 * TextboxContentSearch represents the model behind the search form about `albertborsos\yii2cmsmultisite\models\TextboxContent`.
 */
class TextboxContentSearch extends TextboxContent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'textbox_id', 'published', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['site_language_code', 'content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TextboxContent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            'pagination' => false,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'textbox_id' => $this->textbox_id,
            'published' => $this->published,
            'created_at' => $this->created_at,
            'created_user' => $this->created_user,
            'updated_at' => $this->updated_at,
            'updated_user' => $this->updated_user,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'site_language_code', $this->site_language_code])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
