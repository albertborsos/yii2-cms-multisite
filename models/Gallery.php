<?php

namespace albertborsos\yii2cmsmultisite\models;

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\traits\CacheTrait;
use albertborsos\yii2historizer\Historizer;
use albertborsos\yii2lib\db\ActiveRecord;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "tbl_cms2_gallery".
 *
 * @property integer $id
 * @property string $name
 * @property string $replace_id
 * @property string $order_by
 * @property integer $page_size
 * @property integer $items_in_a_row
 * @property integer $created_at
 * @property integer $created_user
 * @property integer $updated_at
 * @property integer $updated_user
 * @property integer $status
 *
 * @property GalleryDocument[] $galleryDocuments
 */
class Gallery extends ActiveRecord
{
    use CacheTrait;

    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_DELETED  = 2;

    const PREFIX = '[#';
    const POSTFIX = '#]';

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'clearCache']);
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'clearCache']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_cms2_gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /* [['firstName', 'lastName'], 'default'], // resets to null */
        /* [['firstName', 'lastName'], 'trim'], // trim */
        return [
            [['name', 'replace_id', 'order_by'], 'trim'],
            [['name', 'replace_id', 'order_by'], 'default'],
            [['page_size', 'items_in_a_row', 'created_at', 'created_user', 'updated_at', 'updated_user', 'status'], 'integer'],
            [['name', 'replace_id'], 'string', 'max' => 255],
            [['order_by'], 'string', 'max' => 5],
            [['replace_id'], 'unique'],
            [['replace_id'], 'validateUniqueReplaceKey'],
        ];
    }

    public function validateUniqueReplaceKey($attribute, $params){
        // check this key does not exists in textbox
        $textbox = Textbox::find()->where([
            'replace_key' => $this->replace_id,
        ])->one();

        if(!is_null($textbox)){
            $this->addError('replace_id', 'Ez az azonosító már foglalt!');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'replace_id' => 'Replace ID',
            'order_by' => 'Order By',
            'page_size' => 'Page Size',
            'items_in_a_row' => 'Items In A Row',
            'created_at' => 'Created At',
            'created_user' => 'Created User',
            'updated_at' => 'Updated At',
            'updated_user' => 'Updated User',
            'status' => 'Status',
        ];
    }

    public function getGalleryDocuments(): ActiveQuery
    {
        return $this->hasMany(GalleryDocument::className(), ['gallery_id' => 'id'])
            ->andWhere(['status' => GalleryDocument::STATUS_ACTIVE])
            ->orderBy($this->getOrderByCondition());
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()){
            if($this->replace_id == Gallery::PREFIX . Gallery::POSTFIX){
                $this->replace_id = null;
            }
            return true;
        }else{
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)){
            Historizer::createArchive($this);
            $this->setOwnerAndTime();
            return true;
        }else{
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()){
            Historizer::createArchive($this);
            return true;
        }else{
            return false;
        }
    }

    public function getReplaceID(){
        if (is_null($this->replace_id)){
            $this->replace_id = '[#gallery-'.$this->getNextID().'#]';
        }
        return $this->replace_id;
    }

    public function setReplaceID()
    {
        $convertedKey = DataProvider::replaceCharsToUrl($this->replace_id);
        $this->replace_id = self::PREFIX . $convertedKey . self::POSTFIX;
    }

    public static function insertGallery($content){
        $galleries = Yii::$app->db->cache(function ($db) {
            return Gallery::find()
                ->where(['status' => Gallery::STATUS_ACTIVE])
                ->andWhere('replace_id IS NOT NULL')->all();
        });
        /** @var Gallery $gallery */
        foreach($galleries as $gallery){
            if (strpos($content, $gallery->replace_id) !== false){
                $content = str_replace($gallery->replace_id, $gallery->generate(), $content);
            }
        }
        return $content;
    }

    public function generate($page = 0){
        return \albertborsos\yii2cmsmultisite\components\Gallery::widget([
            'id' => 'gallery-'.$this->id,
            'header' => null,
            'galleryId' => $this->id,
            'page' => $page,
            'pageSize' => $this->page_size,
            'order' => $this->order_by,
            'itemNumInRow' => $this->items_in_a_row,
        ]);
    }

    public static function renderByReplaceID($replace_id){
        $gallery = Gallery::find()
            ->where([
                'status' => Gallery::STATUS_ACTIVE,
                'replace_id' => $replace_id,
            ])->one();

        if(is_null($gallery)){
            return $replace_id;
        }else{
            return $gallery->generate();
        }
    }

    public function getOrderByCondition(): array
    {
        $sortOrder = $this->order_by == 'ASC'
            ? ['sort_order' => SORT_ASC]
            : ['sort_order' => SORT_DESC];

        $idOrder = $this->order_by = 'ASC'
            ? ['id' => SORT_DESC]
            : ['id' => SORT_ASC];

        return array_merge($sortOrder, $idOrder);
    }
}
