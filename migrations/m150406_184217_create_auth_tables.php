<?php

use yii\db\Schema;
use yii\db\Migration;

class m150406_184217_create_auth_tables extends Migration
{
    public function up()
    {
        $this->execute("# Dump of table auth_rule
                        # ------------------------------------------------------------

                        CREATE TABLE IF NOT EXISTS `auth_rule` (
                          `name` varchar(64) NOT NULL,
                          `data` text,
                          `created_at` int(11) DEFAULT NULL,
                          `updated_at` int(11) DEFAULT NULL,
                          PRIMARY KEY (`name`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

                        # Dump of table auth_item
                        # ------------------------------------------------------------

                        CREATE TABLE IF NOT EXISTS `auth_item` (
                          `name` varchar(64) NOT NULL,
                          `type` int(11) NOT NULL,
                          `description` text,
                          `rule_name` varchar(64) DEFAULT NULL,
                          `data` text,
                          `created_at` int(11) DEFAULT NULL,
                          `updated_at` int(11) DEFAULT NULL,
                          PRIMARY KEY (`name`),
                          KEY `rule_name` (`rule_name`),
                          KEY `type` (`type`),
                          CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


                        INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`)
                        VALUES
                            ('admin',2,'Adminisztrátor',NULL,NULL,NULL,NULL),
                            ('editor',2,'Szerkesztő',NULL,NULL,NULL,NULL),
                            ('guest',2,'Vendég',NULL,NULL,NULL,NULL),
                            ('reader',2,'Olvasó',NULL,NULL,NULL,NULL);

                        # Dump of table auth_assignment
                        # ------------------------------------------------------------

                        CREATE TABLE IF NOT EXISTS `auth_assignment` (
                          `item_name` varchar(64) NOT NULL,
                          `user_id` varchar(64) NOT NULL,
                          `created_at` int(11) DEFAULT NULL,
                          PRIMARY KEY (`item_name`,`user_id`),
                          CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


                        # Dump of table auth_item_child
                        # ------------------------------------------------------------

                        CREATE TABLE IF NOT EXISTS `auth_item_child` (
                          `parent` varchar(64) NOT NULL,
                          `child` varchar(64) NOT NULL,
                          PRIMARY KEY (`parent`,`child`),
                          KEY `child` (`child`),
                          CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
                          CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

                        INSERT INTO `auth_item_child` (`parent`, `child`)
                        VALUES
                            ('admin','editor'),
                            ('editor','reader'),
                            ('reader','guest');

                        ");
    }

    public function down()
    {
        $this->dropTable('auth_rule');
        $this->dropTable('auth_item_child');
        $this->dropTable('auth_item');
        $this->dropTable('auth_assignment');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
