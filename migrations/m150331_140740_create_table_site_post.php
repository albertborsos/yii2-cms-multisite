<?php

use yii\db\Schema;
use yii\db\Migration;

class m150331_140740_create_table_site_post extends Migration
{
    public function up()
    {
        $this->createTable('tbl_cms2_site_post', [
            "id" => Schema::TYPE_PK,
            "site_id" => Schema::TYPE_INTEGER,
            "parent_post_id" => Schema::TYPE_INTEGER,
            "type" => 'TINYINT(1)',
            "name" => Schema::TYPE_STRING,
            "sort_order" => 'TINYINT',
            "commentable" => 'TINYINT(1)',
            "shareable" => 'TINYINT(1)',
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey('tbl_cms2_site_post_ibfk_1', 'tbl_cms2_site_post', 'site_id', 'tbl_cms2_site', 'id');
        $this->addForeignKey('tbl_cms2_site_post_ibfk_2', 'tbl_cms2_site_post', 'parent_post_id', 'tbl_cms2_site_post', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('tbl_cms2_site_post_ibfk_1', 'tbl_cms2_site_post');
        $this->dropForeignKey('tbl_cms2_site_post_ibfk_2', 'tbl_cms2_site_post');

        $this->dropTable('tbl_cms2_site_post');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
