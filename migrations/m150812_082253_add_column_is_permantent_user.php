<?php

use yii\db\Schema;
use yii\db\Migration;

class m150812_082253_add_column_is_permantent_user extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_user_users', 'is_permanent', Schema::TYPE_BOOLEAN . ' DEFAULT 0 AFTER username');
    }

    public function down()
    {
        $this->dropColumn('tbl_user_users', 'is_permanent');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
