<?php

use yii\db\Migration;

/**
 * Class m211008_100251_add_canonical_url_columnt_to_post_meta_table
 */
class m211008_100251_add_canonical_url_columnt_to_post_meta_table extends Migration
{
    public function safeUp(): void
    {
        $this->addColumn('tbl_cms2_site_post_meta', 'canonical_url', $this->string()->after('canonical_post_id'));
        $this->alterColumn('tbl_cms2_site_post_meta', 'title', $this->string(70));
        $this->alterColumn('tbl_cms2_site_post_meta', 'og_title', $this->string(70));
    }

    public function safeDown(): void
    {
        $this->alterColumn('tbl_cms2_site_post_meta', 'og_title', $this->string(55));
        $this->alterColumn('tbl_cms2_site_post_meta', 'title', $this->string(55));
        $this->dropColumn('tbl_cms2_site_post_meta', 'canonical_url');
    }
}
