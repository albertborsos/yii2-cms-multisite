<?php

use yii\db\Schema;
use yii\db\Migration;

class m160423_134043_add_node_columns extends Migration
{
    const TBL_SITEPOST = 'tbl_cms2_site_post';

    public function up()
    {
        $this->addColumn(self::TBL_SITEPOST, 'lft', Schema::TYPE_INTEGER . ' AFTER sort_order');
        $this->addColumn(self::TBL_SITEPOST, 'rgt', Schema::TYPE_INTEGER . ' AFTER sort_order');
    }

    public function down()
    {
        $this->dropColumn(self::TBL_SITEPOST, 'lft');
        $this->dropColumn(self::TBL_SITEPOST, 'rgt');
    }
}
