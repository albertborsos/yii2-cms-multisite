<?php

use yii\db\Schema;
use yii\db\Migration;

class m150406_112900_add_seo_published extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_cms2_site_post_meta', 'published', 'TINYINT(1) AFTER fb_app_id');
    }

    public function down()
    {
        $this->dropColumn('tbl_cms2_site_post_meta', 'published');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
