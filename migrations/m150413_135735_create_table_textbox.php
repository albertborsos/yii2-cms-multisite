<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_135735_create_table_textbox extends Migration
{
    public function up()
    {
        $this->createTable('tbl_cms2_textbox', [
            "id" => Schema::TYPE_PK,
            "name" => Schema::TYPE_STRING,
            "replace_key" => Schema::TYPE_STRING,
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        $this->dropTable('tbl_cms2_textbox');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
