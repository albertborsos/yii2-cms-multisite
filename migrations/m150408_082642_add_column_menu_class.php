<?php

use yii\db\Schema;
use yii\db\Migration;

class m150408_082642_add_column_menu_class extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_cms2_site_post', 'menu_item_class', 'string AFTER shareable');
    }

    public function down()
    {
        $this->dropColumn('tbl_cms2_site_post', 'menu_item_class');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
