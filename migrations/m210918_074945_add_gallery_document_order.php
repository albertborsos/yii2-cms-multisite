<?php

use yii\db\Migration;

/**
 * Class m210918_074945_add_gallery_document_order
 */
class m210918_074945_add_gallery_document_order extends Migration
{
    public function safeUp():void
    {
        $this->addColumn('tbl_cms2_gallery_document', 'sort_order', $this->integer()->after('url'));
    }

    public function safeDown():void
    {
        $this->dropColumn('tbl_cms2_gallery_document', 'sort_order');
    }
}
