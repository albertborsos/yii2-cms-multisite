<?php

use yii\db\Schema;
use yii\db\Migration;

class m150331_140920_create_table_site_gallery_image extends Migration
{
    public function up()
    {
        $this->createTable('tbl_cms2_site_gallery_image', [
            "id" => Schema::TYPE_PK,
            "gallery_id" => Schema::TYPE_INTEGER,
            "filename" => Schema::TYPE_STRING,
            "alt" => Schema::TYPE_STRING,
            "title" => Schema::TYPE_STRING,
            "link" => Schema::TYPE_STRING,
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey('tbl_cms2_site_gallery_image_ibfk_1', 'tbl_cms2_site_gallery_image', 'gallery_id', 'tbl_cms2_site_gallery', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('tbl_cms2_site_gallery_image_ibfk_1', 'tbl_cms2_site_gallery_image');

        $this->dropTable('tbl_cms2_site_gallery_image');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
