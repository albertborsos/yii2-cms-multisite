<?php

use yii\db\Schema;
use yii\db\Migration;

class m150609_084327_add_column_slug extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_cms2_site_post_meta', 'slug', Schema::TYPE_STRING . ' AFTER url');
    }

    public function down()
    {
        $this->dropColumn('tbl_cms2_site_post_meta', 'slug');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
