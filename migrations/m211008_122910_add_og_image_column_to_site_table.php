<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%site}}`.
 */
class m211008_122910_add_og_image_column_to_site_table extends Migration
{
    public function safeUp(): void
    {
        $this->addColumn('tbl_cms2_site', 'og_image_id', $this->integer()->after('type'));
    }

    public function safeDown(): void
    {
        $this->dropColumn('tbl_cms2_site', 'og_image_id');
    }
}
