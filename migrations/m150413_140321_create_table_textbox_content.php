<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_140321_create_table_textbox_content extends Migration
{
    public function up()
    {
        $this->createTable('tbl_cms2_textbox_content', [
            "id" => Schema::TYPE_PK,
            "textbox_id" => Schema::TYPE_INTEGER,
            "site_language_code" => Schema::TYPE_STRING,
            "content" => Schema::TYPE_TEXT,
            "published" => 'TINYINT(1)',
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey('tbl_cms2_textbox_content_ibfk_1', 'tbl_cms2_textbox_content', 'textbox_id', 'tbl_cms2_textbox', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('tbl_cms2_textbox_content_ibfk_1', 'tbl_cms2_textbox_content');

        $this->dropTable('tbl_cms2_textbox_content');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
