<?php

use yii\db\Schema;
use yii\db\Migration;

class m150415_100610_create_table_document_content extends Migration
{

    private $tableName = 'tbl_cms2_document_content';

    public function up()
    {
        $this->createTable($this->tableName, [
            "id" => Schema::TYPE_PK,
            "document_id" => Schema::TYPE_INTEGER,
            "language_code" => Schema::TYPE_STRING,
            "title" => Schema::TYPE_STRING,
            "alt_description" => Schema::TYPE_STRING,
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey($this->tableName . '_ibfk_1', $this->tableName, 'document_id', 'tbl_cms2_document', 'id');
    }

    public function down()
    {
        $this->dropForeignKey($this->tableName . '_ibfk_1', $this->tableName);
        $this->dropTable($this->tableName);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
