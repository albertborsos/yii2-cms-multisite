<?php

use yii\db\Schema;
use yii\db\Migration;

class m150415_100552_create_table_documents extends Migration
{
    private $tableName = 'tbl_cms2_document';

    public function up()
    {
        $this->createTable($this->tableName, [
            "id" => Schema::TYPE_PK,
            "type" => "TINYINT(1)",
            "name" => Schema::TYPE_STRING,
            "filename" => Schema::TYPE_STRING,
            "sha1_original" => Schema::TYPE_STRING,
            "sha1_this" => Schema::TYPE_STRING,
            "right_to_view" => Schema::TYPE_STRING,
            "url" => Schema::TYPE_STRING,
            "preview_image_id" => Schema::TYPE_INTEGER,
            "preview_max_width" => Schema::TYPE_STRING,
            "preview_max_height" => Schema::TYPE_STRING,
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey($this->tableName . '_ibfk_1', $this->tableName, 'preview_image_id', $this->tableName, 'id');
    }

    public function down()
    {
        $this->dropForeignKey($this->tableName . '_ibfk_1', $this->tableName);

        $this->dropTable($this->tableName);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
