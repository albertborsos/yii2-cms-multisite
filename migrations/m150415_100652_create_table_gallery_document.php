<?php

use yii\db\Schema;
use yii\db\Migration;

class m150415_100652_create_table_gallery_document extends Migration
{
    private $tableName = 'tbl_cms2_gallery_document';

    public function up()
    {
        $this->createTable($this->tableName, [
            "id" => Schema::TYPE_PK,
            "gallery_id" => Schema::TYPE_INTEGER,
            "document_id" => Schema::TYPE_INTEGER,
            "url" => Schema::TYPE_STRING,
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey($this->tableName . '_ibfk_1', $this->tableName, 'gallery_id', 'tbl_cms2_gallery', 'id');
        $this->addForeignKey($this->tableName . '_ibfk_2', $this->tableName, 'document_id', 'tbl_cms2_document', 'id');
    }

    public function down()
    {
        $this->dropForeignKey($this->tableName . '_ibfk_2', $this->tableName);
        $this->dropForeignKey($this->tableName . '_ibfk_1', $this->tableName);

        $this->dropTable($this->tableName);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
