<?php

use yii\db\Migration;

/**
 * Class m180330_100646_textbox_content_widget_field
 */
class m180330_100646_textbox_content_widget_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tbl_cms2_textbox_content', 'widget', $this->string()->after('site_language_code'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tbl_cms2_textbox_content', 'widget');
    }
}
