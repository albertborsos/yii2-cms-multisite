<?php

use yii\db\Schema;
use yii\db\Migration;

class m150331_101921_create_table_site extends Migration
{
    public function up()
    {
        $this->createTable('tbl_cms2_site', [
            "id" => Schema::TYPE_PK,
            "name" => Schema::TYPE_STRING,
            "baseUrl" => Schema::TYPE_STRING,
            "type" => 'TINYINT(1)',
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        $this->dropTable('tbl_cms2_site');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
