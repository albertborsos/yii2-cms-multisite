<?php

use yii\db\Schema;
use yii\db\Migration;

class m150408_085745_add_column_menu_link extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_cms2_site_post', 'link_post_id', 'integer AFTER parent_post_id');
    }

    public function down()
    {
        $this->dropColumn('tbl_cms2_site_post', 'link_post_id');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
