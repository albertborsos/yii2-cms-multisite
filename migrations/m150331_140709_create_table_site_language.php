<?php

use yii\db\Schema;
use yii\db\Migration;

class m150331_140709_create_table_site_language extends Migration
{
    public function up()
    {
        $this->createTable('tbl_cms2_site_language', [
            "id" => Schema::TYPE_PK,
            "site_id" => Schema::TYPE_INTEGER,
            "code" => Schema::TYPE_STRING,
            "name" => Schema::TYPE_STRING,
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey('tbl_cms2_site_language_ibfk_1', 'tbl_cms2_site_language', 'site_id', 'tbl_cms2_site', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('tbl_cms2_site_language_ibfk_1', 'tbl_cms2_site_language');

        $this->dropTable('tbl_cms2_site_language');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
