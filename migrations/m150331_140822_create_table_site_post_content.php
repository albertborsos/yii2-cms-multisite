<?php

use yii\db\Schema;
use yii\db\Migration;

class m150331_140822_create_table_site_post_content extends Migration
{
    public function up()
    {
        $this->createTable('tbl_cms2_site_post_content', [
            "id" => Schema::TYPE_PK,
            "site_post_id" => Schema::TYPE_INTEGER,
            "site_language_id" => Schema::TYPE_INTEGER,
            "name" => Schema::TYPE_STRING,
            "content_short" => Schema::TYPE_TEXT,
            "content_main" => Schema::TYPE_TEXT,
            "date_show" => Schema::TYPE_DATETIME,
            "published" => 'TINYINT(1)',
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey('tbl_cms2_site_post_content_ibfk_1', 'tbl_cms2_site_post_content', 'site_post_id', 'tbl_cms2_site_post', 'id');
        $this->addForeignKey('tbl_cms2_site_post_content_ibfk_2', 'tbl_cms2_site_post_content', 'site_language_id', 'tbl_cms2_site_language', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('tbl_cms2_site_post_content_ibfk_1', 'tbl_cms2_site_post_content');
        $this->dropForeignKey('tbl_cms2_site_post_content_ibfk_2', 'tbl_cms2_site_post_content');

        $this->dropTable('tbl_cms2_site_post_content');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
