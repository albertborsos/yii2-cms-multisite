<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_110256_add_column_post_content_widget extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_cms2_site_post_content', 'widget', 'string AFTER `layout`');
    }

    public function down()
    {
        $this->dropColumn('tbl_cms2_site_post_content', 'widget');
    }
}
