<?php

use yii\db\Schema;
use yii\db\Migration;

class m150509_082850_add_post_layout extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_cms2_site_post_content', 'layout', 'string AFTER `site_language_id`');
    }

    public function down()
    {
        $this->dropColumn('tbl_cms2_site_post_content', 'layout');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
