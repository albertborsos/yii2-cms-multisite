<?php

use yii\db\Schema;
use yii\db\Migration;

class m150331_140910_create_table_site_gallery extends Migration
{
    public function up()
    {
        $this->createTable('tbl_cms2_site_gallery', [
            "id" => Schema::TYPE_PK,
            "site_id" => Schema::TYPE_INTEGER,
            "replace_id" => Schema::TYPE_STRING,
            "name" => Schema::TYPE_STRING,
            "title" => Schema::TYPE_STRING,
            "order_by" => 'VARCHAR(5)',
            "page_size" => 'TINYINT',
            "items_in_a_row" => 'TINYINT',
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey('tbl_cms2_site_gallery_ibfk_1', 'tbl_cms2_site_gallery', 'site_id', 'tbl_cms2_site', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('tbl_cms2_site_gallery_ibfk_1', 'tbl_cms2_site_gallery');

        $this->dropTable('tbl_cms2_site_gallery');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
