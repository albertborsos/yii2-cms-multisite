<?php

use yii\db\Schema;
use yii\db\Migration;

class m151218_112618_add_column_right_to_view_post extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_cms2_site_post', 'right_to_view', Schema::TYPE_STRING . ' AFTER menu_item_class');
    }

    public function down()
    {
        $this->dropColumn('tbl_cms2_site_post', 'right_to_view');
    }
}
