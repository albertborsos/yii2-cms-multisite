<?php

use yii\db\Migration;

class m160605_101745_fix_post_sort_order_size extends Migration
{
    public function up()
    {
        $this->alterColumn('tbl_cms2_site_post', 'sort_order', \yii\db\Schema::TYPE_INTEGER);

        $this->execute('update tbl_cms2_site_post set `sort_order`=(`sort_order`+`id` - 40) where `sort_order`=127');
    }

    public function down()
    {
        $this->alterColumn('tbl_cms2_site_post', 'sort_order', \yii\db\Schema::TYPE_SMALLINT);
    }
}
