<?php

use yii\db\Schema;
use yii\db\Migration;

class m150331_140836_create_table_site_post_meta extends Migration
{
    public function up()
    {
        $this->createTable('tbl_cms2_site_post_meta', [
            "id" => Schema::TYPE_PK,
            "site_post_id" => Schema::TYPE_INTEGER,
            "site_language_id" => Schema::TYPE_INTEGER,
            "title" => 'VARCHAR(55)',
            "meta_description" => 'VARCHAR(160)',
            "meta_robots" => 'TINYINT(1)',
            "url" => Schema::TYPE_STRING,
            "canonical_post_id" => Schema::TYPE_INTEGER,
            "og_title" => 'VARCHAR(55)',
            "og_description" => 'VARCHAR(160)',
            "og_site_name" => 'VARCHAR(50)',
            "og_image_id" => Schema::TYPE_INTEGER,
            "fb_app_id" => Schema::TYPE_STRING,
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey('tbl_cms2_site_post_meta_ibfk_1', 'tbl_cms2_site_post_meta', 'site_post_id', 'tbl_cms2_site_post', 'id');
        $this->addForeignKey('tbl_cms2_site_post_meta_ibfk_2', 'tbl_cms2_site_post_meta', 'site_language_id', 'tbl_cms2_site_language', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('tbl_cms2_site_post_meta_ibfk_1', 'tbl_cms2_site_post_meta');
        $this->dropForeignKey('tbl_cms2_site_post_meta_ibfk_2', 'tbl_cms2_site_post_meta');

        $this->dropTable('tbl_cms2_site_post_meta');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
