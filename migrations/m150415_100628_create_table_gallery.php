<?php

use yii\db\Schema;
use yii\db\Migration;

class m150415_100628_create_table_gallery extends Migration
{
    private $tableName = 'tbl_cms2_gallery';

    public function up()
    {
        $this->createTable($this->tableName, [
            "id" => Schema::TYPE_PK,
            "name" => Schema::TYPE_STRING,
            "replace_id" => Schema::TYPE_STRING,
            "order_by" => 'VARCHAR(5)',
            "page_size" => 'TINYINT',
            "items_in_a_row" => 'TINYINT',
            "created_at" => Schema::TYPE_INTEGER,
            "created_user" => Schema::TYPE_INTEGER,
            "updated_at" => Schema::TYPE_INTEGER,
            "updated_user" => Schema::TYPE_INTEGER,
            "status" => 'TINYINT(1)',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
