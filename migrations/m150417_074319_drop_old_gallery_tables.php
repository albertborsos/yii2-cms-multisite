<?php

use yii\db\Schema;
use yii\db\Migration;

class m150417_074319_drop_old_gallery_tables extends Migration
{
    public function up()
    {
        $this->truncateTable('tbl_cms2_site_gallery_image');
        $this->dropForeignKey('tbl_cms2_site_gallery_image_ibfk_1', 'tbl_cms2_site_gallery_image');
        $this->dropTable('tbl_cms2_site_gallery_image');

        $this->truncateTable('tbl_cms2_site_gallery');
        $this->dropForeignKey('tbl_cms2_site_gallery_ibfk_1', 'tbl_cms2_site_gallery');
        $this->dropTable('tbl_cms2_site_gallery');
    }

    public function down()
    {
        echo "m150417_074319_drop_old_gallery_tables cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
