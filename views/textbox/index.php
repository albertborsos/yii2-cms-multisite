<?php

use albertborsos\yii2cmsmultisite\models\SiteLanguage;
use albertborsos\yii2cmsmultisite\models\TextboxContent;
use albertborsos\yii2lib\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use albertborsos\yii2cmsmultisite\components\DataProvider;

/* @var $this yii\web\View */
/* @var $searchModel albertborsos\yii2cmsmultisite\models\TextboxSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="textbox-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Szövegdobozok</h3>',
            'type' => 'default',
            'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> Új Szövegdoboz', ['create'], ['class' => 'btn btn-success']),
            'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Szűrések törlése', ['index'], ['class' => 'btn btn-info']),
            'showFooter' => false
        ],
        'floatHeader' => true,
        'export' => false, // [], ha exportálni szeretnél
        'exportConfig' => [
            GridView::CSV => [
                'label' => 'CSV',
                'icon' => 'floppy-open',
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'colDelimiter' => ",",
                'rowDelimiter' => "\r\n",
                'filename' => 'grid-export',
                'alertMsg' => 'The CSV export file will be generated for download.',
                'options' => ['title' => 'Mentés CSV-ként']
            ],
            GridView::EXCEL => [
                'label' => 'Excel',
                'icon' => 'floppy-remove',
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'worksheet' => 'ExportWorksheet',
                'filename' => 'grid-export',
                'alertMsg' => 'The EXCEL export file will be generated for download.',
                'cssFile' => '',
                'options' => ['title' => 'Mentés XLS-ként']
            ],
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model['name'];
                },
            ],
            [
                'attribute' => 'replace_key',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return Html::input('text', $model['replace_key'], $model['replace_key'], [
                        'class' => 'form-control',
                        'readonly' => 'readonly'
                    ]);
                },
            ],
            [
                'header' => Yii::t('app', 'Contents'),
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    $out = '';

                    $languageCodes = SiteLanguage::find()->select(['code'])->distinct()->where([
                        'status' => SiteLanguage::STATUS_ACTIVE,
                    ])->asArray()->all();

                    $languageCodes = array_values(ArrayHelper::map($languageCodes, 'code', 'code'));

                    foreach ($languageCodes as $languageCode) {
                        $content = TextboxContent::find()->where([
                            'textbox_id' => $model->id,
                            'site_language_code' => $languageCode,
                            'published' => 1,
                        ])->orderBy(['id' => SORT_DESC])->one();

                        if(!is_null($content)){
                            $badgeClass = 'btn-success';
                            if(count($model->showAlerts($languageCode, false)) > 0){
                                $badgeClass = 'btn-warning';
                            }
                        }else{
                            $badgeClass = 'btn-danger';
                        }
                        $out .= Html::a(DataProvider::items('languages', $languageCode),
                            ['textbox-content/edit', 'textbox' => $model->id, 'language_code' => $languageCode],
                            ['class' => 'btn btn-block btn-xs '.$badgeClass]);
                    }
                    return $out;
                },
            ],
            [
                'attribute' => 'status',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_DROPDOWN, 'status', DataProvider::items('status'));
                },
                'filter' => DataProvider::items('status'),
            ],

            [
                'attribute' => 'updated_at',
                //'header'      => 'Utolsó módosítás',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'format' => 'raw',
                'headerOptions' => ['class' => 'text-center'],
                'value' => function ($model, $index, $widget) {
                    return \albertborsos\yii2lib\db\ActiveRecord::showLastModifiedInfo($model);
                },
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{update} {delete}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return [$action, 'id' => $model->id];
                },
                //'dropdown' => true,
                'width' => '120px',
                'buttons' => [
                    'update' => function ($url, $model) {
                        if (Yii::$app->user->can('editor')) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('yii', 'Update'),
                                'data-pjax' => '0',
                                'class' => 'btn btn-sm btn-default'
                            ]);
                        } else {
                            return '';
                        }
                    },
                    'delete' => function ($url, $model) {
                        if (Yii::$app->user->can('editor')) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                                'class' => 'btn btn-sm btn-default'
                            ]);
                        } else {
                            return '';
                        }
                    },
                ],
            ],
        ],
    ]); ?>

</div>
