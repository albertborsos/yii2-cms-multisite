<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\Textbox */

?>
<div class="row">
<div class="col-md-6">
<div class="textbox-create">

    <legend>Új szövegdoboz létrehozása</legend>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
