<?php

use albertborsos\yii2cmsmultisite\models\Textbox;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use albertborsos\yii2cmsmultisite\components\DataProvider;
use kartik\widgets\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\widgets\TimePicker;
use \vova07\imperavi\Widget as Redactor;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\Textbox */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="textbox-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => '{label}<div class="col-md-8">{input}</div><div class="col-md-8 col-md-offset-4">{error}</div>',
            'labelOptions' => ['class' => 'col-md-4 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?php
        $model->replace_key = str_replace([Textbox::PREFIX, Textbox::POSTFIX], '', $model->replace_key);
        echo $form->field($model, 'replace_key', [
            'inputTemplate' => '<div class="input-group"><span class="input-group-addon">[#</span>{input}<span class="input-group-addon">#]</span></div>',
        ])->textInput(['maxlength' => 255]) ?>

      <?= $form->field($model, 'status')->dropDownList(DataProvider::items('status')) ?>

    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <?= Html::submitButton($model->isNewRecord ? 'Létrehoz' : 'Módosít', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
