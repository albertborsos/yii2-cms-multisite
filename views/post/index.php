<?php

use albertborsos\yii2cmsmultisite\components\SortableGridView;
use albertborsos\yii2cmsmultisite\models\SitePost;
use albertborsos\yii2cmsmultisite\models\SitePostContent;
use albertborsos\yii2lib\db\ActiveRecord;
use kartik\grid\GridView;
use yii\helpers\Html;
use albertborsos\yii2cmsmultisite\components\DataProvider;

/* @var $this yii\web\View */
/* @var $searchModel albertborsos\yii2cmsmultisite\models\SitePostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="site-post-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
        $buttons = function(){
            $out = '';
            $buttonMenu = Html::a('<i class="glyphicon glyphicon-plus"></i> Új Menüpont',
                ['create', 'site' => Yii::$app->request->get('site'), 'type' => SitePost::TYPE_MENU],
                ['class' => 'btn btn-success']);
            $buttonMenuLink = Html::a('<i class="glyphicon glyphicon-plus"></i> Új tartalom nélküli menülink',
                ['create', 'site' => Yii::$app->request->get('site'), 'type' => SitePost::TYPE_DROPDOWN],
                ['class' => 'btn btn-warning']);
            /*$buttonMenuLink = Html::a('<i class="glyphicon glyphicon-plus"></i> Új Menülink',
                ['create', 'site' => Yii::$app->request->get('site'), 'type' => SitePost::TYPE_MENU_LINK],
                ['class' => 'btn btn-success']);*/
            $buttonBlog = Html::a('<i class="glyphicon glyphicon-plus"></i> Új Blogbejegyzés',
                ['create', 'site' => Yii::$app->request->get('site'), 'type' => SitePost::TYPE_BLOG],
                ['class' => 'btn btn-success']);
            $buttonDropDown = Html::a('<i class="glyphicon glyphicon-plus"></i> Új Legördülő menü',
                    ['create', 'site' => Yii::$app->request->get('site'), 'type' => SitePost::TYPE_DROPDOWN],
                    ['class' => 'btn btn-info']);

            switch(Yii::$app->request->get('type')){
                case SitePost::TYPE_MENU:
                    $out .= $buttonMenu;
                    $out .= ' '.$buttonMenuLink;
                    $out .= ' '.$buttonDropDown;
                    break;
                case SitePost::TYPE_BLOG:
                    $out .= $buttonBlog;
                    $out .= ' '.$buttonDropDown;
                    break;
                default:
                    $out .= $buttonMenu;
                    $out .= ' '.$buttonMenuLink;
                    $out .= ' '.$buttonBlog;
                    $out .= ' '.$buttonDropDown;
                    break;
            }

            return $out;
        };

    ?>

    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Bejegyzések</h3>',
            'type' => 'default',
            'before' => $buttons(),
            'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Szűrések törlése', ['index'], ['class' => 'btn btn-info']),
            'showFooter' => false
        ],
        'floatHeader' => true,
        'export' => false, // [], ha exportálni szeretnél
        'exportConfig' => [
            GridView::CSV => [
                'label' => 'CSV',
                'icon' => 'floppy-open',
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'colDelimiter' => ",",
                'rowDelimiter' => "\r\n",
                'filename' => 'grid-export',
                'alertMsg' => 'The CSV export file will be generated for download.',
                'options' => ['title' => 'Mentés CSV-ként']
            ],
            GridView::EXCEL => [
                'label' => 'Excel',
                'icon' => 'floppy-remove',
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'worksheet' => 'ExportWorksheet',
                'filename' => 'grid-export',
                'alertMsg' => 'The EXCEL export file will be generated for download.',
                'cssFile' => '',
                'options' => ['title' => 'Mentés XLS-ként']
            ],
        ],
        'rowOptions' => function($model, $key, $index, $grid){
            if ($model->type == SitePost::TYPE_DROPDOWN || $model->type == SitePost::TYPE_MENU_LINK){
                return ['class' => 'info'];
            }elseif($model->status == SitePost::STATUS_ACTIVE){
                return ['class' => 'success'];
            }elseif($model->status == SitePost::STATUS_DELETED){
                return ['class' => 'danger'];
            }elseif($model->status == SitePost::STATUS_INACTIVE){
                return ['class' => 'warning'];
            }
            return null;
        },
        'columns' => [
            [
                'header' => '',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center', 'style' => 'width:5%;min-width:30px;'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return \rmrevin\yii\fontawesome\FA::icon(\rmrevin\yii\fontawesome\FA::_ARROWS_V);
                },
            ],
            [
                'attribute' => 'name',
                'hAlign' => 'left',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    $name = $model->name;
                    if($model->type == SitePost::TYPE_MENU_LINK){
                        $name = $model->linkPost->name;
                    }
                    return '<span class="tree-level level-' . $model->getTreeNodeAscii() . '"></span>' . $name;
                },
            ],
            [
                'attribute' => 'menu_item_class',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_TEXTINPUT, 'menu_item_class');
                },
                'visible' => in_array(Yii::$app->request->get('type'), [SitePost::TYPE_MENU, SitePost::TYPE_MENU_LINK, SitePost::TYPE_DROPDOWN]),
            ],
            [
                'attribute' => 'right_to_view',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_TEXTINPUT, 'right_to_view');
                },
                'visible' => in_array(Yii::$app->request->get('type'), [SitePost::TYPE_MENU, SitePost::TYPE_MENU_LINK, SitePost::TYPE_DROPDOWN]),
            ],
            [
                'attribute' => 'site.baseUrl',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'visible' => is_null(Yii::$app->request->get('site')),
            ],
            [
                'attribute' => 'type',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return DataProvider::items('post_type', $model['type'], false);
                },
                'visible' => is_null(Yii::$app->request->get('type')),
            ],
            [
                'attribute' => 'commentable',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {/* @var $model albertborsos\yii2cmsmultisite\models\SitePost */
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_DROPDOWN, 'commentable', DataProvider::items('yesno'));
                },
                'visible' => !in_array(Yii::$app->request->get('type'), [SitePost::TYPE_DROPDOWN]),

            ],
            [
                'attribute' => 'shareable',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {/* @var $model albertborsos\yii2cmsmultisite\models\SitePost */
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_DROPDOWN, 'shareable', DataProvider::items('yesno'));
                },
                'visible' => !in_array(Yii::$app->request->get('type'), [SitePost::TYPE_DROPDOWN]),
            ],
            [
                'header' => Yii::t('app', 'Contents'),
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    $out = '';
                    switch($model->type){
                        case SitePost::TYPE_MENU:
                        case SitePost::TYPE_BLOG:
                        case SitePost::TYPE_DROPDOWN:
                        foreach ($model->site->siteLanguages as $language) {
                            $content = SitePostContent::find()->where([
                                'site_post_id' => $model->id,
                                'site_language_id' => $language->id,
                                'published' => 1,
                            ])->orderBy(['id' => SORT_DESC])->one();

                            if(!is_null($content)){
                                $badgeClass = 'btn-success';
                                if(count($model->showAlerts($language->id, false)) > 0){
                                    $badgeClass = 'btn-warning';
                                }
                            }else{
                                $badgeClass = 'btn-danger';
                            }
                            $out .= Html::a($language->name,
                                    ['post-content/edit', 'post' => $model->id, 'language_id' => $language->id],
                                    ['class' => 'btn btn-block btn-xs '.$badgeClass]);
                        }
                        break;
                    }
                    return $out;
                },
            ],
            [
                'attribute' => 'status',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_DROPDOWN, 'status', DataProvider::items('status'));
                },
                'filter' => DataProvider::items('status'),
            ],

            [
                'attribute' => 'updated_at',
                //'header'      => 'Utolsó módosítás',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'format' => 'raw',
                'headerOptions' => ['class' => 'text-center'],
                'value' => function ($model, $index, $widget) {
                    return \albertborsos\yii2lib\db\ActiveRecord::showLastModifiedInfo($model);
                },
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{update} {delete}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return [$action, 'id' => $model->id];
                },
                //'dropdown' => true,
                'width' => '120px',
                'buttons' => [
                    'update' => function ($url, $model) {
                        if (Yii::$app->user->can('editor')) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('yii', 'Update'),
                                'data-pjax' => '0',
                                'class' => 'btn btn-sm btn-default'
                            ]);
                        } else {
                            return '';
                        }
                    },
                    'delete' => function ($url, $model) {
                        if (Yii::$app->user->can('editor')) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                                'class' => 'btn btn-sm btn-default'
                            ]);
                        } else {
                            return '';
                        }
                    },
                ],
            ],
        ],
    ]); ?>

</div>
