<?php

use albertborsos\yii2cmsmultisite\models\SitePost;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use albertborsos\yii2cmsmultisite\components\DataProvider;
use kartik\widgets\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\widgets\TimePicker;
use \vova07\imperavi\Widget as Redactor;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\SitePost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-post-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => '{label}<div class="col-md-8">{input}</div><div class="col-md-8 col-md-offset-4">{error}</div>',
            'labelOptions' => ['class' => 'col-md-4 control-label'],
        ],
    ]); ?>

    <?php
    switch($model->type){
        case SitePost::TYPE_MENU:
        case SitePost::TYPE_DROPDOWN:
        case SitePost::TYPE_MENU_LINK:
            echo $form->field($model, 'parent_post_id')->dropDownList($model->getSelectParentMenu(), ['prompt' => 'Válassz! (nem kötelező)']);
            break;
    }
    ?>

    <?php
    switch($model->type){
        case SitePost::TYPE_MENU:
        case SitePost::TYPE_BLOG:
        case SitePost::TYPE_DROPDOWN:
            echo $form->field($model, 'name')->textInput(['maxlength' => 255]);
            break;
        case SitePost::TYPE_MENU_LINK:
            echo $form->field($model, 'link_post_id')->dropDownList($model->getLinkPosts(), ['prompt' => 'Válassz!']);
            break;
    }
    ?>

    <?php
    switch($model->type){
        case SitePost::TYPE_MENU:
        case SitePost::TYPE_MENU_LINK:
        case SitePost::TYPE_DROPDOWN:
            echo $form->field($model, 'sort_order')->dropDownList(DataProvider::items('sort_order'), [
                'prompt' => 'Következő érték',
            ]);

            echo  $form->field($model, 'menu_item_class')->textInput(['maxlength' => 255]);

            echo  $form->field($model, 'right_to_view')->textInput(['maxlength' => 255]);
            break;
    }
    ?>

    <?php
    switch($model->type){
        case SitePost::TYPE_MENU:
            echo $form->field($model, 'commentable')->dropDownList(DataProvider::items('noyes'));

            echo $form->field($model, 'shareable')->dropDownList(DataProvider::items('noyes'));
            break;
        case SitePost::TYPE_BLOG:
            echo $form->field($model, 'commentable')->dropDownList(DataProvider::items('yesno'));

            echo $form->field($model, 'shareable')->dropDownList(DataProvider::items('yesno'));
            break;
        case SitePost::TYPE_DROPDOWN:
            break;
    }
    ?>

    <?= $form->field($model, 'status')->dropDownList(DataProvider::items('status')) ?>

    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <?= Html::submitButton($model->isNewRecord ? 'Létrehoz' : 'Módosít', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
