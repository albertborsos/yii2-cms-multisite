<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\SitePost */

?>
<div class="row">
<div class="col-md-6">
<div class="site-post-create">

    <legend>Új bejegyzés létrehozása</legend>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
