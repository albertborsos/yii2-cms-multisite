<?php

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\SitePostContent */
/* @var $urlPostfix string|null */

echo \albertborsos\yii2lib\bootstrap\Tabs::widget([
    'encodeLabels' => false,
    'items' => [
        [
            'label' => 'Tartalom',
            'linkOptions' => [
                'href' => ['post-content/edit', 'post' => $model->site_post_id, 'language_id' => $model->site_language_id],
            ],
            'active' => false,
        ],
        [
            'label' => 'SEO beállítások',
            'content' => $this->render('_edit', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'urlPostfix' => $urlPostfix,
            ]),
            'active' => true,
        ],
    ]
]);