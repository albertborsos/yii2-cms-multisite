<?php

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\Document;
use albertborsos\yii2cmsmultisite\models\SitePost;
use albertborsos\yii2cmsmultisite\models\SitePostMeta;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\SitePostMeta */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $urlPostfix string|null */
?>

<div class="site-post-meta-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => '{label}<div class="col-md-8">{input}</div><div class="col-md-8 col-md-offset-4">{error}</div>',
            'labelOptions' => ['class' => 'col-md-4 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 70, 'placeholder' => $model->sitePost->getContent($model->site_language_id)->getTitle(true)]) ?>

    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => 160]) ?>

    <?= $form->field($model, 'meta_robots')->dropDownList(SitePostMeta::items('meta-robots')) ?>

    <?php
        $endPostFix = $urlPostfix && $model->sitePost->sort_order != 1 ? '<span class="input-group-addon">' . $urlPostfix . '</span>' : null;
        switch($model->sitePost->type){
            case SitePost::TYPE_BLOG:
                $slugPrefix = '/blog/';
                break;
            case SitePost::TYPE_MENU:
            default:
                $slugPrefix = '/';
                break;
        }
        echo $form->field($model, 'slug', [
            'inputTemplate' => '<div class="input-group"><span class="input-group-addon">' . $model->sitePost->site->baseUrl . $slugPrefix . '</span>{input}' . $endPostFix . '</div>',
        ])->textInput(['maxlength' => 255, 'placeholder' => ltrim($model->sitePost->generateUrl($model->site_language_id, false), '/')]);
    ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => 255, 'placeholder' => $model->sitePost->generateUrl($model->site_language_id, false)]); ?>

    <?= $form->field($model, 'canonical_post_id')->dropDownList($model->getCanonicalPosts(true, true), ['prompt' => 'Válassz, ha ez az oldal hasonlít egy másikra!']) ?>

    <?= $form->field($model, 'canonical_url')->textInput(['maxlength' => 255, 'placeholder' => $model->sitePost->site->baseUrl . $model->sitePost->generateUrl($model->site_language_id, false)]) ?>

    <?= $form->field($model, 'og_title')->textInput(['maxlength' => 55, 'placeholder' => $model->title ?: $model->sitePost->getContent()->getTitle(true)]) ?>

    <?= $form->field($model, 'og_description')->textInput(['maxlength' => 160, 'placeholder' => $model->meta_description]) ?>

    <?= $form->field($model, 'og_site_name')->textInput(['maxlength' => 50, 'placeholder' => $model->sitePost->site->name]) ?>

    <?= $form->field($model, 'og_image_id')->widget(\albertborsos\yii2cmsmultisite\widgets\ImageSelectorWidget::class) ?>

    <?= $form->field($model, 'fb_app_id')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'published')->dropDownList(DataProvider::items('yesno')) ?>

    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <?= Html::submitButton($model->isNewRecord ? 'Létrehoz' : 'Módosít', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
