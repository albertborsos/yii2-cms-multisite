<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\SitePostMeta */

?>
<div class="site-post-meta-view">

    <legend>SEO beállítások áttekintés</legend>

    <p>
        <?= Html::a('Módosítás', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Törlés', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Biztosan törölni szeretnéd ezt az elemet?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'site_post_id',
            'site_language_id',
            'title',
            'meta_description',
            'meta_robots',
            'url:url',
            'canonical_post_id',
            'og_title',
            'og_description',
            'og_site_name',
            'og_image_id',
            'fb_app_id',
            'published',
            'created_at',
            'created_user',
            'updated_at',
            'updated_user',
            'status',
        ],
    ]) ?>

</div>
