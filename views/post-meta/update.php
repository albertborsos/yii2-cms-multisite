<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\SitePostMeta */

?>
<div class="row">
<div class="col-md-6">
<div class="site-post-meta-update">

    <legend>SEO beállítások módosítása</legend>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
