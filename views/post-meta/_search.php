<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model vendor\albertborsos\yii2cmsmultisite\models\SitePostMetaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-post-meta-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'site_post_id') ?>

    <?= $form->field($model, 'site_language_id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'meta_description') ?>

    <?php // echo $form->field($model, 'meta_robots') ?>

    <?php // echo $form->field($model, 'url') ?>

    <?php // echo $form->field($model, 'canonical_post_id') ?>

    <?php // echo $form->field($model, 'og_title') ?>

    <?php // echo $form->field($model, 'og_description') ?>

    <?php // echo $form->field($model, 'og_site_name') ?>

    <?php // echo $form->field($model, 'og_image_id') ?>

    <?php // echo $form->field($model, 'fb_app_id') ?>

    <?php // echo $form->field($model, 'published') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_user') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_user') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
