<?php

use albertborsos\yii2cmsmultisite\models\Document;
use albertborsos\yii2lib\db\ActiveRecord;
use yii\helpers\Html;
use kartik\grid\GridView;
use albertborsos\yii2cmsmultisite\components\DataProvider;

/* @var $this yii\web\View */
/* @var $searchModel albertborsos\yii2cmsmultisite\models\GalleryDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="gallery-document-index">

    <?= \albertborsos\yii2cmsmultisite\components\SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Galériaképek</h3>',
            'type' => 'default',
            'showFooter' => false
        ],
        'floatHeader' => true,
        'export' => false, // [], ha exportálni szeretnél
        'columns' => [
            [
                'header' => '',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center', 'style' => 'width:5%;min-width:30px;'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return \rmrevin\yii\fontawesome\FA::icon(\rmrevin\yii\fontawesome\FA::_ARROWS_V);
                },
            ],
            [
                'header' => 'Előnézet',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    /** @var $model \albertborsos\yii2cmsmultisite\models\GalleryDocument */
                    switch($model->document->type){
                        case Document::TYPE_IMAGE:
                        case Document::TYPE_VIDEO_PREVIEW:
                        case Document::TYPE_DOCUMENT_PREVIEW:
                            return $model->document->getPreviewImageWithLink();
                            break;
                    }
                },
            ],
            [
                'attribute' => 'url',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_TEXTINPUT, 'url');
                },
            ],
            [
                'attribute' => 'status',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_DROPDOWN, 'status', DataProvider::items('status'));
                },
                'filter' => DataProvider::items('status'),
            ],
            [
                'attribute' => 'updated_at',
                //'header'      => 'Utolsó módosítás',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'format' => 'raw',
                'headerOptions' => ['class' => 'text-center'],
                'value' => function ($model, $index, $widget) {
                    return \albertborsos\yii2lib\db\ActiveRecord::showLastModifiedInfo($model);
                },
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{delete}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return [$action, 'id' => $model->id];
                },
                //'dropdown' => true,
                'width' => '120px',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        if (Yii::$app->user->can('editor')) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                                'class' => 'btn btn-sm btn-default'
                            ]);
                        } else {
                            return '';
                        }
                    },
                ],
            ],
        ],
    ]); ?>

</div>
