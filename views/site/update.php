<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\Site */

?>
<div class="row">
<div class="col-md-6">
<div class="site-update">

    <legend>Oldal módosítása</legend>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
