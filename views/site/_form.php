<?php

use albertborsos\yii2cmsmultisite\components\DataProvider;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\Site */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => '{label}<div class="col-md-8">{input}</div><div class="col-md-8 col-md-offset-4">{error}</div>',
            'labelOptions' => ['class' => 'col-md-4 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'baseUrl')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'type')->dropDownList(DataProvider::items('site_type')) ?>

    <?= $form->field($model, 'og_image_id')->widget(\albertborsos\yii2cmsmultisite\widgets\ImageSelectorWidget::class) ?>

    <?= $form->field($model, 'status')->dropDownList(DataProvider::items('status')) ?>

    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <?= Html::submitButton($model->isNewRecord ? 'Létrehoz' : 'Módosít', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
