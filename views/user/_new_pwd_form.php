<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<?php if(empty(Yii::$app->user->identity->password_hash)): ?>
    <div class="alert alert-danger">
        <h4>Nincsen jelszavad!</h4>
        <p>Gyorsan adj meg most egy jelszót, hogy ne kelljen minden belépésnél jelszóemlékeztetőt kérned!</p>
    </div>
<?php endif;?>

<?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => '{label}<div class="col-md-8">{input}</div><div class="col-md-8 col-md-offset-4">{error}</div>',
            'labelOptions' => ['class' => 'col-md-4 control-label'],
        ],
    ]);?>


<?php $form->errorSummary($model); ?>

<?= Html::activeHiddenInput($model, 'email') ?>
<?= $form->field($model, 'password')->passwordInput() ?>
<?= $form->field($model, 'password_again')->passwordInput() ?>


<div class="form-group row">
    <div class="col-md-8 col-md-offset-4">
        <?= Html::submitButton(
            'Beállítom',
            ['class' => 'btn btn-primary', 'id' => 'setnewpasswordform-submit']
        ) ?>
    </div>
</div>
<div class="clearfix"></div>
<?php ActiveForm::end(); ?>
