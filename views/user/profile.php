<?php
/**
 * @var yii\web\View $this
 * @var albertborsos\yii2cmsmultisite\models\UserDetails $model
 * @var SetNewPasswordForm $new_pwd_model
 */

echo \albertborsos\yii2lib\bootstrap\Tabs::widget([
    'items' => [
        [
            'label' => 'Jelszómódosítás',
            'content' => \yii\helpers\Html::tag('div', $this->render('_new_pwd_form', [
                'model' => $new_pwd_model,
            ]), ['class' => 'col-md-8', 'style' => 'padding:10px;']),
            'active' => true,
        ],
        [
            'label' => 'Fiókadatok módosítása/törlése',
            'content' => \yii\helpers\Html::tag('div', $this->render('@vendor/albertborsos/yii2-cms-multisite/views/userdetails/_form.php', [
                'model' => $model,
            ]), ['class' => 'col-md-8', 'style' => 'padding:10px;']),
            'active' => false,
        ],
    ]
]);