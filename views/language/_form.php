<?php

use albertborsos\yii2cmsmultisite\models\Site;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use albertborsos\yii2cmsmultisite\components\DataProvider;
use kartik\widgets\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\widgets\TimePicker;
use \vova07\imperavi\Widget as Redactor;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\SiteLanguage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-language-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => '{label}<div class="col-md-8">{input}</div><div class="col-md-8 col-md-offset-4">{error}</div>',
            'labelOptions' => ['class' => 'col-md-4 control-label'],
        ],
    ]); ?>

    <?php
        if(is_null($model->site_id)){
            $sites = Site::find()->where('status <> :deleted', [':deleted' => Site::STATUS_DELETED])->asArray()->all();
            echo $form->field($model, 'site_id')->dropDownList(\yii\helpers\ArrayHelper::map($sites, 'id', 'name'));
        }else{
            echo Html::activeHiddenInput($model, 'site_id');
        }
    ?>

    <?= $form->field($model, 'code')->dropDownList(DataProvider::items('languages')) ?>

    <?= $form->field($model, 'status')->dropDownList(DataProvider::items('status')) ?>

    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <?= Html::submitButton($model->isNewRecord ? 'Létrehoz' : 'Módosít', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
