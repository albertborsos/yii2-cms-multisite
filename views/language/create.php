<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\SiteLanguage */

?>
<div class="row">
<div class="col-md-6">
<div class="site-language-create">

    <legend>Új nyelv létrehozása</legend>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
