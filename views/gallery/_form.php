<?php

use albertborsos\yii2cmsmultisite\models\Gallery;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use albertborsos\yii2cmsmultisite\components\DataProvider;
use kartik\widgets\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\widgets\TimePicker;
use \vova07\imperavi\Widget as Redactor;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\Gallery */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => '{label}<div class="col-md-8">{input}</div><div class="col-md-8 col-md-offset-4">{error}</div>',
            'labelOptions' => ['class' => 'col-md-4 control-label'],
        ],
    ]); ?>

    <?php
        $model->replace_id = str_replace([Gallery::PREFIX, Gallery::POSTFIX], '', $model->replace_id);
        echo $form->field($model, 'replace_id', [
            'inputTemplate' => '<div class="input-group"><span class="input-group-addon">[#</span>{input}<span class="input-group-addon">#]</span></div>',
        ])->textInput(['maxlength' => 255]);
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'order_by')->dropDownList(DataProvider::items('order')) ?>

    <?= $form->field($model, 'page_size')->dropDownList(DataProvider::items('pagesize')) ?>

    <?= $form->field($model, 'items_in_a_row')->dropDownList(DataProvider::items('itemsinarow')) ?>

    <?= $form->field($model, 'status')->dropDownList(DataProvider::items('status')) ?>

    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <?= Html::submitButton($model->isNewRecord ? 'Létrehoz' : 'Módosít', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
