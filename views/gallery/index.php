<?php

use albertborsos\yii2lib\db\ActiveRecord;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use kartik\grid\GridView;
use albertborsos\yii2cmsmultisite\components\DataProvider;

/* @var $this yii\web\View */
/* @var $searchModel albertborsos\yii2cmsmultisite\models\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="gallery-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Galériák</h3>',
            'type' => 'default',
            'showFooter' => false
        ],
        'floatHeader' => true,
        'export' => false, // [], ha exportálni szeretnél
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'name',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_TEXTINPUT, 'name');
                },
            ],
            [
                'attribute' => 'replace_id',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return Html::input('text', $model['replace_id'], $model['replace_id'], [
                        'class' => 'form-control',
                        'readonly' => 'readonly'
                    ]);
                },
            ],
            [
                'header'        => 'Képek',
                'hAlign'        => 'center',
                'vAlign'        => 'middle',
                'format' => 'raw',
                'headerOptions' => ['class' => 'text-center'],
                'value'         => function ($model, $index, $widget) {
                    return count($model->galleryDocuments). ' ' . 'db';
                },
            ],
            [
                'attribute' => 'order_by',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_DROPDOWN, 'order_by', DataProvider::items('order'));
                },
            ],
            [
                'attribute' => 'page_size',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_DROPDOWN, 'page_size', DataProvider::items('pagesize'));
                },
            ],
            [
                'attribute' => 'items_in_a_row',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_DROPDOWN, 'items_in_a_row', DataProvider::items('itemsinarow'));
                },
            ],
            [
                'attribute' => 'status',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_DROPDOWN, 'status', DataProvider::items('status'));
                },
                'filter' => DataProvider::items('status'),
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{image} {update} {delete}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return [$action, 'id' => $model->id];
                },
                //'dropdown' => true,
                'width' => '120px',
                'buttons' => [
                    'image' => function ($url, $model) {
                        if (Yii::$app->user->can('editor')) {
                            $url = ['gallery-document/index', 'gallery_id' => $model->id];
                            return Html::a(FA::icon(FA::_PICTURE_O), $url, [
                                'title' => Yii::t('yii', 'Update'),
                                'data-pjax' => '0',
                                'class' => 'btn btn-sm btn-default'
                            ]);
                        } else {
                            return '';
                        }
                    },
                    'update' => function ($url, $model) {
                        if (Yii::$app->user->can('editor')) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('yii', 'Update'),
                                'data-pjax' => '0',
                                'class' => 'btn btn-sm btn-default'
                            ]);
                        } else {
                            return '';
                        }
                    },
                    'delete' => function ($url, $model) {
                        if (Yii::$app->user->can('editor')) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                                'class' => 'btn btn-sm btn-default'
                            ]);
                        } else {
                            return '';
                        }
                    },
                ],
            ],
        ],
    ]); ?>

</div>
