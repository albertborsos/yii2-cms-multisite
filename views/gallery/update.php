<?php

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\Gallery */

?>
<div class="row">
    <div class="col-md-4">
        <div class="site-gallery-update">
            <?= \kartik\helpers\Html::panel([
                'heading' => \kartik\helpers\Html::tag('h3', FA::icon(FA::_PLUS).' Galéria módosítása', ['class' => 'panel-title']),
                'body' => $this->render('_form', [
                    'model' => $model,
                ]),
            ])?>
        </div>
    </div>
    <div class="col-md-8">
        <?= $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ])?>
    </div>
</div>