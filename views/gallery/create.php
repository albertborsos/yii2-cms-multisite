<?php

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\Gallery */

?>
<div class="row">
    <div class="col-md-4">
        <div class="site-gallery-create">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title"><i class="fa fa-plus"></i> Új galéria létrehozása
                    </h3></div>
                <div class="panel-body">
                    <?= $this->render('_form', [
                    'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <?= $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ])?>
    </div>
</div>