<?php

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\Document;
use albertborsos\yii2cmsmultisite\models\DocumentContent;
use albertborsos\yii2cmsmultisite\models\SiteLanguage;
use albertborsos\yii2lib\db\ActiveRecord;
use albertborsos\yii2lib\wrappers\Editable;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel albertborsos\yii2cmsmultisite\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="document-index">

    <?php
    $buttons = [];
    if (!$hideButtons) {
        $buttons = [
            'type' => 'default',
            'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> Új ' . $label, ['upload', 'type' => Yii::$app->request->get('type')], ['class' => 'btn btn-success']),
            'showFooter' => false
        ];
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => ArrayHelper::merge(['heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Dokumentumok</h3>'], $buttons),
        'floatHeader' => true,
        'export' => false, // [], ha exportálni szeretnél
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'header' => 'Előnézet',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    /** @var $model Document */
                    switch ($model->type) {
                        case Document::TYPE_IMAGE:
                        case Document::TYPE_VIDEO_PREVIEW:
                        case Document::TYPE_DOCUMENT_PREVIEW:
                            return $model->getPreviewImageWithLink();
                            break;
                    }
                },
                'visible' => in_array(Yii::$app->request->get('type'), [Document::TYPE_IMAGE, Document::TYPE_VIDEO_PREVIEW, Document::TYPE_DOCUMENT_PREVIEW]),
            ],
            [
                'attribute' => 'filename',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return $model['filename'];
                },
            ],
            [
                'attribute' => 'name',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    /** @var $model Document */
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_TEXTINPUT, 'name');
                },
            ],
            [
                'header' => Yii::t('app', 'Contents'),
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    $out = '';

                    $languageCodes = SiteLanguage::find()->select(['code'])->distinct()->where([
                        'status' => SiteLanguage::STATUS_ACTIVE,
                    ])->asArray()->all();

                    $languageCodes = array_values(ArrayHelper::map($languageCodes, 'code', 'code'));

                    foreach ($languageCodes as $languageCode) {
                        $content = DocumentContent::find()->where([
                            'document_id' => $model->id,
                            'language_code' => $languageCode,
                        ])->orderBy(['id' => SORT_DESC])->one();

                        if (!is_null($content)) {
                            $badgeClass = 'btn-success';
                            if (count($model->showAlerts($languageCode, false)) > 0) {
                                $badgeClass = 'btn-warning';
                            }
                        } else {
                            $badgeClass = 'btn-danger';
                        }
                        $out .= Html::a(DataProvider::items('languages', $languageCode),
                            ['document-content/edit', 'document' => $model->id, 'language_code' => $languageCode],
                            ['class' => 'btn btn-block btn-xs ' . $badgeClass]);
                    }
                    return $out;
                },
            ],
//            [
//                'attribute' => 'right_to_view',
//                'hAlign' => 'center',
//                'vAlign' => 'middle',
//                'headerOptions' => ['class' => 'text-center'],
//                'format' => 'raw',
//                'value' => function ($model, $index, $widget) {
//                    return $model['right_to_view'];
//                },
//                'visible' => in_array(Yii::$app->request->get('type'), [Document::TYPE_DOCUMENT]),
//            ],
            [
                'attribute' => 'url',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    /** @var $model Document */
                    switch ($model->type) {
                        case Document::TYPE_VIDEO_PREVIEW:
                        case Document::TYPE_DOCUMENT_PREVIEW:
                            return $model->editable(ActiveRecord::EDITABLE_TYPE_TEXTINPUT, 'url');
                            break;
                    }
                },
                'visible' => in_array(Yii::$app->request->get('type'), [Document::TYPE_VIDEO_PREVIEW, Document::TYPE_DOCUMENT_PREVIEW]),
            ],
            [
                'attribute' => 'preview_image_id',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    $source = Document::asDropDown('id', function (Document $item): string {
                        return $item->filename;
                    }, ['type' => Document::TYPE_DOCUMENT_PREVIEW], ['id' => SORT_DESC]);

                    /** @var $model Document */
                    switch ($model->type) {
                        case Document::TYPE_DOCUMENT:
                            return Editable::select2('preview_image_id', $model->getPrimaryKey(), $model->preview_image_id, 'Válassz..', ['updatebyeditable'], $source, []);
                    }
                },
                'visible' => in_array(Yii::$app->request->get('type'), [Document::TYPE_DOCUMENT]),
            ],
//            [
//                'attribute' => 'preview_max_width',
//                'hAlign' => 'center',
//                'vAlign' => 'middle',
//                'headerOptions' => ['class' => 'text-center'],
//                'format' => 'raw',
//                'value' => function ($model, $index, $widget) {
//                    return $model['preview_max_width'];
//                },
//                'visible' => in_array(Yii::$app->request->get('type'), [Document::TYPE_DOCUMENT]),
//            ],
//            [
//                'attribute' => 'preview_max_height',
//                'hAlign' => 'center',
//                'vAlign' => 'middle',
//                'headerOptions' => ['class' => 'text-center'],
//                'format' => 'raw',
//                'value' => function ($model, $index, $widget) {
//                    return $model['preview_max_height'];
//                },
//                'visible' => in_array(Yii::$app->request->get('type'), [Document::TYPE_DOCUMENT]),
//            ],
            [
                'attribute' => 'status',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function (Document $model) {
                    return $model->editable(ActiveRecord::EDITABLE_TYPE_DROPDOWN, 'status', DataProvider::items('status'));
                },
                'filter' => DataProvider::items('status'),
            ],

            [
                'attribute' => 'updated_at',
                //'header'      => 'Utolsó módosítás',
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'format' => 'raw',
                'headerOptions' => ['class' => 'text-center'],
                'value' => function ($model, $index, $widget) {
                    return \albertborsos\yii2lib\db\ActiveRecord::showLastModifiedInfo($model);
                },
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{delete}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return [$action, 'id' => $model->id];
                },
                //'dropdown' => true,
                'width' => '120px',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        if (Yii::$app->user->can('editor')) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                                'class' => 'btn btn-sm btn-default'
                            ]);
                        } else {
                            return '';
                        }
                    },
                ],
            ],
        ],
    ]); ?>

</div>
