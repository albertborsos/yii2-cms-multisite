<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use albertborsos\yii2cmsmultisite\components\DataProvider;
use kartik\widgets\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\widgets\TimePicker;
use \vova07\imperavi\Widget as Redactor;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\Document */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="document-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => '{label}<div class="col-md-8">{input}</div><div class="col-md-8 col-md-offset-4">{error}</div>',
            'labelOptions' => ['class' => 'col-md-4 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'type')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'filename')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'sha1_original')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'sha1_this')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'right_to_view')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'preview_image_id')->textInput() ?>

    <?= $form->field($model, 'preview_max_width')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'preview_max_height')->textInput(['maxlength' => 255]) ?>

      <?= $form->field($model, 'status')->dropDownList(DataProvider::items('status')) ?>

    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <?= Html::submitButton($model->isNewRecord ? 'Létrehoz' : 'Módosít', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
