<?php
use albertborsos\yii2cmsmultisite\models\Document;
use albertborsos\yii2lib\helpers\FileUpload;
use yii\helpers\Html;

?>
<div class="row">
    <div class="col-md-5">
        <?php
            $formID = 'document-upload';
        ?>
        <legend><?= Document::TYPE_LABELS[$type] ?></legend>
        <?= $form = Html::beginForm('', 'post', ['enctype'=>'multipart/form-data', 'id' => $formID]) ?>

        <?= FileUpload::widget([
            'name' => 'filename',
            'url' => ['upload-ajax', 'type' => $type],
            'fieldOptions' => ['multiple' => true, 'accept' => $accept],
            'clientOptions' => [
                'maxFileSize' => $maxFileSize,
                'formId' => $formID,
            ]
        ]);
        ?>

        <?= Html::endForm() ?>
    </div>
    <div class="col-md-7">
        <?= $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'hideButtons' => $hideButtons,
        ])?>
    </div>
</div>