<?php
/**
 * @var albertborsos\yii2cmsmultisite\models\Users $user
 */
?>

<h4>Kedves <?= \kartik\helpers\Html::encode($user->getDetails()->name_first); ?>!</h4>
<p>Mivel jelszóemlékeztetőt kértél a weboldalon, ezért az alábbi linken tudsz beállítani magadnak új jelszót:</p>
<?= \yii\helpers\Html::encode($link['reminder']); ?>