<p>Sikeres regisztráció! Ezen a linken tudod aktiválni a fiókodat:<br /> <?= \yii\helpers\Html::encode($link['activation'])?></p>
<p>Az aktiválás után be tudsz lépni az oldalra, azzal a jelszóval, amit megadtál!</p>
<p>E-mail címed: <?= \yii\helpers\Html::encode($user->email) ?></p>
<p>Ha elfelejtetted a jelszavad, akkor az oldalon tudsz újat kérni!</p>
