<?php

use albertborsos\yii2cmsmultisite\models\SitePost;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use albertborsos\yii2cmsmultisite\components\DataProvider;
use kartik\widgets\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\widgets\TimePicker;
use \vova07\imperavi\Widget as Redactor;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\SitePostContent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-post-content-form">

    <?php $form = ActiveForm::begin([
        //'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => '{label}{input}{error}',
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?php
        $layout = $form->field($model, 'layout')->dropDownList(Yii::$app->getModule('cms2')->layouts, ['prompt' => 'Válassz..']);

        $widget = $form->field($model, 'widget')->dropDownList(Yii::$app->getModule('cms2')->widgets, ['prompt' => 'Válassz..']);

        $fieldShort = $form->field($model, 'content_short')->widget(Redactor::classname(), [
            'settings' => \albertborsos\yii2lib\helpers\Widgets::redactorOptions(),
        ]);

        $settings = ArrayHelper::merge(\albertborsos\yii2lib\helpers\Widgets::redactorOptions(), [
            'imageManagerJson' => Url::to(['/cms2/api/images-get']),
            'imageUpload' => Url::to(['/cms2/api/image-upload']),
            'plugins' => [
                'imagemanager'
            ]
        ]);
    
        $fieldMain = $form->field($model, 'content_main')->widget(Redactor::classname(), [
            'settings' => $settings,
        ]);

        switch($model->sitePost->type){
            case SitePost::TYPE_DROPDOWN:
                break;
            case SitePost::TYPE_MENU:
                echo $layout;
                echo $widget;
                echo $fieldMain;
                break;
            case SitePost::TYPE_BLOG:
                echo $layout;
                echo $widget;
                echo $fieldShort;
                echo $fieldMain;
                break;
        }
    ?>

    <?= $form->field($model, 'date_show')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Válassz megjelenési időpontot'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]) ?>

    <?= $form->field($model, 'published')->dropDownList(DataProvider::items('yesno')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Létrehoz' : 'Módosít', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
