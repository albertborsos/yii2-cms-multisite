<?php

use albertborsos\yii2cmsmultisite\components\DataProvider;
use albertborsos\yii2cmsmultisite\models\SitePost;
use albertborsos\yii2cmsmultisite\models\SitePostContent;
use kartik\grid\GridView;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\SitePostContent */

?>

<div class="row">
<div class="col-md-8">
<div class="site-post-content-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
<div class="col-md-4">
<div class="site-post-content-list">
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'panel' => [
        'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Bejegyzések</h3>',
        'showFooter'=>false
    ],
    'panelTemplate' => '<div class="panel {type}">
			{panelHeading}
			{items}
		</div>',
    'floatHeader'=>true,
    'rowOptions' => function($model, $key, $index, $grid){
        if($model->published == DataProvider::YES){
            return ['class' => 'success'];
        }elseif($model->published == DataProvider::NO){
            return ['class' => 'danger'];
        }
        return null;
    },
    'columns' => [
        [
            'attribute'      => 'id',
            'hAlign'         => 'center',
            'vAlign'         => 'middle',
            'headerOptions'  => ['class' => 'text-center'],
            'format'         => 'raw',
            'value'          => function($model, $index, $widget){
                return $model['id'];
            },
        ],
        [
            'attribute'      => 'published',
            'hAlign'         => 'center',
            'vAlign'         => 'middle',
            'headerOptions'  => ['class' => 'text-center'],
            'format'         => 'raw',
            'value'          => function($model, $index, $widget){/** @var $model \albertborsos\yii2cmsmultisite\models\SitePostContent */
                return $model->editable($model::EDITABLE_TYPE_DROPDOWN, 'published', DataProvider::items('yesno'));
            },
        ],
        [
            'attribute'     => 'created_at',
            //'header'      => 'Utolsó módosítás',
            'hAlign'        => 'center',
            'vAlign'        => 'middle',
            'format'        => 'raw',
            'headerOptions' => ['class' => 'text-center'],
            'value'         => function ($model, $index, $widget) {
                return \albertborsos\yii2lib\helpers\Date::timestampToDate($model->created_at);
            },
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{edit}',
            'urlCreator' => function($action, $model, $key, $index) { return [$action, 'id' => $model->id]; },
            //'dropdown' => true,
            'width' => '120px',
            'buttons' => [
                'edit' => function ($url, $model) {
                    $url = ['edit', 'post' => $model->site_post_id, 'language_id' => $model->site_language_id, 'rev' => $model->id];
                    if (Yii::$app->user->can('editor')){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('yii', 'Update'),
                            'data-pjax' => '0',
                            'class'=>'btn btn-sm btn-default'
                        ]);
                    }else{
                        return '';
                    }
                },
            ],
        ],
    ],
]);
?>


</div>
</div>
</div>
