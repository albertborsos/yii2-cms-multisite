<?php

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\SitePostContent */

$tabItems = [
    [
        'label' => 'Tartalom',
        'content' => $this->render('_edit', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]),
        'active' => true,
    ],
    [
        'label' => 'SEO beállítások',
        'content' => 'SEO',
        'active' => false,
        'linkOptions' => [
            'href' => ['post-meta/edit', 'post' => $model->site_post_id, 'language_id' => $model->site_language_id],
        ],
    ],
];

echo \albertborsos\yii2lib\bootstrap\Tabs::widget([
    'encodeLabels' => false,
    'items' => $tabItems,
]);