<?php
    use albertborsos\yii2cmsmultisite\models\SitePost;
    use albertborsos\yii2lib\helpers\Date;
    use albertborsos\yii2cmsmultisite\models\Users;
    use rmrevin\yii\fontawesome\FA;
    use yii\helpers\Html;

    /* @var albertborsos\yii2cmsmultisite\models\SitePost $post */
    /* @var string $tags */
?>
<div class="content">
    <div class="header">
        <h3><?= Html::a($post->getContent()->name, $post->generateUrl())?></h3>

        <div class="description">
            <span class="author"><?= FA::icon(FA::_USER) ?> Szerző: <?= Users::findIdentity($post->created_user)->getFullname() ?></span>
            <span class="date"><?= FA::icon(FA::_CALENDAR) ?> <?= Date::timestampToDate($post->created_at) ?></span>
                <span class="comment-count">
                <?= FA::icon(FA::_COMMENT) ?>
                <?= Html::a('Szólj hozzá!', $post->generateUrl() . '#disqus_thread')?>
            </span>
        </div>
    </div>
    <div class="tags">
        <p><?= $tags ?></p>
    </div>
    <div class="body">
        <?= $post->getContent()->content_short ?>
    </div>
    <div class="body separator">&nbsp;</div>
</div>