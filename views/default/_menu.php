<?php
	use kartik\social\FacebookPlugin;

    /* @var albertborsos\yii2cmsmultisite\models\SitePost $post */
    /* @var string $tags */
?>
<?= $post->getContent()->content_main ?>
<?php if($post->shareable):?>
<div id="social-buttons" class="alert alert-warning">
    <?= FacebookPlugin::widget(['type' => FacebookPlugin::LIKE]) ?>
</div>
<?php endif; ?>
<?php if($post->commentable):?>
    <div id="comments">
        <?= \kartik\social\Disqus::widget(); ?>
    </div>
<?php endif; ?>