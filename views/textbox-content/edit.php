<?php

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\TextboxContent */

echo $this->render('_edit', [
    'model' => $model,
    'dataProvider' => $dataProvider,
]);