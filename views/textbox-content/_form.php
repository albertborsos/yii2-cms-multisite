<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use albertborsos\yii2cmsmultisite\components\DataProvider;
use kartik\widgets\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\widgets\TimePicker;
use \vova07\imperavi\Widget as Redactor;

/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\TextboxContent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="textbox-content-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'widget')->dropDownList(Yii::$app->getModule('cms2')->widgets, ['prompt' => 'Válassz..']) ?>

    <?= $form->field($model, 'content')->widget(Redactor::classname(), [
        'settings' => \albertborsos\yii2lib\helpers\Widgets::redactorOptions(),
    ]) ?>

    <?= $form->field($model, 'published')->dropDownList(DataProvider::items('noyes')) ?>

    <div class="row">
        <div class="col-md-12">
            <?= Html::submitButton($model->isNewRecord ? 'Létrehoz' : 'Módosít', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
