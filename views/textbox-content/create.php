<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model albertborsos\yii2cmsmultisite\models\TextboxContent */

?>
<div class="row">
<div class="col-md-6">
<div class="textbox-content-create">

    <legend>Új szövegdoboz tartalom létrehozása</legend>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
