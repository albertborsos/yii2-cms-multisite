<?php

    use albertborsos\yii2lib\helpers\Values;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var albertborsos\yii2cmsmultisite\models\UserDetails $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="user-details-form">

    <?php $form = ActiveForm::begin([
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => '{label}<div class="col-md-8">{input}</div><div class="col-md-8 col-md-offset-4">{error}</div>',
                'labelOptions' => ['class' => 'col-md-4 control-label'],
            ],
        ]); ?>

    <?= $form->field($model, 'name_first')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'name_last')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'email')->input('email', ['maxlength' => 100, 'disabled' => 'disabled']) ?>

    <div class="form-group row">
        <div class="col-md-8 col-md-offset-4 pull-right">
            <?= Html::submitButton('Mentés', ['class' => 'btn btn-primary']) ?>
            <?= Html::submitButton('Fiókom törlése', [
                'class' => 'btn btn-default',
                'name' => 'remove-profile',
                'data-confirm' => 'Biztosan véglegesen törlöd a profilodat? Minden adatod el fog veszni!',
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
