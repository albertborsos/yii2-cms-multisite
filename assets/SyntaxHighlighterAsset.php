<?php

namespace albertborsos\yii2cmsmultisite\assets;

use yii\web\AssetBundle;
use Yii;

class SyntaxHighlighterAsset extends AssetBundle
{
    public $time;
    public $sourcePath = '@vendor/albertborsos/yii2-cms-multisite/assets/vendor/';

    public $css = [
        'syntaxHighlighter/shCoreEclipse.css',
        'syntaxHighlighter/shThemeEclipse.css',
    ];

    public $js = [
        'syntaxHighlighter/shCore.js',
        'syntaxHighlighter/shBrushJScript.js',
        'syntaxHighlighter/shBrushPhp.js',
        'syntaxHighlighter/shBrushPerl.js',
        'syntaxHighlighter/shBrushBash.js',
        'syntaxHighlighter/shBrushSql.js',
        'syntaxHighlighter/shBrushCss.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
