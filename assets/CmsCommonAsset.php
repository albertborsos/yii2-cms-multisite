<?php

namespace albertborsos\yii2cmsmultisite\assets;

use yii\web\AssetBundle;
use Yii;

class CmsCommonAsset extends AssetBundle
{
    public $time;
    public $sourcePath = '@vendor/albertborsos/yii2-cms-multisite/assets/common/';

    public $css = [
        'css/common.css',
    ];

    public $js = [
        'js/images.js',
        'js/init-bootbox.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
        '\albertborsos\yii2cmsmultisite\assets\BowerAsset',
    ];

} 