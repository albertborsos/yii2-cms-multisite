$(function(){
    yii.allowAction = function($e) {
        var message = $e.data('confirm');
        return message === undefined || yii.confirm(message, $e);
    };
    yii.confirm = function(message, ok, cancel) {
        bootbox.confirm({
            message: message,
            buttons: {
                confirm: {
                    label: 'OK'
                },
                cancel: {
                    label: 'Mégsem'
                }
            },
            callback: function(confirmed) {
                if (confirmed) {
                    return !ok || ok();
                } else {
                    return !cancel || cancel();
                }
            }
        });
        // confirm will always return false on the first call
        // to cancel click handler
        return false;
    };
});