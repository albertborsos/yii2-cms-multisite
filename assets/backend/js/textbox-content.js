$(function(){

    var textboxContent = {
        init: function(){
            this.cacheDOM();
            this.bindEvents();
            this.toggleContent();
        },
        cacheDOM: function(){
            this.selectWidget = $('#textboxcontent-widget');
            this.wrapperContent = $('.field-textboxcontent-content');
        },
        bindEvents: function(){
            this.selectWidget.on('change', this.toggleContent.bind(this));
        },
        toggleContent: function(){
            if (this.selectWidget.val()){
                this.wrapperContent.slideUp();
            }else{
                this.wrapperContent.slideDown();
            }
        }
    };

    textboxContent.init();

});