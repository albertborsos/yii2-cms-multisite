$(function(){

    var postContent = {
        init: function(){
            this.cacheDOM();
            this.bindEvents();
            this.toggleContent();
        },
        cacheDOM: function(){
            this.selectWidget = $('#sitepostcontent-widget');
            this.wrapperContent = $('.field-sitepostcontent-content_main');
        },
        bindEvents: function(){
            this.selectWidget.on('change', this.toggleContent.bind(this));
        },
        toggleContent: function(){
            if (this.selectWidget.val()){
                this.wrapperContent.slideUp();
            }else{
                this.wrapperContent.slideDown();
            }
        }
    };

    postContent.init();

});