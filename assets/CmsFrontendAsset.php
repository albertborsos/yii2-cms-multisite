<?php

namespace albertborsos\yii2cmsmultisite\assets;

use yii\web\AssetBundle;
use Yii;

class CmsFrontendAsset extends AssetBundle
{
    public $time;
    public $sourcePath = '@vendor/albertborsos/yii2-cms-multisite/assets/frontend/';

    public $css = [
        'css/style.css',
        'css/fb_responsive.css',
    ];

    public $js = [
        'js/cms.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
        '\albertborsos\yii2cmsmultisite\assets\CmsCommonAsset',
    ];

} 