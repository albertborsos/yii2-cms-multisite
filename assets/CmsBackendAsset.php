<?php

namespace albertborsos\yii2cmsmultisite\assets;

use yii\web\AssetBundle;
use Yii;

class CmsBackendAsset extends AssetBundle
{
    public $time;
    public $sourcePath = '@vendor/albertborsos/yii2-cms-multisite/assets/backend/';

    public $css = [
        'css/style.css',
    ];

    public $js = [
        'js/post-content.js',
        'js/textbox-content.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
        '\albertborsos\yii2cmsmultisite\assets\CmsCommonAsset',
    ];

} 