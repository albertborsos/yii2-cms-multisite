<?php
/**
 * Created by PhpStorm.
 * User: aborsos
 * Date: 2016. 05. 21.
 * Time: 12:08
 */

namespace albertborsos\yii2cmsmultisite\assets;


use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;

class BowerAsset extends AssetBundle
{
    public $sourcePath = '@bower/';
    public $js         = [
        'bootbox/bootbox.js',
        'magnific-popup/dist/jquery.magnific-popup.js'
    ];
    public $css = [
        //'awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
        'magnific-popup/dist/magnific-popup.css'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
    ];
}